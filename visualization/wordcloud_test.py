import cPickle as pickle
import wordcloud
import sys
import matplotlib.pyplot as plt

def clouds_from_freq(freqs):
    wc = wordcloud.WordCloud(width = 800,height = 400).fit_words(freqs)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(wc)
    ax.axis("off")
    plt.savefig('test.png',dpi = 800)

def multi_topic_cloud(topics,fname):
    fig = plt.figure(figsize = (16,9))
    for i in range(len(topics)):
        ax = fig.add_subplot(len(topics),1,i+1)
	wc = wordcloud.WordCloud(width = 800).fit_words(topics[i])
	ax.imshow(wc)
	ax.axis("off")
	top_num = i + 1
	ax.set_title("Topic %i" %top_num)
    plt.savefig(fname)



def load_topics(fPath):
    with open(fPath,"rb") as f:
	topics = pickle.load(f)
    return(topics)


if __name__ == "__main__":
    infile = sys.argv[1]
    topics = load_topics(infile)
    start = int(sys.argv[2])
    num_tops = int(sys.argv[3])
    fname = sys.argv[4]
    multi_topic_cloud(topics[start:start + num_tops],fname)


