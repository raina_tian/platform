# -*- coding: utf-8 -*-
"""
Code for making word clouds from grams and their weights

@author: Eli
"""

from six.moves import cPickle as pickle
import wordcloud
import sys
import numpy as np
import matplotlib.pyplot as plt


def cloud(freqs = None,grams = None, weights = None):
    """Generates a word cloud from grams and weights, doesn't save
    Input:    
        freqs: a list of (gram,weight) tuples. Use this or grams and weights.
        grams: a list of grams. Use this and weights or just freqs
        weights: a list of the weights for the grams
    Output:
        wc: worcloud object"""
    #Use freqs
    if not freqs is None:
        wc = wordcloud.WordCloud(width = 800,height = 400).fit_words(freqs)
    #use grams and weights
    elif not grams is None and not weights is None:
        wc = wordcloud.WordCloud(width = 800,height = 400).fit_words(
                                                            zip(grams,weights))

    return(wc)        
        

def cloud_from_freq(fname,freqs = None,grams = None, weights = None):
    """Generates a wordcloud from (gram,weight) pairs and saves
    Input:
        fname: file name to save to
        freqs: a list of (gram,weight) tuples. Use this or grams and weights.
        grams: a list of grams. Use this and weights or just freqs
        weights: a list of the weights for the grams"""
    #Use freqs
    if not freqs is None:
        wc = wordcloud.WordCloud(width = 800,height = 400).fit_words(freqs)
    #use grams and weights
    elif not grams is None and not weights is None:
        wc = wordcloud.WordCloud(width = 800,height = 400).fit_words(
                                                            zip(grams,weights))

    wc.to_file(fname)
    
    


def cloud_from_df(df,name_col,weight_col,fname,num_rows = 400):
    """Generates a wordcloud from a pandas dataframe
    Input:
        df: DataFrame
        name_col: name of column with the words/grams
        weight_col: column of weights (frequencies, etc.)
        fname: file to save to
        num_rows: number of rows of the dataframe to use (assuming sorted)
    """
    #if num_rows is grater than the number of rows, use all rows
    if num_rows > df.shape[0]:
        num_rows = df.shape[0]
    #get gram and weight arrays
    grams = np.array(df[name_col].iloc[:num_rows])
    weights = np.array(df[weight_col].iloc[:num_rows])
    
    #crate and save word cloud
    cloud_from_freq(fname,grams = grams, weights = weights)
    

        