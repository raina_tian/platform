# -*- coding: utf-8 -*-
"""
Code for visualizing topic distributions

@author: Eli
"""
from scipy.stats import entropy
import numpy as np
import matplotlib.pyplot as plt

  
def js_div(p,q):
    """Calculates the Jenson-Shannon Divergence between discrete distributions
            p and q
        Input:
            p,q: prob dists as numpy arrays
        Output:
            JS divergence"""
    m = .5 * (p + q)   
    return(.5 * (entropy(p,m,2) + entropy(q,m,2)))
    
def jsdiv_mat(mat):
    """Computes a dissimilarity matrix from mat using JS Divergence"""
    
    n = mat.shape[0]
    diss = np.zeros((n,n))
    for i in range(n):
        for j in range(i + 1):
            diss[i,j] = js_div(mat[i,:],mat[j,:])
    diss = diss + diss.T
    return(diss)

def jskern(mat,a):
    """Computes a kernel matrix from the JS Divergence by converting it to 
        a kernel as in 
        http://www.svcl.ucsd.edu/publications/techreports/SVCL-TR-2004-01.pdf
    """
    #initialize kernel matrix
    n = mat.shape[0]
    K = np.zeros((n,n))
    for i in range(n):
        for j in range(i + 1):
            K[i,j] = np.exp(a *js_div(mat[i,:],mat[j,:]))
    K = K + K.T
    return(K)    

def linear_kern(mat):
    """Computes the linear kernel"""
    return(np.dot(mat,mat.T))    
    
def ppkern(mat,rho):
    """Computes the  Probability product kernel for a matrix of topic counts
        http://www.jmlr.org/papers/volume5/jebara04a/jebara04a.pdf"""
    #initialize kernel matrix    
    n = mat.shape[0]
    K = np.zeros((n,n))
    #calculate kernels
    for i in range(n):
        for j in range(i + 1):
            v1 = np.power(np.multiply(mat[i,:],mat[j,:]),rho)
            v2 = np.power(np.multiply(1 - mat[i,:],1 - mat[j,:]),rho)
            K[i,j] = np.prod(v1 + v2)
    #symmetrize
    K = K + K.T
    return(K)

def kern_to_dist(K):
    """Converts a kernel matrix to a distance matrix"""
    #initialize distance matrix
    n = K.shape[0]
    D = np.zeros(K.shape)
    for i in range(n):
        for j in range(i + 1):
            D[i,j] = np.sqrt(K[i,i] + K[j,j] - 2*K[i,j])
    #symmetrize
    D = D + D.T
    return(D)
    
def plot(X, title,labels = None):
    """Plots 2-D data set X (a dataset after doing dimensionality reduction)"""
    if labels is not None:    
        plt.scatter(X[:,0],X[:,1],c = labels,cmap = plt.cm.Set1)
    else:
        plt.scatter(X[:,0],X[:,1],c = labels)
    
def reduce_plot(mat,kd_func, red_func,cluster_func = None, *args):
    """Does dimensionality reduction on a matrix and plots
    Input:
        mat: n_docs x n_topics matrix to be reduced
        kd_func: kernel (or dissimilarity) function to use.
                Use kernel function for KPCA, dissimilarity for MDS
        red_func: sklearn object to do the reduction. Has fit_transform 
                    method. 
        *args: arguments for kd_func
        cluster_func: sklearn object to do clustering to get labels.
                    has fit_predict method. Defaults to None
    """
    
    #get kernel/dissimilarity matrix
    KD = kd_func(mat,*args)
    #do dimensionality reduction
    X = red_func.fit_transform(KD)
    #cluster
    if cluster_func is not None:
        labels = cluster_func.fit_predict(mat)
    else:
        labels = None
    #plot
    plot(X,"2-D Viz",labels)