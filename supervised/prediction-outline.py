# -*- coding: utf-8 -*-
"""
Created on Thu Jul 16 17:07:16 2015

@author: elliott
"""

from __future__ import division
import numpy as np
from scipy import stats
import pandas as pd
from sklearn.cross_decomposition import PLSRegression

def fe_transform(x, fes, df, newx=None, tol=10 ** -8):
    
    demean = lambda a: a[newx] - a[newx].mean()
    
    df = df.dropna(subset=[x])
    if newx is None:
        newx = 'c'+x
    oldx = 'old'+x
    df[newx] = df[x]
    df[oldx] = df[x]+ - 1
    while np.sqrt(np.sum(df[newx] - df[oldx]) ** 2) >= tol:       
        print np.sum(df[newx] - df[oldx]) ** 2
        df[oldx] = df[newx]
        for fe in fes:
            #key = lambda a: a[fe]
            df[newx] = df.groupby(fe).transform(demean)
    print np.sum(df[newx] - df[oldx]) ** 2
    return df                         

     
def featurewise_ols(y, featureids, cur, xtable, btable):            
    """
    y is the response variable (numpy array), indexed by docid
    featureids is the list of featureid indexes
    cur is a psycopg2 cursor
    xtable is the name of the psql table with feature frequencies/scores
    btable is the name of the psql table for inserting estimates    
    """
    
    cur.execute('DROP TABLE IF EXISTS ' + btable)
    cur.execute('CREATE TABLE ' + btable + """ (
                    featureid INT,
                    beta FLOAT,
                    se FLOAT)""")
                            
    numdocs = len(y)        
        
    for featureid in featureids:        
        
        x = np.zeros(numdocs) # initialize to an array of zeros
        
        # fill in feature counts where the feature appears
        cur.execute("SELECT (docid,freq) FROM "
                    +xtable
                    +" WHERE featureid = %s" (featureid,))                    
        for (docid,freq) in cur:
            x[docid] = freq
        
        # OLS regression
        coeff , _ , _ , _ , se = stats.linregress(x, y)
        
        cur.execute('INSERT INTO ' + btable + ' VALUES (%s,%s,%s)', (featureid, coeff , se))



def select_predictors(intable,outfile,int2gram,cur,numpredictors=30000):
        
    data = []
        
    cur.execute('select * from ' + intable)
    for row in cur:
        featureid, beta, se = row
        tstat = beta / se
        abststat = abs(tstat)
        data.append([abststat,featureid, tstat])
       #print [abststat,gram, tstat]
    
    data.sort()
    data.reverse()
    
    return data[:numpredictors]
                
def predictors_dataset(y,intable,cur,docids,featureids):
                       
    #print len(doclist)
    output = []  
    
    for docid in docids:
        #print cik, year
        outdict = {'docid':docid,'y':y}
        for igram in featureids:
            outdict['g_'+str(igram)] = 0
        cur.execute("""SELECT igram,freq
                		 FROM """ + intable + """ 
                		  WHERE docid = %s""", (docid,))

        for row in cur:
            igram, freq = row
            outdict['g_'+igram] = freq            
            #print igram, freq           
                       
            
        output.append(outdict)                    

    df = pd.DataFrame(output)
    return df


def test_train(df,trainprop):
    
    df['random'] = np.random.rand(len(df))
    df.sort(columns=['random'], inplace=True)
    df.drop(['random'],axis=1,inplace=True)
    dftrain = df[:round(len(df)*trainprop)]
    dftest = df[round(len(df)*trainprop):]
    
    return dftrain, dftest

def pls_train(X,y,numcomponents):
    
    pls = PLSRegression(n_components=numcomponents)
    pls.fit(X, y)    
    return pls

               
def pls_predict(X,pls):
    
    ypred = pls.predict(X)[:,0]
    return ypred

from scipy.stats.stats import ks_2samp    
import matplotlib.pyplot as plt
def pls_test(ytrue, ypred, pls, bins=100):
    
    plt.hist(ytrue, bins=bins, normed=True, alpha=.5)
    plt.hist(ypred, bins=bins, normed=True, alpha=.5)
    
    return ks_2samp(ytrue,ypred)
    
    