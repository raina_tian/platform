# -*- coding: utf-8 -*-
"""
Code for fitting and evaluating a Partial Least Squares Regression

@author: Eli
"""

from sklearn import cross_decomposition
import load_data
from sklearn import cross_validation
import numpy as np
from scipy.stats.stats import ks_2samp    
from scipy import stats
from sklearn import grid_search
from sklearn.cross_decomposition.pls_ import _PLS
from scipy import linalg
from sklearn import utils
import matplotlib.pyplot as plt


import sys
sys.path.append("../5-visualization")
import wordclouds


class PLSReg(cross_decomposition.PLSRegression):
    """PLSRegression with SVD Algorithm"""
    
    def __init__(self, n_components=2, scale=True,
                 max_iter=500, tol=1e-06, copy=True):
        cross_decomposition.PLSRegression.__init__(self,n_components,scale,
                                                   max_iter,tol,copy)  
                      
    def predict(self,X,n_components = None):
        """Predict values for X using the first n_components
        Input:
            X: feature matrix
            n_compoennets: number of latent components to use, defaults to 
                            all components
        Output:
            Ypred: predicted values
        """
        if n_components is None:
            n_components = self.n_components
        p = X.shape[1]
        x_weights_ = self.x_weights_[:, :n_components]# W
        x_loadings_ = self.x_loadings_[:, :n_components]# P
        y_loadings_ = self.y_loadings_[:, :n_components]  # Q
        #get coef matrix
        # rotations from input space to transformed space (scores)
        # T = X W(P'W)^-1 = XW* (W* : p x k matrix)
        # U = Y C(Q'C)^-1 = YC* (W* : q x k matrix)
        x_rotations_ = np.dot(
            x_weights_,
            linalg.pinv(np.dot(x_loadings_.T, x_weights_)))
        coef_ = np.dot(x_rotations_, y_loadings_.T)
        coef_ = (1. / self.x_std_.reshape((p, 1)) * coef_ *
                          self.y_std_)   

        # Normalize
        X -= self.x_mean_
        X /= self.x_std_
        Ypred = np.dot(X, coef_)
        return(Ypred + self.y_mean_)
        
    def score(self,X,y,n_components = None):
        """Returns the mean square error (PRESS) when predicting y from X
        Input:
            X: feature matrix
            y: target values
            n_compoennets: number of latent components to use, defaults to 
                            all components
        Output:
            mse: mean square error
        """
        #ensure 2d array
        y = utils.check_array(y)
        #get prediction
        ypred = self.predict(X,n_components)
        #calculate mse
        mse = np.mean((y - ypred)**2)
        return(mse)

def test_train(mat,meta,target,test_prop = .25):
    """Split data into test and train data
    Input:
        mat: feature matrix
        meta: meta data as pandas dataframe
        target: name of column of dataframe that has target variable
        test_prop: proportion of data to make test, defaults to .25
    """
    #get target data    
    y = load_data.get_target(meta,target)
    #split data
    
    matTrain,matTest,yTrain,yTest = \
        cross_validation.train_test_split(mat,y,test_size = test_prop)
                                                                      
    return(matTrain,matTest,yTrain,yTest)
    
    

def train(X,y,num_components = 2):
    """Trains a PLS Regression
    Input:
        X: feature matrix (dense!)
        y: target vector
        num_components: number of components for pls regression
    Output:
        pls: fitted PLSRegression object
    """
    #initialize PLSRegression    
    pls = cross_decomposition.PLSRegression(num_components)
    #train
    pls.fit(X,y)
    
    return(pls)
    
    
def predict(X,pls):
    """Returns prediciton of PLS Regression
    Input:
        X: Input feature matrix
        pls: trained PLSRegression Object
    Output:
        ypred: predicted output
    """
    
    ypred = pls.predict(X)[:,0]
    return(ypred)
    
def score(X,ytrue,pls):
    """Gets the prediction from a trained PLSRegresison and evaluates
    Input:
        X: feature matrix
        y: true value of target variable
        pls: traiend PLSRegression object
    Output:
        rsq: r squared value
        mean_log_diff: average log difference between truth and prediction
        ks_test_stat: 2 sample Kolmogorov–Smirnov test statistic
        p: p-value of above test"""
    #get predictions
    ypred = pls.predict(X)[:,0]
    #get sum of square error and total sum of squares
    sse = ((ytrue-ypred)**2).sum()
    ssto = ((ytrue - ytrue.mean())**2).sum()
    #get rsq
    rsq = 1 - sse/ssto
    
    #get mean log difference
    mean_log_diff = np.mean(np.log(np.abs(ytrue - ypred)))
    
    #get 2 sample ks test stat
    ks_test_stat,p = ks_2samp(ytrue,ypred)
    
    return(rsq,mean_log_diff,ks_test_stat,p)
    
def tune(X,y,max_comp = 10,folds = 5):
    """Tunes the number of components hyper parameter with a randomized 
        grid search and cross validation
    Input:
        X: feature matrix of training sample
        y: target vector of training sample
        max_comp: max number of components to consider
        folds: number of folds for cross validation. Defaults to 5
    Output:
        pls: trained PLSRegression with bet number of componenets
        results: the results of the search
    """
    """
    #randomly choose n_components between 1 and number of features / 2
    params = {"n_components":stats.randint(1,10)}
    pls = cross_decomposition.PLSRegression()
    #initialize randomized search
    grid = grid_search.RandomizedSearchCV(pls,params,cv = folds,verbose = 3,
                                          n_iter = n_iter)
    #fit
    grid.fit(X,y)
    
    #return best estimator and results
    return(grid.best_estimator_,grid.grid_scores_)
    """
    #kfold cv iterator
    kfold = cross_validation.KFold(X.shape[0],folds)
    #initialize PLSReg object
    reg = PLSReg(max_comp)
    #score matrix. rows are results from one fold, columns are folds
    scores = np.zeros((max_comp,folds))
    #go through folds
    for i,(train_ind,val_ind) in enumerate(kfold):
        #train
        reg.fit(X[train_ind,:],y[train_ind])
        #get mse for each number of componenets
        for c in range(1,max_comp + 1):
            score = reg.score(X[val_ind,:],y[val_ind],c)
            scores[c-1,i] = score
    return(scores)
    
    
def get_loadings(X,y,n_components = 2,n_loadings = 2):
    """Fits a PLSReg and returns the loadings associated with each gram
    Input:
        X: feature matrix
        y: target vector
        n_components: number of components for pls model. Defualts to 2
        n_loadings: number of components to get loadings for
    Output:
        loadings: n_features x n_loadings matrix with the loadings for each 
            feature for each of the first n_loadings components
    """
    #fit pls reg
    plsreg = PLSReg(n_components)
    plsreg.fit(X,y)
    #get loadings
    loadings = plsreg.x_loadings_
    #only keep first n_loadings components
    loadings = loadings[:,:n_loadings]
    return(loadings)
    
def wordcloud_loadings(X,y,fnames,outfile,n_components = 2,n_loadings = 2):
    """Fits a PLSReg and makes a word cloud of the loadings
    Input:
        X: feature matrix
        y: target vector
        fnames: feature names
        outfile: file to figure to
        n_components: number of components for pls model. Defualts to 2
        n_loadings: number of components to get loadings for
    """    
    #get loadings
    loadings = get_loadings(X,y,n_components,n_loadings)
    #get absolute values
    loadings = np.abs(loadings)
    #set up plot
    fig = plt.figure()
    #go through each component and make word cloud
    for i in range(n_loadings):
        print(i)
        #make axes
        ax = fig.add_subplot(n_loadings,1,i + 1)
        #get wordcloud
        wc = wordclouds.cloud(grams = fnames,weights = loadings[:,i])
        ax.imshow(wc)
        ax.axis("off")
        comp_num = i +1
        ax.set_title("Component %i" %comp_num)
    
    #save figure
    plt.savefig(outfile)
