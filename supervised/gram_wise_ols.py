# -*- coding: utf-8 -*-
"""
Code for gram-wise OLS

@author: Eli
"""


import load_data
import numpy as np
from scipy import stats
import pandas as pd
from scipy import sparse
import os
from six.moves import cPickle as pickle
import argparse

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s\t%(message)s')

def f_wise_ols(meta,fmat,fnames,y,thresh = 5):
    """Runs 1-dim'l OLS for each feature in fmat on target variable y
    Input:
        meta: meta data as pandas dataframe
        fmat: (sparse) feature matrix
        fnames: names of the features
        y: name of column in meta that is the target varaible
        thresh: Don't use features with less than thresh non zero elements
                in the feature vector
    Output:
        output: pandas dataframe where each row is [fname,beta,se]
    """
    #isolate target
    y = meta[y]
    #convert fmat into a csc matrix if it isn't already
    #or if it is a dense numpy array, keep it that way
    if type(fmat) != sparse.csc.csc_matrix or type(fmat) != np.ndarray:
        fmat = fmat.tocsc()
    #go through features and perform ols
    #output = np.zeros((fmat.shape[1],3))
    output = pd.DataFrame(index=np.arange(0, fmat.shape[1]),
                          columns = ["col","feature_name","beta","se","tstat"])
    for i in range(fmat.shape[1]):
        #ensure there are more than thresh nonzero elements
        if fmat[:,i].nnz >= thresh:
            #get feature name
            fname = fnames[i]
            #get column of fmat
            x = fmat[:,i].toarray().T[0]
            #do ols
            beta,_,_,_,se = stats.linregress(x,y)
            #calculate absolute value of t statistic
            tstat = abs(beta / float(se))
            if i % 500 == 0:
                logging.info("%i,%s,%f,%f,%f" %(i,fname,beta,se,tstat))
            #add to output
            output.iloc[i] = [i,fname,beta,se,tstat]
    #sort the output by tstat
    output = output.sort("tstat",ascending = False)
    #drop na values
    output = output.dropna()
    return(output)

def f_reduce(f_imps,fmat,num = 50000):
    """Reduces the feature space of fmat by using the top num features by tstat
    Input:
        f_imps: pandas DataFrame from f_wise_ols
        fmat: feature matrix
        num: dimension of feature space
    output:
        fmat: dimension reduced feature space
    """
    
    #sort f_imps by tstat just in case it isn't already
    f_imps  = f_imps.sort("tstat",ascending = False)
    #create boolean mask
    mask = np.zeros(fmat.shape[1],dtype = bool)
    #go through f_imps and get the column indices of the highest ranking 
    #features
    
    for i in range(num):
        row = f_imps.iloc[i]
        col = row["col"]
        #check if col is NAN, if it is, then stop
        if np.isnan(col):
            break
        mask[col] = True
    
    #flatten mask
    mask = np.flatnonzero(mask)
    
    #convert to csc
    fmat = fmat.tocsc()
    
    #apply mask
    
    fmat = fmat[:,mask]
    
    #return fmat
    
    return(fmat)

    

def main(feat_path,meta_path,fid_col,y,thresh = 5,num = 50000):
    """Main method
    Input:
        feat_path: path to directory which holds feature files
        meta_path: path to meta data
        fid_col: name of column which has file ids in the meta data file
        y: name of column in meta that is the target varaible
        thresh: Don't use features with less than thresh non zero elements
                in the feature vector 
        num: dimension of feature space after reducing (number of feautres
                to keep)
    """
    #load data
    fmat,fnames,file_names,meta = load_data.load_reorder(feat_path,
                                                         meta_path,
                                                         fid_col)
    #do gram-wise ols
    output = f_wise_ols(meta,fmat,fnames,y,thresh)
    
    #save output
    outdir = os.path.join(os.path.split(meta_path)[0],"ols")
    #create directory if if it doesn't exist
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    #save feature importance data
    with open(os.path.join(outdir,"ols_output.pkl"),"wb") as f:
        pickle.dump(output,f,protocol = 2)
    #reduce feature space
    fmat = f_reduce(output,fmat,num)
    #save reduced feature space
    with open(os.path.join(outdir,"reduced.pkl"),"wb") as f:
        pickle.dump(fmat,f,protocol = 2)
    
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Runs gram-wise OLS")
    parser.add_argument("feat_path",type = str, help = "path to directory which\
                        holds pickled feature files")
    parser.add_argument("meta_path",type = str, help = "path to meta data")
    parser.add_argument("fid_col",type = str, help = "name of column which has\
                         file ids in the meta data file")
    parser.add_argument("y",type = str, help = "name of column in meta that is \
                        the target varaible")
    parser.add_argument("--thresh",dest = "thresh", help = "Don't use features\
                         with less than thresh non zero elements in the \
                         feature vector",type = int, default = 5 )
    parser.add_argument("--num",dest = "num", help = "Number of features to \
                        keep",type = int, default = 50000 )                         
    args = parser.parse_args()                     
    feat_path = args.feat_path
    meta_path = args.meta_path
    fid_col = args.fid_col
    y = args.y
    thresh = args.thresh
    num = args.num
    
    main(feat_path,meta_path,fid_col,y,thresh,num)
            