# -*- coding: utf-8 -*-
"""
Scripts for loading data and matching with feature extraction output

@author: Eli
"""


import numpy as np
import pandas as pd
import os
import six
from scipy import sparse
from six.moves import cPickle as pickle


def load_csv(csv,sep = ","):
    """Loads a csv as a pandas dataframe
    Input:
        csv: a file path to a csv file
    Output:
        dat: a pandas data frame
    """
    
    dat = pd.read_csv(csv)
    return(dat)
    
def load(pkl):
    """Loads a pickle file
    Input: 
        pkl: a file path to a pickle file
    Output:
        output: contents of pickle file
    """
    
    with open(pkl,"rb") as f:
        output = pickle.load(f)
    
    return(output)

def load_features(path):
    """Loads the feature matrix, feature names, and file names
    Input:
        path: path to directory which holds the pickle files
    Output:
        f_mat: feature matrix
        f_names: feature names
        file_names: file names
    """
    #load files
    f_mat = load(os.path.join(path,"feature_mat.pkl"))
    f_names = load(os.path.join(path,"feature_names.pkl"))
    file_names = load(os.path.join(path,"file_names.pkl"))   
    
    return(f_mat,f_names,file_names)
    

def reorder(dat,fmat,rows,file_col):
    """Reorders a dataframe and feature matrix so the rows correspond
    Input:
        dat: pandas dataframe
        fmat: feature matrix
        rows: names of rows in feature matrix
        file_col: the column of dat which has the file ids
    Output:
        dat: pandas dataframe after reordering
        fmat: feature matrix after reordering
        rows: names of rows in feature matrix after reordering
    """
    #convert fmat into a csr matrix if it isn't already
    #or if it is a dense numpy array, keep it that way
    if type(fmat) != sparse.csr.csr_matrix or type(fmat) != np.ndarray:
        fmat = fmat.tocsr()
    #if there are more rows in the dataframe, delete the ones not in fmat
    if dat.shape[0] > fmat.shape[0]:
        mask = np.ones(dat.shape[0],dtype = bool)
        #go through the rows of dat and delete those which have documents
        #not in fmat (ids are not in rows)
        ids = dat[file_col]
        #get rows as a set
        sr = set(rows)
        for i,fid in enumerate(ids):
            if fid not in sr:
                mask[i] = False
        #use the mask on dat
        dat = dat[mask]
    #if there are more rows in fmat than in dat, delete the ones that aren't
    #in dat
    elif fmat.shape[0] > dat.shape[0]:
        mask = np.ones(fmat.shape[0],dtype = bool)
        #go through rows and see if they're in dat[file_col]
        #get dat[file_col] as a set
        sr = set(dat[file_col])
        for i,fid in enumerate(rows):
            if fid not in sr:
                mask[i] = False
        #delete rows of fmat that aren't in dat
        
        rows = [row for (row,bit) in zip(rows,mask) if bit]
        mask = np.flatnonzero(mask)
        fmat = fmat[mask,:] 
        
        
        
        
    #reorder dat so that the rows correspond with fmat
    #create a id -> row index dict for dat
    id2row = {fid:idx for (idx,fid) in enumerate(dat[file_col])}
    #get new row indexes from fmat
    row_locs = [id2row[row] for row in rows]
    #row_locs = [dat[dat[file_col] == row].index.tolist()[0] for 
    #            row in rows]
    dat = dat.reindex(row_locs)
    
    return(dat,fmat,rows)
        
            
def load_reorder(feat_path,meta_path,fid_col):
    """Loads features and metadata, and makes the orderings correct
    Input:
        feat_path: path to directory which holds feature files
        meta_path: path to meta data
        fid_col: name of column which has file ids in the meta data file
    Output:
        fmat: feature matrix
        fnames: feature names
        file_names: names of files
        dat: meta data
    """
    #load data
    dat = load_csv(meta_path)
    fmat,fnames,file_names = load_features(feat_path)
    #reorder
    dat,fmat,file_names = reorder(dat,fmat,file_names,fid_col)
    
    return(fmat,fnames,file_names,dat)


def get_target(dat,target_name):
    """Gets the column of a dataframe that has the target variable   
    Input:
        dat: dataframe
        target_name: name of column with target varaible
    Output:
        y: target as numpy array
    """
    
    y = np.array(dat[target_name])
    
    return(y)
