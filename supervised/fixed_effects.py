# -*- coding: utf-8 -*-
"""
Code for Fixed Effects transform by alternating projections

@author: Eli
"""
import numpy as np
import copy
import pandas as pd
import load_data
from scipy import sparse
from sklearn import preprocessing
import os
from six.moves import cPickle as pickle
import argparse
import time

def center(x):
    """Subtracts the mean from x
    Input:
        x: vector to center
    Output:
        d: centered x
    """
    return(x - x.mean())


def fe(x,fes,tol = 10 ** -8):
    """Computes the projection in algorithm 3.1 of 
    http://www.sciencedirect.com/science/article/pii/S0167947313001266
    using alternating projections
    Input:
        x: vector to find projection of
        fes: dataframe with categorical columns
        tol: tolerance
    Output:
        x_proj: projected x
    """
    #get fixed effects names
    names = list(fes.columns.values)
    #copy dataframe
    df = fes.copy()
    #store the x as x+1 so the difference will be less than tol
    df["x"] = x + 1
    #store the new x as x
    df["x_new"] = x 
    #terminate when the norm of the difference is less than tol
    while np.linalg.norm(df["x_new"] - df["x"]) > tol:
        df["x"] = df["x_new"]
        #go through each fixed effect variable and center
        for fe in names:
            df["x_new"] = df.groupby(fe)["x_new"].transform(center)
            
    return(np.array(df["x_new"]))
    
def fe_transform(fmat,meta,fes,target,tol = 10 ** -8):
    """Does the fixed effects transformation on data
    Input:
        fmat: sparse feature matrix
        meta: meta data
        fes: categorical variables to use as fixed effects
        target: value to be predicted
    Output:
        fmat_center: centered feature matrix
        y_center: centered target vector
    """
    #isolate fixed effects columns
    effects = meta[fes]
    #convert matrix to csc
    fmat = fmat.tocsc()
    #go through columns and do transformation
    for i in range(fmat.shape[1]):
        print(i)
        #get column as dense vector
        x = fmat[:,i].toarray().T[0]
        #do transformation
        x = fe(x,effects,tol)
        x = x.reshape((fmat.shape[0],1))
        #put column back into feature matrix
        fmat[:,i] = sparse.csr_matrix(x)
    #center target vector
    
    y = fe(np.array(meta[target]),effects,tol)
    
    return(fmat,y)
    
def fe_transform2(fmat,meta,fes,target):
    """Computes the projections explicitly
    Input:
        fmat: sparse feature matrix
        meta: meta data
        fes: categorical variables to use as fixed effects
        target: value to be predicted
    Output:
        fmat_proj: projected feature matrix
        y_proj: projected target vector"""
    #one hot encoder
    onehot = preprocessing.OneHotEncoder()
    #turn the categorical values into One Hot Encoding, this is the D matrix
    cat = meta[fes]
    D = onehot.fit_transform(cat)
    #calculate projection matrix
    I = sparse.eye(D.shape[0]) 
    tmp = D.dot(sparse.linalg.inv(D.T.dot(D)).dot(D.T))
    P = I - tmp
    #calculcate projection of feature matrix and y
    fmat_proj = P.dot(fmat)
    y = np.array(meta["target"])
    y_proj= P.dot(y)

    return(fmat_proj,y_proj)
    


def fe_transform3(fmat,meta,fes,target,tol = 10 ** -8):
    """Does fixed effects tranformation on data with all features at a time
    Input:
        fmat: sparse feature matrix
        meta: meta data
        fes: categorical variables to use as fixed effects
        target: value to be predicted
        tol: tolerance
    Output:
        fmat_proj: projected feature matrix
        meta_proj: metadata with projected target vector"""
        
    #get the unique categories for each fixed effect variable
    cats = {fe:np.unique(meta[fe]) for fe in fes}
    meta = meta.copy()
    fmat_proj = fmat
    #do transformation on y
    diff = 1
    y = meta[target]
    while diff > tol:
        y_old = y.copy()
        for fe in fes:
            #group by fixed effect
            gb = meta.groupby(fe)
            new = gb[target].transform(center)
            meta[target] = new
        #check norm
        diff = np.linalg.norm(meta[target] - y_old)
    meta_proj = meta
    #copy fmat
    fmat = fmat.copy()
    diff = 1
    it = 0
    it2 = 0
    while diff > tol: 
        print(it)
        it += 1
        #copy old version of fmat
        fmat_old = fmat.copy()
        #de mean by each fixed effect variable
        for fe in fes:
            #create new sparse matrix
            new = sparse.lil_matrix(fmat.shape)
            #group by the fixed effect
            gb = meta.groupby(fe)
            #go through each grou[ and de mean
            for cat in cats[fe]:
                print(it2,cat)
                it2 +=1
                #get the indices of the rows in each group
                idx = gb.get_group(cat).index
                new[idx,:] = fmat[idx,:] - fmat[idx,:].mean(0)
            #set fmat to new
            fmat = new.tocsr()
        #check the norms of the differences from new and old and if the max 
        #norm is less than the tolerance, stop
        d = fmat - fmat_old
        d = d.tocsc()
        diff = min(np.norm(d[:,i]) for i in range(d.shape[1]))
        fmat = fmat.tocsr()

    return(fmat_proj,meta_proj)


class FeTrans:
    
    def __init__(self,fmat,meta,fes,target,tol = 10 ** -4):
        """Constructor
        Input:
            fmat: sparse feature matrix
            meta: meta data
            fes: categorical variables to use as fixed effects
            target: value to be predicted
            tol: tolerance
        Output:
            fmat_proj: projected feature matrix
            meta_proj: metadata with projected target vector"""        
        
        self.fmat = fmat.copy()
        self.meta = meta.copy()
        self.fes = fes
        self.target = target
        self.tol = tol
        self.new = sparse.lil_matrix(fmat.shape)
        
        
    def demean(self,x):
        """Function to be put into apply. Demeans feature matrix"""
        #get indices
        idxs = x.index       
        #de mean
        self.new[idxs,:] = self.fmat[idxs,:] - self.fmat[idxs,:].mean(0)
        return(1)
        
    def center(self,x):
        """Centers x"""
        return(x - x.mean())
        
    def fe_transform(self):
        #first transform target vector
        diff = 1
        while diff > self.tol:
            y_old = np.array(self.meta[self.target])
            #go through fixed effect variables
            for fe in self.fes:
                #group by variable
                gb = self.meta.groupby(fe)
                new = gb[self.target].transform(center)
                self.meta[self.target] = new
                
                
            #check norm
            diff = np.linalg.norm(self.meta[self.target] - y_old)
            print(diff)
        
        #store demeaned data
        meta_proj = self.meta
        
        #transform feature matrix
        diff = 1
        i = 0
        while diff > self.tol:
            fmat_old = self.fmat.copy()
            #keep track of iteration
            print(i)
            i += 1
            #go through fixed effects
            for fe in self.fes:
                #group by fixed effect
                gb = self.meta.groupby(fe)
                #use apply to demean
                t1 = time.time()
                gb.apply(self.demean)
                t2 = time.time()
                #print(t2-t1)
                #set self.fmat to self.new
                t3 = time.time()
                self.fmat = self.new.tocsr()
                self.new = sparse.lil_matrix(self.fmat.shape)
                t4 = time.time()
                #print(t4 - t3)
            #Use frobenius norm of diff_mat to check the size of the difference
            diff_mat = self.fmat - fmat_old
            diff = sparse.linalg.norm(diff_mat)
            print(diff)
        fmat_proj = self.fmat
        
        return(fmat_proj,meta_proj)
            
    
    
    
    
        
    
            
def main(feat_path,meta_path,fid_col,fes,target,tol = 10 ** -4):
    """Main method
    Input:
        feat_path: path to directory which holds feature files
        meta_path: path to meta data
        fid_col: name of column which has file ids in the meta data file
        fes: categorical variables to use as fixed effects
        target: value to be predicted
        tol: tolerance  """

    #load data 
    fmat,fnames,file_names,meta = load_data.load_reorder(feat_path,
                                                         meta_path,
                                                         fid_col)
    #do fe transform
    fet = FeTrans(fmat,meta,fes,target,tol)
    fmat_proj,meta_proj = fet.fe_transform()
    #save output
    outdir = os.path.join(os.path.split(meta_path)[0],"fe_transform")
    #create directory if if it doesn't exist
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    #save projected feature matrix
    with open(os.path.join(outdir,"fmat_proj.pkl"),"wb") as f:
        pickle.dump(fmat_proj,f,protocol = 2)
    #save projected meta data
    with open(os.path.join(outdir,"meta_proj.pkl"),"wb") as f:
        pickle.dump(meta_proj,f,protocol = 2)
        
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Runs fixed effect transform")
    parser.add_argument("feat_path",type = str, help = "path to directory which\
                        holds pickled feature files")
    parser.add_argument("meta_path",type = str, help = "path to meta data")
    parser.add_argument("fid_col",type = str, help = "name of column which has\
                         file ids in the meta data file")
    parser.add_argument("target",type = str, help = "name of column in meta that is \
                        the target varaible")
    parser.add_argument("fes",type = str, help = "name of columns of fixed \
                        effect variables", nargs = "+")     

    parser.add_argument("--tol",type = int, help = "tolerance of projection",
                        default = 10**-4)
    args = parser.parse_args()                     
    feat_path = args.feat_path
    meta_path = args.meta_path
    fid_col = args.fid_col
    fes = args.fes
    target = args.target
    tol = args.tol
    
    main(feat_path,meta_path,fid_col,fes,target,tol)