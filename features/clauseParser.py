# -*- coding: utf-8 -*-
"""
Parses out clauses in files
"""
import re
from collections import defaultdict
import os
from nltk import sent_tokenize
import fnmatch
from unidecode import unidecode
from six.moves import cPickle as pickle

class clauseParser:
    
    def __init__(self):
        

        match = "([A|a][R|r][T|t][I|i][C|c][L|l][E|e])(\s+\d+|\s+[I|V|X|L|M|C]+)"
        #roman numeral regex
        self.roman = re.compile(r'\b(?!LLC)(?=[MDCLXVI]+\b)M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\b')

        self.romanNumeralMap = (('M',  1000),
                   ('CM', 900),
                   ('D',  500),
                   ('CD', 400),
                   ('C',  100),
                   ('XC', 90),
                   ('L',  50),
                   ('XL', 40),
                   ('X',  10),
                   ('IX', 9),
                   ('V',  5),
                   ('IV', 4),
                   ('I',  1))
        self.sec = re.compile(match) #regular expression for Article/Section
                                        #followed by arabic or roman numerals
        self.clauses = defaultdict(str) #clause dictionary indexed by filename
        self.errors = defaultdict(list) #dictionary which makes note of the
                                        #errors in each file
        self.numeric = re.compile("\d+") #regex for digits
        
    def checkSection(self,string):
        """Returns a regex match searching for an Article or Section in a string"""
        return self.sec.search(string)

    def findClauses(self,lines,fName):
        """Finds the clauses in a file represented as an array of lines"""
        clausedict = defaultdict(str) #dictionary holds all of the clauses
        currClause = 0 #current clause number
        #iterate over lines and add the clauses
        lineNum = 0        
        for line in lines:
            lineNum += 1
            match = self.checkSection(line)
            if match == None:
                #if not a new clause and already on a clause, add the text
                clausedict[currClause] +=  line.strip() + " "
            else:
                #get the article/section number
                romanNum = self.roman.search(match.group())
                if romanNum != None:
                    clauseNum = romanNum.group()
                else:
                    num = self.numeric.search(match.group())
                    if num != None:
                        clauseNum = num.group()
                    else:
                        clauseNum = ''
                if clauseNum != '':
                    #get the number
                    if clauseNum.isdigit():
                        clauseNum = int(clauseNum)
                    else:
                        clauseNum = self.fromRoman(clauseNum)
                    #if the clauseNum is less than the current clause, then 
                    #continue including the line in the current clause


                    #if the next clause number is not one more than the previous
                    #make a note of it and add a new clause to the clause dict
                    #if the next clause number is not one more than the previous
                    #make a note of it and add a new clause to the clause dict                    
                    if clauseNum != currClause + 1:
                        self.errors[fName].append(lineNum)
                            
                    start = match.start()
                    end = match.end()
                    #add everything before the new section starts to the old clause
                    clausedict[currClause] += line[:start]
                    #add everything after the section 
                    #make currClause the clauseNum that was found
                    currClause = clauseNum
                    clausedict[currClause] += line[start:].strip() + " "
                else:
                    #if a new clause add 1 to clause number and add to new clause
                    start = match.start()
                    end = match.end()
                    #add everything before the new section starts to the old clause
                    clausedict[currClause] += line[:start]
                    #add everything after the section to the 
                    currClause += 1
                    clausedict[currClause] += line[start:].strip() + " "
                
        return clausedict
                
    def allClausesDir(self,directory):
        """Goes through all files in directory and gets the clauses, 
                saves to clauses"""
        #get file names
        fNames = os.listdir(directory)
        #add path to file names
        for i in range(len(fNames)):
            fNames[i] = os.path.join(directory,fNames[i])
        #get clauses from files
        for fName in fNames:
            name = fName[:fName.find(".txt")]
            f = open(fName,"r")
            lines = f.readlines()
            f.close()
            self.clauses[name] = self.findClauses(lines,name)
    
    def allClauses(self,directory):
        """Uses os.walk() to recursively walk through the directory and all 
            subdirectories gets the clauses for the .txt files found"""
        #Walk through subdirectories and save .txt file names
        files = []
        for root,dirnames,filenames in os.walk(directory):
            for filename in fnmatch.filter(filenames, '*.txt'):
                 files.append(os.path.join(root, filename))
        
        #get clauses from files
        for fName in files:
            f = open(fName,"r")
            lines = f.readlines()
            f.close()
            key = fName.split('/')[-1][:-4]
            self.clauses[key] = self.findClauses(lines,fName)
    

        
    def toTokens(self):
        """Returns a a dictionary with the same structure as clauses except
            each clause is an array of sentences where each sentence is tokenized"""
        tokens = defaultdict(str)
        for key in self.clauses.keys():
            tokens[key] = defaultdict(list)
            for num in self.clauses[key].keys():
                #get rid of all numeric values
                numeric = re.compile("\d+")
                txt = self.clauses[key][num]
                #split on periods to get sentences
                txt = sent_tokenize(unidecode(txt))
                #check if last element is empty
                #tokenize each sentence and add to tokens dict
                for sent in txt:
                    words = [w for w in re.findall(r"[a-z]+-?[a-z]+", sent.lower(),flags = re.UNICODE | re.LOCALE)]                    
                    seen = set()
                    seen_add = seen.add
                    words = [w for w in words if not (w in seen or seen_add(w))]                    
                    if len(words) < 4:
                        continue
                    tokens[key][num].append(words)
        return tokens
        
        
        

    def fromRoman(self,s):
        """convert Roman numeral s to integer"""
    
        result = 0
        index = 0
        for numeral, integer in self.romanNumeralMap:
            while s[index:index+len(numeral)] == numeral:
                result += integer
                index += len(numeral)
        return result
"""                        
cl = clauseParser()
cl.allClauses(directory='data/1a-cbatext/education-text')    
tokens = cl.toTokens()
pickle.dump(tokens, open('data/3b-clauses/teacher-clauses.pkl','wb'))
"""
