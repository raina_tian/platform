# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 23:32:26 2015

@author: elliott
"""

from __future__ import division
from gensim.models.phrases import Phrases
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
from gensim import utils
import numpy as np
from collections import defaultdict
from nltk import ngrams
from nltk.stem.snowball import SnowballStemmer
logger = logging.getLogger(__name__)
from six import iteritems
from textblob_aptagger import PerceptronTagger
from nltk.corpus import stopwords as stop

class TagTokens(PerceptronTagger):
    "tag the words in a sentence"
    def tag(self, words):
        prev, prev2 = self.START
        tags = []
        context = self.START + [self._normalize(w) for w in words] + self.END
        for i, word in enumerate(words):
            tag = self.tagdict.get(word)
            if not tag:
                features = self._get_features(i, word, context, prev, prev2)
                tag = self.model.predict(features)
            tags.append(tag)
            prev2 = prev
            prev = tag
        return tags

class PosPhrases(Phrases):

    def __init__(self, sentences=None, min_count=5, threshold=None,
            max_vocab_size=40000000, delimiter=b'_', tagpatterns=None):
        """
        `keeptags` is the set of part-of-speech tags to bigramize
        """
        if min_count <= 0:
            raise ValueError("min_count should be at least 1")

        #if threshold <= 0:
        #    raise ValueError("threshold should be positive")

        self.min_count = min_count
        if threshold is not None:
            self.threshold = threshold
        else:
            self.threshold = {2 : .04, # PMI threshold for bigrams
                              3 : .02} # PMI threshold for trigrams
        self.max_vocab_size = max_vocab_size
        self.vocab = defaultdict(int)  # mapping between utf8 token => its count
        self.min_reduce = 1  # ignore any tokens with count smaller than this
        self.delimiter = delimiter
        self.tagger = TagTokens()
        self.snow = SnowballStemmer("english")
        self.stopwords = set()#stop.words('english')
        if tagpatterns is not None:
            self.tagpatterns = tagpatterns
        else:
            self.tagpatterns = set(['AN','NN','AAN','ANN','NAN','NPN',
                                'VN','VAN','VNN','VPN','ANV','NVV','VDN'])

        self.tagset = set(''.join(self.tagpatterns))


        self.tagdict = defaultdict(str,
                        {'NN':'N',
                        'NNS':'N',
                        'NNP':'N',
                        'NNPS':'N',
                        'JJ':'A',
                        'JJR':'A',
                        'JJS':'A',
                        'VBG':'A',
                        'RB':'A',
                        'DT':'D',
                        'IN':'P',
                        'TO':'P',
                        'VB':'V',
                        'VBD':'V',
                        'VBN':'V',
                        'VBP':'V',
                        'VBZ':'V'})

        if sentences is not None:
            self.add_vocab(sentences)

    def learn_vocab(self, sentences):
        """Collect unigram/bigram counts from the `sentences` iterable."""
        sentence_no = -1
        total_words = 0
        #logger.info("collecting all words and their counts")
        vocab = defaultdict(int)
        min_reduce = 1
        for sentence_no, sentence in enumerate(sentences):
            #if sentence_no % 10000 == 0:
                #logger.info("PROGRESS: at sentence #%i, processed %i words and %i word types" %
                #            (sentence_no, total_words, len(vocab)))
            sentence = [utils.any2utf8(w) for w in sentence]

            # get part-of-speech tags
            tags = [self.tagdict[x] for x in self.tagger.tag(sentence)]

            # only keep the parts of speech in the tag set
            for i in range(len(sentence)):
                tag = tags[i]
                if tag in self.tagset and sentence[i] not in self.stopwords:
                    sentence[i] = self.snow.stem(sentence[i]) # stem if kept
                else:
                    sentence[i] = None

            # add single words
            for w in sentence:
                if w is not None:
                    vocab[w] += 1

            # add bigrams
            for w, t in zip(ngrams(sentence,2), ngrams(tags,2)):
                if None not in w:
                    if len(w) == len(set(w)):
                        if ''.join(t) in self.tagpatterns:
                            vocab[self.delimiter.join(w)] += 1

            # add trigrams
            for w, t in zip(ngrams(sentence,3), ngrams(tags,3)):
                if None not in w:
                    if len(w) == len(set(w)):
                        if ''.join(t) in self.tagpatterns:
                            vocab[self.delimiter.join(w)] += 1

#            for w in ngrams(sentence,4):
#                if None not in w:
#                    if len(w) == len(set(w)):
#                        vocab[self.delimiter.join(w)] += 1

        #logger.info("collected %i word types from a corpus of %i words (unigram + bigrams) and %i sentences" %
        #            (len(vocab), total_words, sentence_no + 1))
        return min_reduce, vocab


    def add_vocab(self, sentences):
        """
        Merge the collected counts `vocab` into this phrase detector.

        """
        # uses a separate vocab to collect the token counts from `sentences`.
        # this consumes more RAM than merging new sentences into `self.vocab`
        # directly, but gives the new sentences a fighting chance to collect
        # sufficient counts, before being pruned out by the (large) accummulated
        # counts collected in previous learn_vocab runs.
        min_reduce, vocab = self.learn_vocab(sentences)

        #logger.info("merging %i counts into %s" % (len(vocab), self))
        self.min_reduce = max(self.min_reduce, min_reduce)
        for word, count in iteritems(vocab):
            self.vocab[word] += count
        if len(self.vocab) > self.max_vocab_size:
            prune_vocab(self.vocab, self.min_reduce)
            self.min_reduce += 1

        #logger.info("merged %s" % self)

    def phrase(self, sentence):
        """
        connect noun phrases in sentence fitting tag patterns
        """
        if len(sentence) == 0:
            return None
        tags = [self.tagdict[x] for x in self.tagger.tag(sentence)]

        #words = [self.snow.stem(w.lower()) for w in sentence]
        #I'M ASSUMING HERE THAT THE WORDS ARE ALREADY HOW THEY SHOULD BE
        words = [w for w in sentence]
        oldwords = [utils.any2utf8(w) for w in words]
        words = [utils.any2utf8(w) for w in words]
        sentlength = len(words)
        new_s = []
        #keep track of which tags were merged too
        new_t = []

        skip = 0
        for i in range(sentlength):
            if skip > 0:
                skip -= 1
                continue
            if words[i] is None:
                new_s.append(oldwords[i])
                new_t.append(tags[i])
                continue
            for n in [3,2]:
                if i+n > sentlength:
                    continue
                gram = words[i:i+n]
                
                if None in gram:
                    continue
                pattern = ''.join(tags[i:i+n])
                if pattern not in self.tagpatterns:
                    continue
                gram_word = self.delimiter.join(gram)
                if gram_word in self.vocab:
                    score = self.gmean(gram_word,gram)
                    
                    if score > self.threshold[n]:
                        new_s.append(gram_word)
                        new_t.append(pattern)
                        skip = n-1
                        break
            if skip == 0:
                new_s.append(words[i])
                new_t.append(tags[i])

        return ([utils.to_unicode(w) for w in new_s],new_t)


    def gmean(self, gram_word, gram=None):
        """geometric mean association."""
        if gram is None:
            gram = gram_word.split(self.delimiter)
        n = float(len(gram))
        p = [float(self.vocab[w]) for w in gram]

        pg = float(self.vocab[gram_word])
        #print pg,p,n,np.prod(p),np.prod(p) ** (1/n)
        score = pg / (np.prod(p) ** (1/n))
        #print score

        return score

    def nbest(self, n, num=None):
        """return the highest-scoring grams of length n, with their scores and frequencies"""
        if num is None:
            num = 1000
        output = []
        grams = [g for g in self.vocab if g.count(self.delimiter) == n - 1]
        for g in grams:
            score = self.gmean(g)
            output.append([score,self.vocab[g], g])
        output.sort()
        output.reverse()
        return output[:num]


def prune_vocab(vocab, min_reduce):
    """
    Remove all entries from the `vocab` dictionary with count smaller than `min_reduce`.
    Modifies `vocab` in place.

    """
    old_len = len(vocab)
    for w in list(vocab):  # make a copy of dict's keys
        if vocab[w] <= min_reduce:
            del vocab[w]
    logger.info("pruned out %i tokens with count <=%i (before %i, after %i)" %
        (old_len - len(vocab), min_reduce, old_len, len(vocab)))