# -*- coding: utf-8 -*-
"""
More memory (and time!) efficient code for getting phrased ngrams as a 
corpus iterator

@author: Eli
"""
import nltk
from nltk.stem.snowball import SnowballStemmer
from nltk.stem.porter import PorterStemmer  
from nltk.stem import WordNetLemmatizer
from nltk.tag.stanford import StanfordPOSTagger as POSTagger
from textblob_aptagger import PerceptronTagger
from nltk.corpus import stopwords
from collections import Counter, defaultdict
import string
import csv
import os
import re
import argparse
import logging
import six
from six.moves import cPickle as pickle
import zipfile
from gensim.corpora import dictionary
from gensim import matutils
from gensim import models
import numpy as np
import sys
sys.path.append("..")
from docLoader import *
#import stuff from ngrams
sys.path.append("../ngrams")
from ngrams import APTagger, keydefaultdict
import ngrams_corpus
from pos_phrases import PosPhrases
from sklearn import feature_extraction


#logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s\t%(message)s')

class MultiGramCorpusIterator(ngrams_corpus.CorpusIterator):
    """Class to do tokenization, tagging, stemming, etc. and yield each 
        document 1 at a time. Yields [bigrams,trigrams,quadgrams] for each
        document
    """
    
    def __init__(self, indir,stem = None, stop_words = True,
                 punctuation = True, split_clauses = False,outdir = None):  
        """Constructor
        Input:
            indir: path to directory of txt files
            n: order of n gram
            stem: {'snowball','porter','lemma',None} stemmer to use
                    Defaults to None.
            stop_words: Boolean. include stopwords. Defaults to True
            punctuation: Boolean. include punctuation. Defaults to True
            split_clauses: Boolean. Split on clauses
            outdir: directory to write to. Defaults to indir/phrase_results        
        """
        
        #create outdir
        if not zipfile.is_zipfile(indir):
            if outdir is None:
                outdir = os.path.join(indir,"phrase_results")
            #check if directory exists, if not create direcotry
            if not os.path.exists(outdir):
                os.mkdir(outdir)
        else:
            if outdir is None:
                #get the directory of the zip archive
                tmp = os.path.split(indir)[0]
                outdir = os.path.join(tmp,"phrase_results")
            #cheack if the directory exists, if not create the directory
            if not os.path.exists(outdir):
                os.mkdir(outdir)   
                
        #call super constructor
        super(MultiGramCorpusIterator,self).__init__(indir, 1, stem,
                                                    stop_words,None,None,
                                                    punctuation,split_clauses,
                                                    outdir)
        
    def ngrams_from_sents(self, sents,n, stem = None, stop_words = True, 
                          tag = None, tag_pattern = None, punctuation = True):
        """Overridden from CorpusIterator. Gets the bigrams,trigrams, and
            quadgrams from a list of sentences
        Input:
            sents: list of sentences as strings
            n: order of n gram
            stem: {'snowball','porter','lemma',None} stemmer to use
                    Defaults to None.
            stop_words: Boolean. include stopwords. Defaults to True
            tag: {'ap','nltk','stanford',None}. POS tagger to use. Defaults 
                    to None
            tag_pattern: list of of tag patterns to allow in simplified form.
                         Defaults to None. if tag_pattern = "default", 
                         use default tag pattern.
            punctuation: Boolean. include punctuation. Defaults to True
        Output:
            ngrams: list of bigrams,trigrams,quadgrams each as a list of
                "word1_word2_..." strings
        """
           
        
    
        #tag sentences first
        if tag is not None:
            #tokenize the sentences
            tmp = []
            for sent in sents:
                tmp.append([word.lower() for word in self.word_tokenize(sent)])
            sents = tmp
            if tag == 'ap':
                #tag words
                tags = self.tagger.tag_sents(sents)
                #go from universal/penn treebank tagset to simple tagset
                tags = [[self.tagdict[t] for t in tags[i]] for i in range(len(tags))]
                #join tags and words
                #sents = [['::'.join(tagWord) for tagWord in zip(sents[i],tags[i])] 
                            #for i in range(len(sents))]
            
            elif tag == 'nltk':
                #tag words
                tags = self.tagger.tag_sents(sents)
                #extract the tags without the words
                tags = [[self.tagdict[tagWord[1]] for tagWord in tags[i]] for i \
                        in range(len(sents))]
                #join tags and words
                #sents = [['::'.join(t) for t in tags[i]] for i in range(len(sents))]
            
            elif tag == 'stanford':

               #tag words
               tags = self.tagger.tag_sents(sents)
               #extract the tags without the words
               tags = [[self.tagdict[tagWord[1]] for tagWord in tags[i]] for \
                        i in range(len(sents))]
               #join tags and words
               #sents = [['::'.join(t) for t in tags[i]] for i in range(len(sents))]
               
            else:
                #raise a value error if an unsupproted tagger is included
                raise ValueError('Not an available tagger')
            
        
        
        
        #iterate through sentences and get ngrams
        ngrams = []
        for i,words in enumerate(sents):
            if tag is None:
                #if tag is None then word tokenization hasn't happend
                words = self.word_tokenize(words)
            #stem words if stem is not None
            if stem is not None:
                words = [self.stemmer.stem(word) for word in words]
            #join tags and words if tag is not None
            if tag is not None:
                words = ['::'.join(tagWord) for tagWord in zip(words,tags[i])]
            #remove stop words if stop = False
            if not stop_words:
                words = [word for word in words if not word.split("::")[0] in self.stop]
            #remove punctuation if punctuation is false
            if not punctuation:
                pun = string.punctuation
                words = [word for word in words if not word.split("::")[0] in pun]
            ############MODIFIED FROM CORPUSITERATOR###############
            #get n grams and add to ngrams list
            
            bi = ["_".join(gram) for gram in 
                            self.custom_ngrams(words,2)]
            tri = ["_".join(gram) for gram in 
                            self.custom_ngrams(words,3)]
            quad = ["_".join(gram) for gram in 
                            self.custom_ngrams(words,4)]   
            sent_grams = words + bi + tri + quad
            ############################
            #if tag_pattern isn't None, go through sent_grams and only keep those
            #ngrams with the proper tag pattern
            if tag_pattern is not None:
                #assign default tag pattern if tag_pattern == 'default'
                if tag_pattern == 'default':
                    tag_pattern = self.default_tagpatterns
                
                #maybe make this a list comprehension?
                tmp = []
                for gram in sent_grams[i]:
                    #get the tags separately
                    tags_squash = [t.split("::")[1] for t in gram.split("_")]
                    if ''.join(tags_squash) in tag_pattern:
                        tmp.append(gram)
                sent_grams = tmp
            ngrams.extend(sent_grams)
            
        return(ngrams)        

class PhraseCorpusIterator(six.Iterator):
    """Child class of CorpusIterator which phrases sentences in addition"""
    
    def __init__(self,phraser, indir, stem = None, stop_words = True, 
                 tag = 'ap',allowed_tags = ["N"], punctuation = True,
                 split_clauses = False,outdir = None):
        """Constructor
        Input:
            phraser: trained PosPhrases object
            indir: path to directory or ziparchive
            stem: {'snowball','porter','lemma',None} stemmer to use
                    Defaults to None.
            stop_words: Boolean. include stopwords. Defaults to True
            tag: {'ap','nltk','stanford'}. POS tagger to use. Defaults 
                    to 'ap'
            allowed_tags: tags for unigrams that are allowed. Defaults to
                            Nouns
            punctuation: Boolean. include punctuation. Defaults to True
            split_clauses: Boolean. Split on clauses
        """
        self.indir = indir
        self.phraser = phraser
        
        #check if directory is zip archive or directory and act accordingly
        if not zipfile.is_zipfile(indir):
            #list the files in the directory
            self.files = [os.path.join(indir,f) for f in os.listdir(indir) if \
                        os.path.splitext(f)[1] == ".txt"]
            #create directory for results
            if outdir is None:
                outdir = os.path.join(indir,"phrase_results")
            #check if directory exists, if not create direcotry
            if not os.path.exists(outdir):
                os.mkdir(outdir)
            #set zip_corpus to None
            self.zip_corpus = None
        else:
            #files is the namelist of the zip archive
            self.zip_corpus = zipfile.ZipFile(indir)
            self.files = self.zip_corpus.namelist()
            #crete directory for results in the directory of the zip archive
            if outdir is None:
                #get the directory of the zip archive
                tmp = os.path.split(indir)[0]
                outdir = os.path.join(tmp,"phrase_results")
            #cheack if the directory exists, if not create the directory
            if not os.path.exists(outdir):
                os.mkdir(outdir) 
                
        self.stem = stem
        self.stop_words = stop_words
        self.tag = tag
        self.allowed_tags = allowed_tags
        self.punctuation = punctuation
        self.outdir = outdir
        
        #keep an index for the __next__() function
        self.index= 0        

        #class variable holding default tag patterns and dict for conversion to
        #universal tag set
        self.default_tagpatterns = set(['AN','NN', 'VN', 'VV', 
                                    'VP',                                    
                                    'NNN','AAN','ANN','NAN','NPN',
                                    'VAN','VNN','VPN','ANV','NVV','VDN', 'VVV',
                                    'VVP'                                                                    
                                    ])
        self.default_tagset = set(''.join(self.default_tagpatterns))
        
        
        self.tagdict = keydefaultdict(lambda x: x,
                                {'NN':'N',
                                'NNS':'N',
                                'NNP':'N',
                                'NNPS':'N',
                                'JJ':'A',
                                'JJR':'A',
                                'JJS':'A',
                                'VBG':'A',
                                'RB':'A',
                                'DT':'D',
                                'IN':'P',
                                'TO':'P',
                                'VB':'V',
                                'VBD':'V',
                                'VBN':'V',
                                'VBP':'V',                            
                                'VBZ':'V',
                                'MD': 'V',
                                'RP': 'P'})
        #class variable which contains english stop words as a set
        self.stop = set(stopwords.words('english'))
        
        #set up tagger if tag is not None
        if tag is not None:
            if tag == 'ap':
                #cerate tagger
                self.tagger = APTagger()
            
            elif tag == 'nltk':
                #create a named tuple which holds nltk.pos_tag_sents as 
                #tag_sents
                NLTKTagger = namedtuple("NLTKTagger",["tag_sents","tag"])
                self.tagger = NLTKTagger(nltk.pos_tag_sents,nltk.pos_tag)
            
            elif tag == 'stanford':
                #create stanford tagger
               model = '../../stanford/postagger/models/english-bidirectional-distsim.tagger' 
               jar = '../../stanford/postagger/stanford-postagger-3.5.2.jar'
               self.tagger = POSTagger(model,jar,encoding = 'utf-8')
            else:
                #raise a value error if an unsupproted tagger is included
                raise ValueError('Not an available tagger')


        #initialize stemmer if stem is not None
        if stem is not None:
            if stem == 'porter':
                self.stemmer = PorterStemmer()
            elif stem == 'snowball':
                self.stemmer = SnowballStemmer("english")
            elif stem == 'lemma':
                self.stemmer = WordNetLemmatizer()
                #add stem as another name for lemmatize
                self.stemmer.stem = stemmer.lemmatize
            else:
                #raise a value error if a wrong stemmer is chosen
                raise ValueError('Not an available stemmer')  
        #set splitting on clauses
        self.split_clauses = split_clauses
        #current clauses
        self.curr_clauses = []        

    def __len__(self):
        """len function, number of documents"""
        return(len(self.files))
        
    def __next__(self):
        """Next function for iterator"""
        if self.index >= len(self.files):
            raise StopIteration
        #if not splitting on clauses
        if not self.split_clauses:
            #get sentences from file
            sents = doc_sents(self.files[self.index],zipped = self.zip_corpus)
            #get ngrams of doc and yield 
            ngrams = self.grams_from_sents(sents,self.stem,self.stop_words,
                                            self.tag,self.allowed_tags,
                                            self.punctuation)
            self.index += 1
            return(ngrams)
        else:
            if len(self.curr_clauses) == 0:
                self.curr_clauses = doc_sents(self.files[self.index],
                                         zipped = self.zip_corpus,
                                         clauses = True)
                self.index += 1
            #pop one clause
            sents = self.curr_clauses.pop()
            ngrams = self.grams_from_sents(sents,self.stem,self.stop_words,
                                            self.tag,self.allowed_tags,
                                            self.punctuation) 
            return(ngrams)
         
    
    def __iter__(self):
        """Iterator, does tokenization,stemming,tagging,etc on a doc before
            returning it"""
        if not self.split_clauses:
            for i, fName in enumerate(self.files):
                if i % 100 == 0:
                    logging.info("Computing N-grams for %ith file %s" %(i,fName))            
                #get sentences from file
                sents = doc_sents(fName,zipped = self.zip_corpus)
                #get ngrams of doc and yield 
                ngrams = self.grams_from_sents(sents,self.stem,self.stop_words,
                                            self.tag,self.allowed_tags,
                                            self.punctuation)
                yield(ngrams)
        else:
            for i, fName in enumerate(self.files):
                if i % 100 == 0:
                    logging.info("Computing N-grams for %ith file %s" %(i,fName))            
                #get sentences for clauses
                clauses = doc_sents(fName,zipped = self.zip_corpus,clauses = True)
                #iterate over clauses
                for sents in clauses:
                    #get ngrams of doc and yield 
                    ngrams = self.grams_from_sents(sents,self.stem,self.stop_words,
                                                self.tag,self.allowed_tags,
                                                self.punctuation)
                    yield(ngrams)            

    def word_tokenize(self,words):
        """Faster word tokenization than nltk.word_tokenize
        Input:
            words: a string to be tokenized
        Output:
            tokens: tokenized words
        """
        tokens = re.findall(r"[a-z]+-?[a-z]+", words.lower(),
                            flags = re.UNICODE | re.LOCALE)
        return(tokens)

        
    def grams_from_sents(self, sents, stem = None, stop_words = True, 
                          tag = 'ap', allowed_tags = ["N"], punctuation = True):
        """Gets the unigrams from a list of phrased sentences. 
        Input:
            sents: list of sentences as strings
            stem: {'snowball','porter','lemma',None} stemmer to use
                    Defaults to None.
            stop_words: Boolean. include stopwords. Defaults to True
            tag: {'ap','nltk','stanford'}. POS tagger to use. Defaults 
                    to 'ap's
            punctuation: Boolean. include punctuation. Defaults to True
        Output:
            ngrams: list of ngrams as "word1-word2" strings
        """
           
        
        
        #iterate through sentences and get ngrams
        grams = []
        for i,words in enumerate(sents):
            #tokenize words
            words = self.word_tokenize(words)
            #stem words if stem is not None
            if stem is not None:
                words = [self.stemmer.stem(word) for word in words]
            #remove stop words if stop = False
            if not stop_words:
                words = [word for word in words if not word.split("::")[0] 
                                in self.stop]
            #remove punctuation if punctuation is false
            if not punctuation:
                pun = string.punctuation
                words = [word for word in words if not word.split("::")[0] 
                            in pun]
            #####MODIFIED###########
            #phrase the sentence
            out = self.phraser.phrase(words)
            if out is not None:
                phrases,tags = out 
                #isolate multi word phrases and words with proper tags
                words = [word for i,word in enumerate(phrases)if (tags[i] in 
                            allowed_tags or len(list(tags[i])) > 1)]
            grams.extend(words)
                
            
        return(grams)     
        
class PhraseCorpus(PhraseCorpusIterator):
    """Child class of PhraseCorpusIterator, trains a phraser and gets ngrams
        with phrased output"""
        
    def __init__(self,indir, tfidf = False,stem = None, stop_words = True, 
                 tag = 'ap', allowed_tag = ['N'], punctuation = True, 
                 threshold = 5, split_clauses = False,outdir = None):
        """Constructor
        Input:
            indir: path to directory of txt files
            tfidf: Boolean. do tfidf
            stem: {'snowball','porter','lemma',None} stemmer to use
                    Defaults to None.
            stop_words: Boolean. include stopwords. Defaults to True
            tag: {'ap','nltk','stanford'}. POS tagger to use. Defaults 
                    to 'ap'
            allowed_tags: tags for unigrams that are allowed. Defaults to
                            Nouns
            punctuation: Boolean. include punctuation. Defaults to True
            threshold: minimum number of documents a gram has to be present in.
                        Defaults to 5.
            split_clauses: Boolean. Split on clauses
            outdir: directory to write to. Defaults to indir/results
        """  
        #set up tagger if tag is not None
        if tag == 'ap':
            #cerate tagger
            self.tagger = APTagger()
            
        elif tag == 'nltk':
            #create a named tuple which holds nltk.pos_tag_sents as 
            #tag_sents
            NLTKTagger = namedtuple("NLTKTagger",["tag_sents","tag"])
            self.tagger = NLTKTagger(nltk.pos_tag_sents,nltk.pos_tag)
            
        elif tag == 'stanford':
            #create stanford tagger
            model = '../../stanford/postagger/models/english-bidirectional-distsim.tagger' 
            jar = '../../stanford/postagger/stanford-postagger-3.5.2.jar'
            self.tagger = POSTagger(model,jar,encoding = 'utf-8')
        else:
            #raise a value error if an unsupproted tagger is included
            raise ValueError('Not an available tagger')
        #load a MultiDict object from the corpus
        mcgi = MultiGramCorpusIterator(indir,stem,stop_words,punctuation,
                                       split_clauses,outdir)
        #load phraser
        self.phraser = PosPhrases()
        #assign the same tagger as self
        self.phraser.tagger = self.tagger
        self.phraser.vocab = self.count_grams(mcgi)
        
        self.dictionary = ngrams_corpus.Dictionary()
        self.threshold = threshold

        #super constructor
        super(PhraseCorpus,self).__init__(self.phraser,indir,stem,stop_words,
                                            tag,allowed_tag,punctuation,
                                            split_clauses,outdir)
        #tfidf boolean
        self.tfidf_bool = tfidf



    def count_grams(self,multi_corpus):
        """Counts the occurances of 1-4 grams in multi_corpus"""
        
        counts = defaultdict(int)
        
        #iterate through corpus
        for doc in multi_corpus:
            #count grams
            for gram in doc:
                counts[gram] += 1
        return(counts)


    def __next__(self):
        """Next function for iterator. same as PhraseCorpusIterator but 
            transforms to bag of words"""
        return(self.dictionary.doc2bow(ngrams,allow_update = True))
        if self.index >= len(self.files):
            raise StopIteration
        #if not splitting on clauses
        if not self.split_clauses:
            #get sentences from file
            sents = doc_sents(self.files[self.index],zipped = self.zip_corpus)
            #get ngrams of doc and yield 
            ngrams = self.grams_from_sents(sents,self.stem,self.stop_words,
                                            self.tag,self.allowed_tags,
                                            self.punctuation)
            self.index += 1
            return(self.dictionary.doc2bow(ngrams,allow_update = True))
        else:
            if len(self.curr_clauses) == 0:
                self.curr_clauses = doc_sents(self.files[self.index],
                                         zipped = self.zip_corpus,
                                         clauses = True)
                self.index += 1
            #pop one clause
            sents = self.curr_clauses.pop()
            ngrams = self.grams_from_sents(sents,self.stem,self.stop_words,
                                            self.tag,self.allowed_tags,
                                            self.punctuation) 
            return(self.dictionary.doc2bow(ngrams,allow_update = True))
    def __iter__(self):
        """Overidden Iterator, does the same as PhraseCorpusIterator but 
            transforms to bag of words"""
        if not self.split_clauses:
            for i, fName in enumerate(self.files):
                if i % 100 == 0:
                    logging.info("Computing N-grams for %ith file %s" %(i,fName))            
                #get sentences from file
                sents = doc_sents(fName,zipped = self.zip_corpus)
                #get ngrams of doc and yield 
                ngrams = self.grams_from_sents(sents,self.stem,self.stop_words,
                                            self.tag,self.allowed_tags,
                                            self.punctuation)
                yield(self.dictionary.doc2bow(ngrams,allow_update = True))
        else:
            for i, fName in enumerate(self.files):
                if i % 100 == 0:
                    logging.info("Computing N-grams for %ith file %s" %(i,fName))            
                #get sentences for clauses
                clauses = doc_sents(fName,zipped = self.zip_corpus,clauses = True)
                #iterate over clauses
                for sents in clauses:
                    #get ngrams of doc and yield 
                    ngrams = self.grams_from_sents(sents,self.stem,self.stop_words,
                                                self.tag,self.allowed_tags,
                                                self.punctuation)
                    yield(self.dictionary.doc2bow(ngrams,allow_update = True))           
    def to_sparse(self):
        """Turns the corpus into a sparse matrix"""
        #get sparse matrix
        sparse = matutils.corpus2csc(self,num_docs = len(self.files)).T
        #do thresholding
        sparse = self.threshold_matrix(sparse)
        #do tfidf if True
        if self.tfidf_bool:
            sparse = self.tfidf(sparse)
        #return sparse matrix
        return(sparse)
    
    def threshold_matrix(self,sparse):
        """Does thresholding on a sparse matrix
        Input:
            sparse: sparse matrix to do threhsolding on
        Output:
            sparse: sparse matrix after thresholding
        """
        logging.info("thresholding")
        #convert the sparse matrix to a csc matrix b/c its faster
        sparse = sparse.tocsc()
        #create a boolean mask
        mask = np.zeros(sparse.shape[1],dtype = bool)
        #change the dictionary token2id to account for droped terms
        id2token = {}
        token2id = {}
        count = 0
        #get all columns with greater than threshold doc freq
        for i in range(sparse.shape[1]):
            if sparse[:,i].nnz >= self.threshold:
                mask[i] = True
                token = self.dictionary[i]
                #use count as the new id
                id2token[count] = token
                token2id[token] = count
                #increase count
                count += 1                
        #only keep the columns with greater than threshold doc freq
        mask_flat = np.flatnonzero(mask)        
        sparse = sparse[:,mask_flat]   
        #put these new dicts in the Dictionary
        self.dictionary.id2token = id2token
        self.dictionary.token2id = token2id
        #convert back to csr
        sparse = sparse.tocsr()
        return(sparse)
        
    def tfidf(self,sparse):
        """Returns the tfidf transformation of a sparse matrix
        Input:
            sparse: sparse amtrix to be transformed
        Output:
            sparse: sparse matrix after tfidf transformation
        """
        transform = feature_extraction.text.TfidfTransformer()
        sparse = transform.fit_transform(sparse)
        return(sparse)     