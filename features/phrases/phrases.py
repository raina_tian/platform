# -*- coding: utf-8 -*-
"""
Computes N-grams after linking together phrases
"""

from pos_phrases import PosPhrases
import nltk
from nltk.stem.snowball import SnowballStemmer
from nltk.stem.porter import PorterStemmer  
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tag.stanford import StanfordPOSTagger as POSTagger
from collections import Counter
import string
import csv
import os
import argparse
import logging
import six
from six.moves import cPickle as pickle
import zipfile
import phrases_corpus

#import ngrams
import sys
sys.path.append("../ngrams")
import ngrams
from ngrams import APTagger, keydefaultdict

#POS is set by the main method, here is a default
POS = PosPhrases()
#stopwords
stop = set(stopwords.words('english'))

#logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s\t%(message)s')

#top level variable holding default tag patterns and dict for conversion to
#universal tag set
default_tagpatterns = set(['AN','NN','AAN','ANN','NAN','NPN',
                                'VN','VAN','VNN','VPN','ANV','NVV','VDN'])

default_tagset = set(''.join(default_tagpatterns))


tagdict = keydefaultdict(lambda x: x,
                        {'NN':'N',
                        'NNS':'N',
                        'NNP':'N',
                        'NNPS':'N',
                        'JJ':'A',
                        'JJR':'A',
                        'JJS':'A',
                        'VBG':'A',
                        'RB':'A',
                        'DT':'D',
                        'IN':'P',
                        'TO':'P',
                        'VB':'V',
                        'VBD':'V',
                        'VBN':'V',
                        'VBP':'V',
                        'VBZ':'V'})




def load_doc_freq(fPath):
    """loads document frequency information
    Input:
        fPath: a pickled tuple containing a Counter object with document 
                frequency information as the first element
    Output:
        df: document frequency Counter"""
    #load using pickle
    with open(fPath,'rb') as f:
        df = pickle.load(f)[0]
        
    #index through grams and joint he gram tuples by "_"
    grams = list(df)
    for gram in grams:
        #deal with bytes/unicode accordingly for python 2 and 3
        if six.PY2:
            df["_".join([g for g in gram])] = df.pop(gram)
        elif six.PY3:
            df[b"_".join([str.encode(g) for g in gram])] = df.pop(gram)
    return(df)
    
def combine_counters(fPaths):
    """combines the counters pickled at fPaths
    Input:
        fPaths: list of file paths leading to pickled tuples with counter
                containing document frequency information as first element
    Output:
        df_total: document frequency Counter which is a combination of the 
            ones found at fPaths
    """
    #load and combine Counters
    df_total = Counter()
    
    for f in fPaths:
        df_total += load_doc_freq(f)

    return(df_total)
    
    
def load_phraser(fPaths):
    """Loads a PosPhrases objects with vocab defined by the counter objects
        pickled at fPaths
    Input:
        fPaths: list of file paths leading to pickled tuples with counter
                containing document frequency information as first element
    output:
        pos: PosPhrases object
    """
    #combine counters
    df = combine_counters(fPaths)
    #instantiate PosPhrases object
    pos = PosPhrases()
    #assign df as the vocab for PosPhrases
    pos.vocab = df
    return(pos)
    
    
def ngrams_from_sents(sents,n, stem = None, stop_words = True, 
                      tag = None, tag_pattern = None, punctuation = True):
    """Gets the ngrams from a list of sentences
        CHANGED FROM ngrams.py TO INCLUDE PHRASER
    Input:
        sents: list of sentences as strings
        n: order of n gram
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        tag: {'ap','nltk','stanford',None}. What POS tagger to use. Defaults 
                to None
        tag_pattern: list of of tag patterns to allow in simplified form.
                     Defaults to None. if tag_pattern = "default", 
                     use default tag pattern.
        punctuation: Boolean. Whether to include punctuation. Defaults to True
    Output:
        ngrams: list of ngrams as tuples
    """
       
    #tokenize the sentences
    tmp = []
    for sent in sents:
        tmp.append([word.lower() for word in nltk.word_tokenize(sent)])
    sents = tmp
    #tag sentences first
    if tag is not None:
        if tag == 'ap':
            #tag words
            tagger = APTagger()
            tags = tagger.tag_sents(sents)
            #go from universal/penn treebank tagset to simple tagset
            tags = [[tagdict[t] for t in tags[i]] for i in range(len(tags))]
            #join tags and words
            #sents = [['::'.join(tagWord) for tagWord in zip(sents[i],tags[i])] 
                        #for i in range(len(sents))]
        
        elif tag == 'ntlk':
            #tag words
            tags = nltk.pos_tag_sents(sents)
            #extract the tags without the words
            tags = [[tagdict[tagWord[1]] for tagWord in tag[i]] for i \
                    in range(len(sents))]
            #join tags and words
            #sents = [['::'.join(t) for t in tags[i]] for i in range(len(sents))]
        
        elif tag == 'stanford':
           model = '../../stanford-postagger/models/english-bidirectional-distsim.tagger' 
           jar = '../../stanford-postagger/stanford-postagger-3.5.2.jar'
           tagger = POSTagger(model,jar,encoding = 'utf-8')
           #tag words
           tags = tagger.tag_sents(sents)
           #extract the tags without the words
           tags = [[tagdict[tagWord[1]] for tagWord in tag[i]] for \
                    i in range(len(sents))]
           #join tags and words
           #sents = [['::'.join(t) for t in tags[i]] for i in range(len(sents))]
           
        else:
            #raise a value error if an unsupproted tagger is included
            raise ValueError('Not an available tagger')
        

    
    
    
    #initialize stemmer if stem is not None
    if stem is not None:
        if stem == 'porter':
            stemmer = PorterStemmer()
        elif stem == 'snowball':
            stemmer = SnowballStemmer("english")
        elif stem == 'lemma':
            stemmer = WordNetLemmatizer()
            #add stem as another name for lemmatize
            stemmer.stem = stemmer.lemmatize
        else:
            #raise a value error if a wrong stemmer is chosen
            raise ValueError('Not an available stemmer')    
    #iterate through sentences and get ngrams
    ngrams = []
    for i,words in enumerate(sents):
        #stem words if stem is not None
        if stem is not None:
            words = [stemmer.stem(word) for word in words]
        #join tags and words if tag is not None
        if tag is not None:
            words = ['::'.join(tagWord) for tagWord in zip(words,tags[i])]
        #remove stop words if stop = False
        if not stop_words:
            words = [word for word in words if not word.split("::")[0] in stop]
        #remove punctuation if punctuation is false
        if not punctuation:
            pun = string.punctuation
            words = [word for word in words if not word.split("::")[0] in pun]
        
        #####MODIFIED###########
        #phrase the sentence

        words = POS.phrase(words)
        #incase for an empty sentence where POS.phrase returns a None
        if words is None:
            words = []
        #get n grams and add to ngrams list
        sent_grams = [gram for gram in nltk.ngrams(words,n)]
        #if tag_pattern isn't None, go through sent_grams and only keep those
        #ngrams with the proper tag pattern
        if tag_pattern is not None:
            #assign default tag pattern if tag_pattern == 'default'
            if tag_pattern == 'default':
                tag_pattern = default_tagpatterns
            tmp = []
            #maybe make this a list comprehension?
            for gram in sent_grams:
                #squash ngram
                squash = '::'.join(gram)
                
                #get tags by getting every odd element
                tags_squash = squash.split("::")[1::2]
                #convert tags into their reduced form
                #e.g. VB and VBN go to V
                tags_squash = [t for t in tags_squash]
                #check if the tag pattern is allowed
                if ''.join(tags_squash) in tag_pattern:
                    tmp.append(gram)
            sent_grams = tmp

        ngrams.extend(sent_grams)  
        
    return(ngrams)
 

def count_doc_freqs(indir, n, stem = None, stop_words = True, tag = None,
                     tag_pattern = None, punctuation = True, freq = False,
                     threshold = 5, outdir = None):
    """Gets the document frequencies in a directory and saves 
        output by both pickling the dicts and writing to csv. Also saves 
        a file for the ngrams across the corpus
    Input:
        indir: path to directory of txt files
        n: order of n gram
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        tag: {'ap','nltk','stanford',None}. What POS tagger to use. Defaults 
                to None
        tag_pattern: list of of tag patterns to allow in simplified form.
                     Defaults to None. if tag_pattern = "default", 
                     use default tag pattern.
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        freq: Boolean. Whether to return frequencies. Defaults to False
        threshold: minimum number of documents a gram has to be present in
        outdir: directory to write to. Defaults to indir/results
    """
    
    #check if directory is zip archive or directory and act accordingly
    if not zipfile.is_zipfile(indir):
        #list the files in the directory
        files = [os.path.join(indir,f) for f in os.listdir(indir) if \
                    os.path.splitext(f)[1] == ".txt"]
        #create directory for results
        if outdir is None:
            outdir = os.path.join(indir,"phrases_results")
        #check if directory exists, if not create direcotry
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        #set zip_corpus to None
        zip_corpus = None
    else:
        #files is the namelist of the zip archive
        zip_corpus = zipfile.ZipFile(indir)
        files = zip_corpus.namelist()
        #crete directory for results in the directory of the zip archive
        if outdir is None:
            #get the directory of the zip archive
            tmp = os.path.split(indir)[0]
            outdir = os.path.join(tmp,"phrases_results")
        #cheack if the directory exists, if not create the directory
        if not os.path.exists(outdir):
            os.mkdir(outdir)
    #initialize Counter object which keeps track of the number of documents
    #a word is in
    doc = Counter()
    #index through all text files in directory and save sngram counts
    for fpath in files:
        output = ngrams.ngram_file_output2(fpath,n,stem,stop_words,tag,tag_pattern,
                                   punctuation, freq, outdir = outdir,
                                   zipped = zip_corpus)
        #add ngrams to document frequency counter
        doc += Counter(output[1].keys())
    #save document frequencies
    #iterate over grams and assign ids by largest doc frequencies 
    gram2id_df = {}
    id2gram_df = {}
    for gram_id, (gram,count) in enumerate(doc.most_common()):
        gram2id_df[gram] = gram_id
        id2gram_df[gram_id] = gram
    doc_file = os.path.join(outdir,"doc_freqs")
    ngrams.save_output(doc_file,n,doc,gram2id_df,id2gram_df)


def save_output(outFile,gramNum,counts,gram2id,id2gram):
    """Saves the frequency Counter object and id dictionaries.
     
         Saves 1 pkl file of output as a tuple and 2 csv files
         
     
     Input:
         outFile: file name to save to w/o file extension
         gramNum: type of n-gram (e.g. 2 = bigram, 3 = trigram, etc.)
         counts: frequency Counter object
         gram2id, id2gram: id dictionaries
    """

    #pickle the files after combining them into a tuple
    output = (counts,gram2id,id2gram)
    fName = outFile + "_" + str(gramNum) + '_phrases.pkl'
    with open(fName,'wb+') as f:
        pickle.dump(output,f,protocol = 2)
        
    #write id csv files
    idName = outFile + "_"+ str(gramNum) + "_gram_phrases_id.csv"
    #check type of python and act accordingly
    if six.PY2:
        with open(idName,'wb+') as csvfile:
            writer = csv.writer(csvfile)
            #iterate through key-value pairs in id2gram and write to csv
            for gramid in id2gram.keys():
                #if there are tags, drop them when writing to file
                gram = [g.split("::")[0] for g in id2gram[gramid]]
                #have n-grams output as word-word rather than ("word","word")
                gram = "-".join(gram)
                """ #delete Unicode BOM
                if gram.startswith(u'\ufeff'):
                    gram = gram[1:]"""
                gram = gram.encode("utf-8")
                
                writer.writerow([gramid,gram])
        #write frequency csv file
        
        freqName = outFile + "_"+ str(gramNum) + "_gram_phrases_freq.csv"
        
        with open(freqName, 'wb+') as freqfile:
            writer = csv.writer(freqfile)
            #iterate through gramids and write gramid,frequency pairs to csv
            for gramid in id2gram.keys():
                writer.writerow([gramid,counts[id2gram[gramid]]])                
    elif six.PY3:
        with open(idName,'w+', newline = '',encoding = 'utf-8') as csvfile:
            writer = csv.writer(csvfile)
            #iterate through key-value pairs in id2gram and write to csv
            for gramid in id2gram.keys():
                #if there are tags, drop them when writing to file
                g = [gram.split("::")[0] for gram in id2gram[gramid]]
                #have n-grams output as word-word rather than ("word","word")
                writer.writerow([gramid,"-".join(g)])        
        #write frequency csv file
        
        freqName = outFile + "_"+ str(gramNum) + "_gram" + "_freq.csv"
        
        with open(freqName, 'w+',newline = '') as freqfile:
            writer = csv.writer(freqfile)
            #iterate through gramids and write gramid,frequency pairs to csv
            for gramid in id2gram.keys():
                writer.writerow([gramid,counts[id2gram[gramid]]])  

def main_with_args(indir,n,files,stem,stop_words,tag,tp,punct,freq,thresh,
                   outdir,make):
    """Wrapper for main method with command line arguments, so it can be 
        called later"""
    #check if tag pattern tp only contains 'default'
    if tp is not None and len(tp) == 1 and tp[0] == 'default':
        tp = 'default'
    #if outdir is None, make a new directory at indir/phrases_results
    if outdir is None:
        #if indir is a zip file, define outdir differently
        if not zipfile.is_zipfile(indir):
            outdir = os.path.join(indir,"phrases_results")
        else:
            tmp = os.path.split(indir)[0]
            outdir = os.path.join(tmp,"phrases_results")            
    #count document frequencies (and save them) if args.make is True
    if make:
        logging.info("Creating document frequency dictionaries")
        #create directory for results
        if outdir is None:
            out = os.path.join(indir,"results")
        else:
            out = outdir
        logging.info("1-gram")
        count_doc_freqs(indir,1,stem,stop_words,tag,tp,punct,
                     freq,thresh,out)
        logging.info("2-gram")
        count_doc_freqs(indir,2,stem,stop_words,tag,tp,punct,
                     freq,thresh,out)
        logging.info("3-gram")
        count_doc_freqs(indir,3,stem,stop_words,tag,tp,punct,
                     freq,thresh,out)
        logging.info("4-gram")
        count_doc_freqs(indir,4,stem,stop_words,tag,tp,punct,
                     freq,thresh,out)                     
        #set files to be the files just created
        files = [os.path.join(out,"doc_freqs_%i.pkl" %i) for i in range(1,5)]

        
    
    #load the phraser as a global variable
    global POS 
    POS = load_phraser(files)    
    #change ngrams_from_sents in sngrams to be the new one
    ngrams.ngrams_from_sents = ngrams_from_sents
    #change the save_output function to the new one
    ngrams.save_output = save_output
    logging.info("Running N-grammmer on phrased sentences")
    #run ngrams_from_dir with input
    return(ngrams.ngrams_from_dir2(indir,n,stem,stop_words,tag,tp,punct,
                     freq,thresh,outdir))
                     
def main(indir,tfidf,stem,stop_words,tag,allowed_tags,punctuation,
         threshold,split_clauses,outdir):
    """Main method using PhraseCorpus Object"""
    logging.info("Training Phraser")
    pc = phrases_corpus.PhraseCorpus(indir,tfidf,stem,stop_words,tag,
                                     allowed_tags,punctuation,
                                     threshold,split_clauses,outdir)
    logging.info("Converting Corpus to sparse matrix")
    #get sparse matrix
    sparse = pc.to_sparse()
    #save sparse matrix and dictionary
    outdir = pc.outdir
    with open(os.path.join(outdir,"mat.pkl"),"wb") as f:
        pickle.dump(sparse,f, protocol = 2)
    with open(os.path.join(outdir,"dictionary.pkl"),"wb") as f:
        pickle.dump(pc.dictionary,f, protocol = 2)    
        
    #return sparse matrix and dictionary
    return(sparse,pc.dictionary)
    
   
if __name__ == "__main__":
    #get arguments
    parser = argparse.ArgumentParser(
                                    description= " Extracts N-grams after \
                                    combining phrases \
                                    Input: a directory of pre-processed text \
                                    files. \
                                    Output: directory of gram-frequency CSV \
                                    files.")
    parser.add_argument("indir", metavar = "indir", type = str,
                        help = "directory of txt files to get N Grams from")
    parser.add_argument("--tfidf",dest = "tfidf",action="store_true",
                        help = "do tfidf transformation",default = False)                           
    parser.add_argument("--stem",dest = "stem",type = str,
                        help = "Stemmer: 'snowball','porter','lemma', defaults \
                        to None", default = None)
    parser.add_argument("--no_stop",dest = "stop",action="store_false",
                        help = "discard stop words",default = True) 
    parser.add_argument("--tag",dest = "tag",type = str,
                        help = "POS tag: 'ap','nltk','stanford', defaults to \
                        no tagging", default = None)
    parser.add_argument("--allowed_tags",dest = "allowed_tags",type = str,
                        help = "tag pattern to allow",nargs = "+",
                        default = ["N"])
    parser.add_argument("--no_punct",dest = "punct", action = "store_false",
                        default = True, help = "discard punctuation")
                        
    parser.add_argument("--threshold", dest = "thresh", type = int,
                        default = 5, help = "minimum number of documents an \
                        ngram must be in, defaults to 5")
    parser.add_argument("--split_clauses", dest = "split_clauses", 
                        action = "store_true",
                        default = False, help = "split documents on clauses")
    parser.add_argument("--outdir",dest = "outdir",default = None, type = str,
                        help = "outdir to write results to, defaults to \
                        indir/results")
    args = parser.parse_args()
    
    indir = args.indir
    tfidf = args.tfidf    
    stem = args.stem
    stop_words = args.stop
    tag = args.tag
    allowed_tags = args.allowed_tags
    punct = args.punct
    thresh = args.thresh
    split_clauses = args.split_clauses
    outdir = args.outdir
    

    main(indir,tfidf,stem,stop_words,tag,allowed_tags,punct,thresh,
         split_clauses,outdir)

