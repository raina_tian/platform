# -*- coding: utf-8 -*-
"""
Computes Brown Clusters and Word clusters based on Pointwise Mutual Information.
Clusters are hierarchical, and represented by bit strings. Words are assigned
to a cluster by the first n bits in the bit string, and these clusters are
used for ngrams rather than the words themselves.
"""
import argparse
import os
import nltk
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from nltk.stem.porter import PorterStemmer  
from nltk.stem import WordNetLemmatizer
import string
import csv
from collections import Counter,defaultdict
import codecs
import logging
import six
from six.moves import cPickle as pickle
import zipfile

import sys
sys.path.append("..")
from docLoader import *

from tan_clustering.class_lm_cluster import ClassLMClusters
from tan_clustering.pmi_cluster import DocumentLevelClusters, make_word_counts, document_generator

#logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s\t%(message)s')


#top level variable which sontains english stop words as a set
STOP_WORDS = set(stopwords.words('english'))

class brown(ClassLMClusters):
    """Child class of ClassLMClusters, with method to output a dict of 
        (word,bitstring) key,value pairs"""
    def return_clusters(self):
        """Returns a dictionary of bitstrings for each word"""
        #initialize clusters dict
        clusters = {}
        #iterate through each word in dictionary,get the bitstring and add to
        #clusters
        
        for w in self.vocab:
            clusters[w] = self.get_bitstring(w)
            
        return(clusters)
        
class pmi(DocumentLevelClusters):
    """Child class of DocumentLevelClusters. with method to output a dict of 
        (word,bitstring) key,value pairs"""
    def return_clusters(self):
        """Returns a dictionary of bitstrings for each word"""
        #initialize clusters dict
        clusters = {}
        #iterate through each word in dictionary,get the bitstring and add to
        #clusters
        
        for w in self.words:
            clusters[w] = self.get_bitstring(w)
            
        return(clusters)        

def get_clusters(brown_file,lower,max_vocab_size,min_word_count,batch_size):
        """Wrapper function to get the clusters."""
        word_counts = make_word_counts(document_generator(brown_file, 
                                                          lower=lower),
                                   max_vocab_size=max_vocab_size,
                                   min_word_count=min_word_count)

        c = pmi(document_generator(brown_file, lower=lower),
                              word_counts, batch_size=batch_size)
        clusters = c.return_clusters()
        return(clusters)


def cluster_to_word(clusters,bits):
    """Reverses the word -> cluster dictionary to find cluster information
    Input:
        clusters: dict of word,cluster key,value pairs
        bits: length of bitstring to cluster by
    Output:
        c2w: dict of cluster,words ley,valu pairs
    """
    c2w = defaultdict(list)
    for k in clusters.keys():
        c2w[clusters[k][:bits]].append(k)
    return(c2w)



def file_to_string(fPath,stem = None, punctuation = True, zipped = None):
    """Reads in a file and returns a string with the file on one line and
        each token seperated by a string.
    Input:
        fPath: path to file
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        zipped: a ZipFile object to read fPath from if the directory is 
                zipped. Defaults to None
    Output:
        fString: file as string
    """
    #initialize stemmer if stem is not None
    if stem is not None:
        if stem == 'porter':
            stemmer = PorterStemmer()
        elif stem == 'snowball':
            stemmer = SnowballStemmer("english")
        elif stem == 'lemma':
            stemmer = WordNetLemmatizer()
            #add stem as another name for lemmatize
            stemmer.stem = stemmer.lemmatize
        else:
            #raise a value error if a wrong stemmer is chosen
            raise ValueError('Not an available stemmer')  
    #get the sentences of the file
    sents = doc_sents(fPath,zipped = zipped)
    #index through sentences, word tokenize the sentences, and extract the words
    all_words = []    
    for sent in sents:
        if stem is not None and not punctuation:
            words = [stemmer.stem(word).lower() for word in \
                    nltk.word_tokenize(sent) if word not in string.punctuation]
        elif stem is not None and punctuation:
            words = [stemmer.stem(word).lower() for word in \
                        nltk.word_tokenize(sent)]
        elif stem is None and not punctuation:
            words = [word.lower() for word in nltk.word_tokenize(sent) \
                        if word not in string.punctuation]
        else:
            words = [word.lower() for word in nltk.word_tokenize(sent)]
        #add the words to all_words
        all_words.extend(words)
    #join the tokens in all_words into a string by white space
    
    fString = " ".join(all_words)
    return(fString)
    
    
def write_corpus_file(indir,outdir = None,stem = None, punctuation = True):
    """Writes the .txt text files in a directory to a single file with 
        each document on one line and each token seperated by white space
    Input:
        indir: path to direcotry of txt files. Defaults to indir
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        outdir: path to directory to save to
    Output:
        file path of txt file written to
    """
    #check if directory is zip archive or directory and act accordingly
    if not zipfile.is_zipfile(indir):
        #list the files in the directory
        files = [os.path.join(indir,f) for f in os.listdir(indir) if \
                    os.path.splitext(f)[1] == ".txt"]
        #create directory for results
        if outdir is None:
            outdir = os.path.join(indir,"brown_files")
        #check if directory exists, if not create direcotry
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        #set zip_corpus to None
        zip_corpus = None
    else:
        #files is the namelist of the zip archive
        zip_corpus = zipfile.ZipFile(indir)
        files = zip_corpus.namelist()
        #crete directory for results in the directory of the zip archive
        if outdir is None:
            #get the directory of the zip archive
            tmp = os.path.split(indir)[0]
            outdir = os.path.join(tmp,"brown_files")
        #cheack if the directory exists, if not create the directory
        if not os.path.exists(outdir):
            os.mkdir(outdir)
    #initialize string which will be written to outdir/brown_file.txt
    brown_file = ""
    for fPath in files:
        logging.info("Reading %s" %fPath)
        brown_file += file_to_string(fPath,stem,punctuation,
                                     zipped = zip_corpus) + "\n"
    #write brown_file to file
    with codecs.open(os.path.join(outdir,"brown_file.txt"),"w","utf-8-sig") as f:
        f.write(brown_file)
    return(os.path.join(outdir, "brown_file.txt"))
    
def cluster_ngrams_sents(sents,n,clusters,bits,stem = None, stop_words = True,
                         punctuation = True):
    """Creates ngrams from sents, but replaces words with first bits of 
        cluster bitstring
    Input: 
        sents: list of sentences as strings
        n: int. order of ngram
        clusters: dict of words to bitstrings
        bits: int. number of bits to consider, creates 2^bits clusters
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to True
    Ouput:
        ngrams: list of cluster ngrams as tuples
    """
    #initialize stemmer if stem is not None
    if stem is not None:
        if stem == 'porter':
            stemmer = PorterStemmer()
        elif stem == 'snowball':
            stemmer = SnowballStemmer("english")
        elif stem == 'lemma':
            stemmer = WordNetLemmatizer()
            #add stem as another name for lemmatize
            stemmer.stem = stemmer.lemmatize
        else:
            #raise a value error if a wrong stemmer is chosen
            raise ValueError('Not an available stemmer')  
    #go through every sentence, convert words to bitstrings, get ngrams
    ngrams = []

    for sent in sents:
        #get words as clusters
        if stem is not None and not punctuation and not stop_words:
            words = [clusters[stemmer.stem(word).lower()][:bits] for word in \
                    nltk.word_tokenize(sent) if word not in string.punctuation \
                    and word not in STOP_WORDS]
        elif stem is not None and not punctuation and stop_words:
            words = [clusters[stemmer.stem(word).lower()][:bits] for word in \
                    nltk.word_tokenize(sent) if word not in string.punctuation]
        elif stem is not None and punctuation and not stop_words:
            words = [clusters[stemmer.stem(word).lower()][:bits] for word in \
                        nltk.word_tokenize(sent) if word not in STOP_WORDS]
        elif stem is not None and punctuation and stop_words:
            words = [clusters[stemmer.stem(word).lower()][:bits] for word in \
                        nltk.word_tokenize(sent)]
        elif stem is None and not punctuation and not stop_words:
            words = [clusters[word.lower()][:bits] for word in nltk.word_tokenize(sent) \
                        if word not in string.punctuation and word not in \
                        STOP_WORDS]
        elif stem is None and not punctuation and stop_words:
            words = [clusters[word.lower()][:bits] for word in nltk.word_tokenize(sent) \
                        if word not in string.punctuation]
        elif stem is None and punctuation and not stop_words:
            words = [clusters[word.lower()][:bits] for word in nltk.word_tokenize(sent) \
                        if word not in STOP_WORDS]
        else:
            words = [clusters[word.lower()][:bits] for word in nltk.word_tokenize(sent)]
        #words = [clusters[words][:bits] for words in nltk.word_tokenize(sent)]
        #get the ngrams
        sent_grams = [gram for gram in nltk.ngrams(words,n)]
        #add to ngrams
        ngrams.extend(sent_grams)
        
    return(ngrams)

def cluster_ngrams_from_file(fPath,n,clusters,bits,stem = None, 
                             stop_words = True, punctuation = True,
                             zipped = None):
    """Gets cluster ngrams from a file
    Input: 
        fPath: path to file
        n: int. order of ngram
        clusters: dict of words to bitstrings
        bits: int. number of bits to consider, creates 2^bits clusters
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        zipped: a ZipFile object to read fPath from if the directory is 
                zipped. Defaults to None
    Ouput:
        ngrams: list of cluster ngrams as tuples
    """ 
    
    sents = doc_sents(fPath,"utf-8-sig",zipped = zipped)
    ngrams = cluster_ngrams_sents(sents,n,clusters,bits,stem,stop_words,
                                  punctuation)
    return(ngrams)

def count_grams(ngrams):
    """Counts the number of ngrams
    
    Input:
        ngrams: list of ngrams
    Output:
        Counter object which counts the ngrams
    """
    
    return(Counter(ngrams))
             
def gram_counts(grams, freq = False):
    """Counts the gram frequencies, and assings IDs to each gram
    
    Input:
        grams: A list of ngrams to be counted
        freq: Boolean. Whether to return frequencies. Defaults to False
    Output:
        gramCounter: Counter object of gram counts
        gram2id: dictionary with grams as keys and IDs as values
        id2gram: dictionary with IDs as keys and grams as values
    """
    #count the grams
    gramCounter = count_grams(grams)
    #initialize id dicts
    gram2id = {}
    id2gram = {}
    #get total number of tokens if freq == True
    if freq:
        total = sum(gramCounter.values())
    #iterate over the grams and assign IDs by most common grams
    for gramID, (gram,count) in enumerate(gramCounter.most_common()):
        #divide counts by total counts if freq == true
        if freq:
            gramCounter[gram] /= total
        gram2id[gram] = gramID
        id2gram[gramID] = gram
        
    return(gramCounter,gram2id,id2gram)


def save_output(outFile,gramNum,counts,gram2id,id2gram):
    """Saves the frequency Counter object and id dictionaries.
     
         Saves 1 pkl file of output as a tuple and 2 csv files
         
     
     Input:
         outFile: file name to save to w/o file extension
         gramNum: type of n-gram (e.g. 2 = bigram, 3 = trigram, etc.)
         counts: frequency Counter object
         gram2id, id2gram: id dictionaries
    """

    #pickle the files after combining them into a tuple
    output = (counts,gram2id,id2gram)
    fName = outFile + "_cluster_" + str(gramNum) + '.pkl'
    with open(fName,'wb+') as f:
        pickle.dump(output,f,protocol = 2)
        
    #write id csv files
    idName = outFile + "_cluster_"+ str(gramNum) + "_gram" + "_id.csv"
    #check type of python and act accordingly
    if six.PY2:
        with open(idName,'wb+') as csvfile:
            writer = csv.writer(csvfile)
            #iterate through key-value pairs in id2gram and write to csv
            for gramid in sorted(id2gram.keys()):
                #if there are tags, drop them when writing to file
                gram = [g.split("::")[0] for g in id2gram[gramid]]
                #have n-grams output as word-word rather than ("word","word")
                gram = "-".join(gram)
                """ #delete Unicode BOM
                if gram.startswith(u'\ufeff'):
                    gram = gram[1:]"""
                gram = gram.encode("utf-8")
                
                writer.writerow([gramid,gram])
        #write frequency csv file
        
        freqName = outFile + "_cluster_"+ str(gramNum) + "_gram" + "_freq.csv"
        
        with open(freqName, 'wb+') as freqfile:
            writer = csv.writer(freqfile)
            #iterate through gramids and write gramid,frequency pairs to csv
            for gramid in sorted(id2gram.keys()):
                writer.writerow([gramid,counts[id2gram[gramid]]])                
    elif six.PY3:
        with open(idName,'w+', newline = '',encoding = 'utf-8') as csvfile:
            writer = csv.writer(csvfile)
            #iterate through key-value pairs in id2gram and write to csv
            for gramid in sorted(id2gram.keys()):
                #if there are tags, drop them when writing to file
                g = [gram.split("::")[0] for gram in id2gram[gramid]]
                #have n-grams output as word-word rather than ("word","word")
                writer.writerow([gramid,"-".join(g)])        
        #write frequency csv file
        
        freqName = outFile + "_cluster_"+ str(gramNum) + "_gram" + "_freq.csv"
        
        with open(freqName, 'w+',newline = '') as freqfile:
            writer = csv.writer(freqfile)
            #iterate through gramids and write gramid,frequency pairs to csv
            for gramid in sorted(id2gram.keys()):
                writer.writerow([gramid,counts[id2gram[gramid]]])

def gram_counts_file(fpath, n, clusters, bits, stem = None, 
                     stop_words = True, punctuation = True, freq = False,
                     zipped = None):
    """Gets the ngrams and their counts from a file
    Input:
        fPath: path to file
        n: int. order of ngram
        clusters: dict of words to bitstrings
        bits: int. number of bits to consider, creates 2^bits clusters
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        freq: Boolean. Whether to return frequencies. Defaults to False
        zipped: a ZipFile object to read fPath from if the directory is 
                zipped. Defaults to None
    Output:
        gramCounter: Counter object of gram counts
        gram2id: dictionary with grams as keys and IDs as values
        id2gram: dictionary with IDs as keys and grams as values
    """
    
    #get ngrams
    ngrams = cluster_ngrams_from_file(fpath,n,clusters,bits,stem,stop_words, 
                                      punctuation,zipped = zipped)  
    #get counter and gram2id and id2gram dicts
    counts,gram2id,id2gram = gram_counts(ngrams,freq)
    #return the counter and id dicts
    return(counts,gram2id,id2gram)
    
#Probably obsolete. Use cluster_ngram_file_output2
def cluster_ngram_file_output(fpath, n, clusters, bits, stem = None, 
                              stop_words = True, punctuation = True, 
                              freq = False, outdir = None):
    """Gets the ngram counts from a document and saves output by both pickling
        the dicts and writing to csv
    Input:
        fPath: path to file
        n: int. order of ngram
        clusters: dict of words to bitstrings
        bits: int. number of bits to consider, creates 2^bits clusters
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        freq: Boolean. Whether to return frequencies. Defaults to False
        outdir: directory to write to. Defaults to directory of fpath
        """
    
    #default outdir to the directory of fpath if outdir is None
    if outdir is None:
        outdir = os.path.split(fpath)[0]
    #get counts (not frequencies) and id dicts
    counts,gram2id,id2gram = gram_counts_file(fpath ,n, clusters, bits, 
                                              stem, stop_words,
                                              punctuation, freq = False)
    
    #get file name from path w/o extension
    out = os.path.split(fpath)[1]
    out = os.path.join(outdir,out)
    out = os.path.splitext(out)[0]  
    
    
    #if freq == True, normalize the counts to get frequencies
    if freq:
        freqs = counts.copy()
        
        total = sum(freqs.values())
        for gram in freqs.keys():
            freqs[gram] /= total
        #save output with frequencies rather than counts
        save_output(out,n,freqs,gram2id,id2gram)
    else:
        #save output with counts rather than frequencies
        save_output(out,n,counts,gram2id,id2gram)
    
    #return counts
    return(counts)

def cluster_ngram_file_output2(fpath, n, clusters, bits, stem = None, 
                       stop_words = True, punctuation = True, freq = False,
                       outdir = None,zipped = None):
    """Gets the ngram counts from a document and returns output to be saved
    Input:
        fPath: path to file
        n: int. order of ngram
        clusters: dict of words to bitstrings
        bits: int. number of bits to consider, creates 2^bits clusters
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        freq: Boolean. Whether to return frequencies. Defaults to False
        outdir: directory to write to. Defaults to directory of fpath
        zipped: a ZipFile object to read fPath from if the directory is 
                zipped. Defaults to None
    Output:
        out: path of file to write to
        counts: gram counts:
        gram2id, id2gram: gram to id and id to gram dictionaries
        """
    #default outdir to the directory of fpath if outdir is None
    if outdir is None:
        outdir = os.path.split(fpath)[0]
    #get counts (not frequencies) and id dicts
    counts,gram2id,id2gram = gram_counts_file(fpath,n,clusters,bits,stem,
                                              stop_words,punctuation,
                                              freq = False,zipped = zipped)
    
    #get file name from path w/o extension
    out = os.path.split(fpath)[1]
    out = os.path.join(outdir,out)
    out = os.path.splitext(out)[0]  
    
    
    #if freq == True, normalize the counts to get frequencies
    if freq:
        freqs = counts.copy()
        total = sum(freqs.values())
        for gram in freqs.keys():
            freqs[gram] /= total
        #return everything and freqs
        return(out,counts,freqs,gram2id,id2gram)
    else:
    
        #return everything but freqs, but pad the output with a None where
        #freqs would be
        return(out,counts,None,gram2id,id2gram)
        
#Probably obsolete, use cluster_ngrams_from_dir2 instead    
def cluster_ngrams_from_dir(indir, n, clusters, bits, stem = None,
                    stop_words = True, punctuation = True, freq = False, 
                    outdir = None):
    """Gets the ngram counts from every document in a directory and saves 
        output by both pickling the dicts and writing to csv. Also saves 
        a file for the ngrams across the corpus
    Input:
        indir: path to directory of txt files
        n: int. order of ngram
        clusters: dict of words to bitstrings
        bits: int. number of bits to consider, creates 2^bits clusters
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        freq: Boolean. Whether to return frequencies. Defaults to False
        outdir: directory to write to. Defaults to directory of fpath
    """
    
    #list the files in the directory
    files = [os.path.join(indir,f) for f in os.listdir(indir) if \
                os.path.splitext(f)[1] == ".txt"]
    #create directory for results
    if outdir is None:
        outdir = os.path.join(indir,"results_cluster")
    #check if directory exists, if not create directory
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    #initialize Counter object which keep track of corupus wide ngram counts
    corpus = Counter()
    #index through all text files in directory and save sngram counts
    for fpath in files:
        counts = cluster_ngram_file_output(fpath,n,clusters,bits,stem, 
                                    stop_words, punctuation, freq, 
                                    outdir = outdir)
        #add counts to corpus
        corpus += counts
    #if freq == True normalize the corpus counts
    #also go through the corpus counts and assign IDs by most common
    gram2id_corpus = {}
    id2gram_corpus = {}
    if freq:
        corpus_total = sum(corpus.values())
    for gramID, (gram,count) in enumerate(corpus.most_common()):
        if freq:
            corpus[gram] /= corpus_total
        gram2id_corpus[gram] = gramID
        id2gram_corpus[gramID] = gram
    #save the counts/frequencies and ids for the whole corpus
    total_file = os.path.join(outdir,"cluster_corpus_total")
    save_output(total_file,n,corpus,gram2id_corpus,id2gram_corpus)
    
def cluster_ngrams_from_dir2(indir, n, clusters, bits, stem = None, 
                             stop_words = True, punctuation = True,
                             freq = False, threshold = 5, outdir = None):
    """Gets the ngram counts from every document in a directory and saves 
        output by both pickling the dicts and writing to csv. Also saves 
        a file for the ngrams across the corpus
    Input:
        indir: path to directory of txt files
        n: int. order of ngram
        clusters: dict of words to bitstrings
        bits: int. number of bits to consider, creates 2^bits clusters
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        freq: Boolean. Whether to return frequencies. Defaults to False
        threshold: minimum number of documents a gram has to be present in.
                    Defaults to 5.
        outdir: directory to write to. Defaults to directory of fpath
    Output: 
        doc: Counter of document frequencies
        gram2id_df: gram to id dict for doc freqs
        final_counts: list of Counters of term frequencies for each doc
    """
    
    #check if directory is zip archive or directory and act accordingly
    if not zipfile.is_zipfile(indir):
        #list the files in the directory
        files = [os.path.join(indir,f) for f in os.listdir(indir) if \
                    os.path.splitext(f)[1] == ".txt"]
        #create directory for results
        if outdir is None:
            outdir = os.path.join(indir,"cluster_results")
        #check if directory exists, if not create direcotry
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        #set zip_corpus to None
        zip_corpus = None
    else:
        #files is the namelist of the zip archive
        zip_corpus = zipfile.ZipFile(indir)
        files = zip_corpus.namelist()
        #crete directory for results in the directory of the zip archive
        if outdir is None:
            #get the directory of the zip archive
            tmp = os.path.split(indir)[0]
            outdir = os.path.join(tmp,"cluster_results")
        #cheack if the directory exists, if not create the directory
        if not os.path.exists(outdir):
            os.mkdir(outdir)
    #initialize Counter object which keep track of corupus wide ngram counts
    corpus = Counter()
    #initialize Counter object which keeps track of the number of documents
    #a word is in
    doc = Counter()
    #initialize dictionary which holds all of the information for each 
    #document to be saved. Keys are file paths
    info = {}
    #index through all text files in directory and save sngram counts
    for fpath in files:
        logging.info("Computing N-grams for file %s" %fpath)
        output = cluster_ngram_file_output2(fpath,n,clusters,bits,stem,
                                            stop_words,punctuation, freq, 
                                            outdir = outdir,
                                            zipped = zip_corpus)
        #add counts to corpus
        corpus += output[1]
        #add ngrams to document frequency counter
        doc += Counter(output[1].keys())
        #add information to info to be saved later
        info[output[0]] = output[1:]
    #if freq == True normalize the corpus counts
    #also go through the corpus counts and assign IDs by most common
    gram2id_corpus = {}
    id2gram_corpus = {}
    if freq:
        corpus_total = sum(corpus.values())
    for gramID, (gram,count) in enumerate(corpus.most_common()):
        if freq:
            corpus[gram] /= corpus_total
        gram2id_corpus[gram] = gramID
        id2gram_corpus[gramID] = gram
    #go through the output for every file and delete the grams if their
    #document frequency is not larger than threshold
    #keep track of term frequencies
    final_counts = []
    for fPath in info.keys():
        #index through grams and place those with high doc frequency in 
        #new counter
        counts = Counter()
        gram2id = {}
        id2gram = {}
        for gram in info[fPath][0]:
            if doc[gram] >= threshold:
                #if freq == True, add frequncies
                if freq:
                    counts[gram] = info[fPath][1][gram]
                else:
                    counts[gram] = info[fPath][0][gram]
                #add to gram2id
                gram_id = info[fPath][2][gram]
                gram2id[gram] = gram_id
                #add to id2gram
                id2gram[gram_id] = info[fPath][3][gram_id]
                
        #save the output
        save_output(fPath,n,counts,gram2id,id2gram)
        #add to final_counts
        final_counts.append(counts)
    #go through the grams in corpus and put those with high doc frequency in
    #a new counter
    corpus_tmp = Counter()
    gram2id_tmp = {}
    id2gram_tmp = {}
    for gram in corpus:
        if doc[gram] >= threshold:
            #delete from corpus
            corpus_tmp[gram] =  corpus[gram]
            #delete from gram2id_corpus and id2gram_corpus
            gram_id = gram2id_corpus[gram]
            gram2id_tmp[gram] =  gram_id
            id2gram_tmp[gram_id] =  id2gram_corpus[gram_id]
    #save the counts/frequencies and ids for the whole corpus
    total_file = os.path.join(outdir,"corpus_total_cluster")
    save_output(total_file,n,corpus_tmp,gram2id_tmp,id2gram_tmp)
    
    #save document frequencies
    #iterate over grams and assign ids by largest doc frequencies 
    gram2id_df = {}
    id2gram_df = {}
    for gram_id, (gram,count) in enumerate(doc.most_common()):
        gram2id_df[gram] = gram_id
        id2gram_df[gram_id] = gram
    doc_file = os.path.join(outdir,"doc_freqs_cluster")
    save_output(doc_file,n,doc,gram2id_df,id2gram_df)
    
    return(doc,gram2id_df,final_counts)


def main():
    parser = argparse.ArgumentParser(description='Create hierarchical word' +
                                     ' clusters from a corpus, following' +
                                     ' Brown et al. (1992). \
                                     Then replace words with clusters and \
                                     compute ngrams')
                                     
    #brown clustering arguments
    parser.add_argument('--corpus_file', help='input file for clustering, \
                        one document per line, with whitespace-separated \
                        tokens. Use if a file already exists',
                        dest = "brown_file", default = None)
    parser.add_argument('--max_vocab_size', help='maximum number of words in' +
                        ' the vocabulary (a smaller number will be used if' +
                        ' there are ties at the specified level)',
                        default=None, type=int)
    parser.add_argument('--min_word_count', help='minimum word count to' +
                        'include a word in the vocabulary. (default: 1)',
                        default=1, type=int)
    parser.add_argument('--batch_size', help='number of clusters to merge at' +
                        ' one time (runtime is quadratic in this value)',
                        default=1000, type=int)
    parser.add_argument('--lower', help='lowercase the input',
                        action='store_true')
    parser.add_argument('--save_clusters',dest = "save_clusters",
                        help = "directory to save cluster information to, \
                        defaults to directory of brown_file", default = None)
    parser.add_argument("--cluster_file",dest = "cluster_file",
                        help = "if brown clustering already run, the filepath \
                        for a .pkl file containing the clusters", default = None)
    #creating corpus file arguments
    parser.add_argument('--write_corpus',dest = "write_corpus",
                        help = "write a corpus file with one document per line \
                        from the txt files in indir",default = False,
                        action = "store_true")
    parser.add_argument('--corpus_outdir',dest = "corpus_outdir",
                        help = "directory to write corpus file to, defaults \
                        to a subdirectory of the directory which contains the \
                        corpus named brown_file",
                        default = None)
    #ngram arguments
    parser.add_argument("indir",help = "directory of txt files to get cluster \
                        ngrams from")
    parser.add_argument("N", type = int, help = "order of Ngram")
    parser.add_argument("bits", type = int, help = "bit length of clusters, \
                        creates 2^bits clusters")
    parser.add_argument("--stem",dest = "stem",type = str,
                        help = "Stemmer: 'snowball','porter','lemma', defaults \
                        to None", default = None)                        
    parser.add_argument("--no_punct",dest = "punct", action = "store_false",
                        default = True, help = "discard punctuation")
    parser.add_argument("--no_stop",dest = "stop_words", action = "store_false",
                        default = True, help = "discard stop words")                       
    parser.add_argument("--freq", dest = "freq", action = "store_true",
                        default = True, help = "save frequencies rather than \
                                                counts")       
    parser.add_argument("--threshold", dest = "thresh", type = int,
                        default = 5, help = "minimum number of documents an \
                        ngram must be in, defaults to 5")                                                
    parser.add_argument("--outdir",dest = "outdir",default = None, type = str,
                        help = "outdir to write results to, defaults to \
                        indir/results")    
    
    args = parser.parse_args()
    
    main_with_args(args.brown_file,args.max_vocab_size,args.min_word_count,
                   args.batch_size,args.lower,args.save_clusters,
                   args.cluster_file,args.write_corpus,args.corpus_outdir,
                   args.indir,args.N,args.bits,args.stem,args.punct,
                   args.stop_words,args.freq,args.thresh,args.outdir)


def main_with_args(brown_file,max_vocab_size,min_word_count,batch_size,lower,
                   save_clusters,cluster_file,write_corpus,corpus_outdir,
                   indir,N,bits,stem,punct,stop_words,freq,thresh,outdir):
    """Wrapper for the main method with arguments so it can be called later"""
    #if brown clustering already run, just get load the cluster file
    if cluster_file is not None:
        with open(cluster_file,"rb") as f:
            clusters = pickle.load(f)
    else:
        #if write_corpus isn't None, first write the corpus
        if write_corpus:
            out = write_corpus_file(indir,corpus_outdir,
                                    stem, punct)
            #set args.brown_file to be the file write_corpus_file writes to
            brown_file = out

        clusters = get_clusters(brown_file,lower,max_vocab_size,
                                min_word_count,batch_size)
        #save clusters to args.save_clusters
        #if args.save_clusters is None, default to 
        #args.brown_file/brown_file
        if save_clusters is None:
            save_clusters = os.path.split(brown_file)[0]
            
        #save clusters to args.save_clusters/brown_clusters.pkl
        save_clusters = os.path.join(save_clusters,
                                          "brown_cluster.pkl")
        with open(save_clusters,"wb") as f:
            pickle.dump(clusters,f)
    #compute and write cluster ngrams
    return(cluster_ngrams_from_dir2(indir,N,clusters,bits, stem,
                            stop_words, punct, freq,
                            thresh, outdir) )   
if __name__ == "__main__":
    main()