# -*- coding: utf-8 -*-
"""
Runs all of the feature extraction scripts and combines the output

@author: Eli
"""
import six
from six.moves import configparser	
from six.moves import cPickle as pickle
from gensim import matutils
from gensim import models
from scipy import sparse
import os
import sys
import zipfile
import ast
#add all the subdirectories to the path
sys.path.append("ngrams")
sys.path.append("dependencies")
sys.path.append("clusters")
sys.path.append("phrases")
sys.path.append("word2vec")
sys.path.append("../3-feature-analysis/lda")
sys.path.append("../3-feature-analysis/hdp")

import gram_lda as lda
import gram_hdp as hdp
import ngrams
#import sngrams
import clusters
import phrases
import embedding
import doc2vec
import docLoader

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s\t%(message)s')


def create_gramDict(docfreq,gram2id):
    """Creates a gramDict from a Counter which has the document frequencies
    Input:
        docfreq: Counter of document frequencies
        gram2id: dict with grams as keys and ids as values
    Output:
        gd: gramDict from input
    """
    
    return(lda.gramDict(gram2id,docfreq))
    
def doc_bow(termfreq,gd):
    """Loads a bag of words representation of a document from a Counter
        of term frequencies
    Input:
        termfreq: Counter of term frequencies
        gd: gramDict with ids already loaded
    Output:
        bow: bag of words representation for use in corpus
    """
    #go through term frequencies and convert to bag of words
    bow = []
    for gram,count in six.iteritems(termfreq):
        #create a (gram_id,gram_count) tuple
        gram_id_count = (gd.token2id["-".join(gram)],count)
        #add to bow
        bow.append(gram_id_count)
    return(bow)
    
def docs_bow(termfreqs,gd):
    """Loads a bag of words representation of a document from a list of 
        Counters of term frequencies
    Input:
        termfreqs: a list of Counters of term frequencies
        gd: a gramDict with ids already loaded
    Output:
        corpus: a bag of words representation of the documents
    """
    corpus = []
    for termfreq in termfreqs:
        corpus.append(doc_bow(termfreq,gd))
    return(corpus)

def to_sparse(corpus):
    """Converts a corpus into a scipy csc sparse matrix
    Input:
        corpus: corpus in bag of words format
    Output:
        sparse: corpus in sparse csc matrix format
    """
    sparse = matutils.corpus2csc(corpus,num_docs = len(corpus))
    return(sparse.T)


    
def ngram_feats_eff(indir,n,tfidf = False, stem = None, keep_stop_words = True, tag = None, 
           tag_pattern = None, punctuation = True, threshold = 5,
           split_clauses = False,outdir = None):
    """Runs main in ngrams.py and sparse matrix and Dictionary
    Input:
        indir: path to directory of txt files
        n: order of n gram
        tfidf: Boolean. do tfidf
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        keep_stop_words: Boolean. Whether to include stopwords. Defaults to True
        tag: {'ap','nltk','stanford',None}. What POS tagger to use. Defaults 
                to None
        tag_pattern: list of of tag patterns to allow in simplified form.
                     Defaults to None. if tag_pattern = "default", 
                     use default tag pattern.
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        threshold: minimum number of documents a gram has to be present in.
                    Defaults to 5.
        split_clauses: Boolean. Split on clauses
        outdir: directory to write to. Defaults to indir/results
    Output:
        mat: sparse csc term document matrix
        dictionary: Dictionary object
    """
    
    mat,dictionary = ngrams.main(indir,n,tfidf,stem,keep_stop_words,tag,
                                         tag_pattern,punctuation,
                                         threshold,split_clauses,outdir) 
    return(mat,dictionary)
        
        
def sngram_feats_eff(indir,n,tfidf = False, relations = [None], stem = None,
                     keep_stop_words = True, rel = False, threshold = 5, 
                     split_clauses = False,outdir = None):
    """Runs main in sngrams
    Input:
        indir: file path for input directory (only looks at .txt files)
        n: Number of words in sn-gram
        tfidf: Boolean. do tfidf
        relations: relations to exclude, defaults to [None]
        stem: {'snowball','porter',None} what kind of stemmer to use. 
                Defaults to None.
        keep_stop_words: Boolean. Whether to include stopwords. Defaults to True
        rel: Boolean. Whether to include relations in the sngrams. Defaults to
            False.
        split_clauses: Boolean. Split on clauses
        outdir: file path for output directory, defaults to indir/results
                if outdir = None   
    Output:
        mat: sparse csc term document matrix
        dictionary: Dictionary object
    """
    
    mat,dictionary = sngrams.main(indir,n,tfidf,relations,stem,keep_stop_words,
                                  rel,threshold,split_clauses,outdir)
    return(mat,dictionary)
                
def cluster_feats(brown_file,max_vocab_size,min_word_count,batch_size,lower,
                   save_clusters,cluster_file,write_corpus,corpus_outdir,
                   indir,N,bits,stem,punct,keep_stop_words,freq,thresh,outdir):
    """Runs main_with_args in clusters.py. Too many inputs to write here.
    Input:
        Check clusters.py and the wiki
    Output:
        gd: gramDict
        corpus: corpus as bag of words
    """
    doc,g2id,counts = clusters.main_with_args(brown_file,max_vocab_size,
                   min_word_count,batch_size,lower,
                   save_clusters,cluster_file,write_corpus,corpus_outdir,
                   indir,N,bits,stem,punct,keep_stop_words,freq,thresh,outdir)  
                   
    #get gramDict and corpus
    gd = create_gramDict(doc,g2id)
    #drop everything with less than thresh document frequency
    gd.drop_below(thresh)
    corpus = docs_bow(counts,gd)
    
    return(gd,corpus)
    
def phrase_feats_eff(indir,tfidf = False, stem = None, keep_stop_words = True, 
                     tag = None, allowed_tags = ["N"], punctuation = True, 
                     threshold = 5, split_clauses = False,
                     outdir = None):
    """Runs main in phrases.py and sparse matrix and Dictionary
    Input:
        indir: path to directory of txt files
        n: order of n gram
        tfidf: Boolean. do tfidf
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        keep_stop_words: Boolean. Whether to include stopwords. Defaults to True
        tag: {'ap','nltk','stanford',None}. What POS tagger to use. Defaults 
                to None
        tag_pattern: list of of tag patterns to allow in simplified form.
                     Defaults to None. if tag_pattern = "default", 
                     use default tag pattern.
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        freq: Boolean. Whether to return frequencies. Defaults to False
        threshold: minimum number of documents a gram has to be present in.
                    Defaults to 5.
        split_clauses: Boolean. Split on clauses
        outdir: directory to write to. Defaults to indir/results
    Output:
        mat: sparse csc term document matrix
        dictionary: Dictionary object
    """
    
    mat,dictionary = phrases.main(indir,tfidf,stem,keep_stop_words,tag,
                                         allowed_tags,punctuation,
                                         threshold,split_clauses,outdir) 
    return(mat,dictionary)
        


def word2vec_feats(indir,stem = None, keep_stop_words = True, punctuation = True,
               split_clauses = False, size=100, window=5, min_count=5,
               workers=1, finalize = True,mem_efficient = False,
               outfile = None):
    """runs load_train_save and returns the feature matrix and file names
    Input: see embedding.py
    Output:
        mat: word2vec feature matrix
    """
    
    mat,fnames = embedding.load_train_save(indir,stem,keep_stop_words,punctuation,
                                           split_clauses,size,window,min_count,
                                           workers,finalize,mem_efficient,
                                           outfile)
                                           
    return(mat)

def doc2vec_feats(indir,stem = None, keep_stop_words = True, punctuation = True,
               split_clauses = False, size=100, window=5, min_count=5,
               workers=1, outfile = None):
    """runs load_train_save and returns the feature matrix and file names
    Input: see doc2vec.py
    Output:
        mat: word2vec feature matrix
    """
    
    mat,fnames =doc2vec.load_train_save(indir,stem,keep_stop_words,punctuation,
                                           split_clauses,size,window,min_count,
                                           workers,outfile)
    return(mat)
    
def get_fnames(fList, zipped = None,split_clauses = False):
    """Gets the file names from a list of file paths
    Input:
        fList: list of file paths
        zipped: a ZipFile object to read files in fList from. Defaults to
                    None.
        split_clauses: whether clauses count as their own documents. Defaults
            to False.
    Output:
        fnames: names of files (file codes)
    """
    #if not splitting the clauses, then just get the file names
    if not split_clauses:
        #get names
        fnames = []
        for f in fList:
            #strip extension and path to just get the file code
            fnames.append(os.path.splitext(os.path.basename(f))[0])  
        return(fnames)
    #if splitting the clauses, find the number of clauses in each document
    #using docLoader to assing the right documents
    else:
        fnames = []
        for f in fList:
            #use docLoader to find the clauses, and count them
            clauses = docLoader.doc_sents(f,zipped = zipped,
                                          clauses = split_clauses)
            #get the number of clauses
            n_clauses = len(clauses)
            #isolate file name
            file_name = os.path.splitext(os.path.basename(f))[0]
            #add file_name n_clauses times
            for i in range(n_clauses):
                fnames.append(file_name)
        return(fnames)
    
    
def get_fnames_dir(indir,split_clauses = False):
    """Gets the file names (file codes) for all text files in a directory
    Input:
        indir: path to a directory
        split_clauses: whether clauses count as their own documents
    Output:
        fnames: file names (file codes) of files
    """
    #check if indir is a zip file and act accordingly
    if not zipfile.is_zipfile(indir):
        #get file paths
        files = [os.path.join(indir,f) for f in os.listdir(indir) if \
                    os.path.splitext(f)[1] == ".txt"]    
        zip_corpus = None
    else:
        zip_corpus = zipfile.ZipFile(indir)
        files = zip_corpus.namelist()
    #get file names
    fnames = get_fnames(files,zip_corpus,split_clauses)
    return(fnames)

    
def do_lda(corpus,names,gd,num_topics,indir,kind,outfile = None): 
    """Runs train_save in gram_lda.py and returns the output
    Input:
        corpus: streaming corpus iterator
        names: the names of the documents in corpus
        gd: a gramDict with ids already taken from a file
        num_topics: number of topics 
        indir: directory where the files are coming from
        kind: String saying what lda was run on (ngrams,phrases,etc) for 
                differentiating the saved files
        outfile: file name to write to (without extension). Defaults to
                indir/lda_results/
    Output:
        dt_mat: a n_documents x n_topics matrix 
        topics: list of lists of (word,frequency) pairs for each topic  
    """
    if outfile is None:
        #if indir is a zip archive, deal with it differently
        if not zipfile.is_zipfile(indir):
            outdir = os.path.join(indir,"lda_results")
            
        else:
            #get the directory of the zip archive
            tmp = os.path.split(indir)[0]
            outdir = os.path.join(tmp,"lda_results")
        outfile = os.path.join(outdir,kind + "_lda")
    else:
        outdir = os.path.split(outfile)[0]
    #check if directory exists, if not create direcotry
    if not os.path.exists(outdir):
        os.mkdir(outdir)        
    doc_tops,dt_mat,topics = lda.train_save(corpus,names,gd,num_topics,outfile)
                                             
    return(dt_mat,topics)

def do_hdp(corpus,names,gd,indir,kind,outfile):
    """Runs train_save in gram_lda.py and returns the output
    Input:
        corpus: streaming corpus iterator
        names: the names of the documents in corpus
        gd: a gramDict with ids already taken from a file
        num_topics: number of topics 
        indir: directory where the files are coming from
        kind: String saying what lda was run on (ngrams,phrases,etc) for 
                differentiating the saved files
        outfile: file name to write to (without extension). Defaults to
                indir/lda_results/
    Output:
        dt_mat: a n_documents x n_topics matrix 
        topics: list of lists of (word,frequency) pairs for each topic  
    """
    if outfile is None:
        #if indir is a zip archive, deal with it differently
        if not zipfile.is_zipfile(indir):
            outdir = os.path.join(indir,"hdp_results")
            
        else:
            #get the directory of the zip archive
            tmp = os.path.split(indir)[0]
            outdir = os.path.join(tmp,"hdp_results")
        outfile = os.path.join(outdir,kind + "_hdp")
    else:
        outdir = os.path.split(outfile)[0]
    #check if directory exists, if not create direcotry
    if not os.path.exists(outdir):
        os.mkdir(outdir)        
    doc_tops,dt_mat,topics = hdp.train_save(corpus,names,gd,outfile)
                                             
    return(dt_mat,topics)    
    
def combine_mats(mats,feat_names):
    """Horizontally combines feature matrices to combine the different 
        features. Also combines the feature names to keep track of them
    Input:
        mats: a list of (sparse or dense) matrices to be combined
        feat_names: list of lsit of the feature names corresponding to 
                    each matrix
    Output:
        comb_mat: combined matrix
        comb_feats: combined feature names
    """
    
    #combine matrices
    comb_mat = sparse.hstack(mats)
    #combine feature names
    comb_feats = []
    for f in feat_names:
        comb_feats.extend(f)
    return(comb_mat.tocsr(),comb_feats)
    
def main(indir,config_file,out = None):
    """The main method, runs with options from config_file
    Input:
        indir: directory of the corpus
        config_file: a configuration file with options
        out: directory to write to. Defaults to indir/final_results"""
    #read config file    
    cp = configparser.ConfigParser()
    cp.read(config_file)
    #function for finding if a string is "None"
    #go through and start to get all the features with the options from 
    #config_file
    #feature amtrices
    mats = []
    #feature names
    names = []

    #whether to split the clauses into their own documents
    split_clauses = ast.literal_eval(cp.get("clauses","split_clauses"))
    #get file names
    logging.info("Getting file names")
    file_names = get_fnames_dir(indir,split_clauses)
    #get lda options
    lda_ngrams = ast.literal_eval(cp.get("lda","ngrams"))
    lda_phrases = ast.literal_eval(cp.get("lda","phrases"))
    lda_sngrams = ast.literal_eval(cp.get("lda","sngrams"))
    lda_num_topics = ast.literal_eval(cp.get("lda","num_topics"))
    lda_outfile = ast.literal_eval(cp.get("lda","outfile"))
    #get hdp options
    hdp_ngrams = ast.literal_eval(cp.get("hdp","ngrams"))
    hdp_phrases = ast.literal_eval(cp.get("hdp","phrases"))
    hdp_sngrams = ast.literal_eval(cp.get("hdp","sngrams"))
    hdp_outfile = ast.literal_eval(cp.get("hdp","outfile"))    

    #ngrams
    if ast.literal_eval(cp.get("ngrams","ngrams")):
        #get options
        n = ast.literal_eval(cp.get("ngrams","n"))
        tfidf = ast.literal_eval(cp.get("ngrams","tfidf"))
        stem = ast.literal_eval(cp.get("ngrams","stem"))
        keep_stop_words = ast.literal_eval(cp.get("ngrams","keep_stop_words"))
        tag = ast.literal_eval(cp.get("ngrams","tag"))
        tp = ast.literal_eval(cp.get("ngrams","tp"))
        punct = ast.literal_eval(cp.get("ngrams","punct"))
        thresh = ast.literal_eval(cp.get("ngrams","thresh"))
        outdir = ast.literal_eval(cp.get("ngrams","outdir"))
        #run ngrams
        mat,dictionary = ngram_feats_eff(indir,n,tfidf,stem,keep_stop_words,tag,tp,
                                            punct,thresh,split_clauses,outdir)
        #store sparse matrix
        mats.append(mat)
        #store feature names
        names.append(list(dictionary.id2token.values()))
        #do lda if True
        if lda_ngrams:
            #load corpus from sparse matrix
            corpus = matutils.Sparse2Corpus(mat,False)
            dt_mat,topics = do_lda(corpus,file_names,dictionary,
                                   lda_num_topics,indir,"ngrams",lda_outfile)
            #store lda matrix
            mats.append(dt_mat)
            #add topic features
            names.append(["ngrams_lda_topic_%i" %i 
                            for i in range(1,len(topics) + 1)])   
        #do hdp if True
        if hdp_ngrams:
            #load corpus from sparse matrix
            corpus = matutils.Sparse2Corpus(mat,False)
            dt_mat,topics = do_hdp(corpus,file_names,dictionary,
                                   indir,"ngrams",hdp_outfile)
            #store lda matrix
            mats.append(dt_mat)
            #add topic features
            names.append(["ngrams_hdp_topic_%i" %i 
                            for i in range(1,len(topics) + 1)])                              
    #sngrams
    if ast.literal_eval(cp.get("sngrams","sngrams")):
        n = ast.literal_eval(cp.get("sngrams","n"))
        tfidf = ast.literal_eval(cp.get("ngrams","tfidf"))
        relations = ast.literal_eval(cp.get("sngrams","relations"))
        stem = ast.literal_eval(cp.get("sngrams","stem"))
        keep_stop_words = ast.literal_eval(cp.get("sngrams","keep_stop_words"))
        rel = ast.literal_eval(cp.get("sngrams","rel"))
        thresh = ast.literal_eval(cp.get("sngrams","thresh"))
        outdir = ast.literal_eval(cp.get("sngrams","outdir"))   
        #run sngrams
        mat,dictionary = sngram_feats_eff(indir,n,tfidf,relations,
                                          stem,keep_stop_words,rel,thresh,
                                          split_clauses,outdir)
        #store sparse matrix
        mats.append(mat)
        #store feature names
        names.append(list(dictionary.id2token.values()))
        #do lda if True
        if lda_sngrams:
            #load corpus from sparse matrix
            corpus = matutils.Sparse2Corpus(mat,False)
            dt_mat,topics = do_lda(corpus,file_names,dictionary,
                                   lda_num_topics,indir,"sngrams",lda_outfile)
            #store lda matrix
            mats.append(dt_mat)
            #add topic features
            names.append(["sngrams_lda_topic_%i" %i 
                            for i in range(1,len(topics) + 1)])  

        #do hdp if True
        if hdp_sngrams:
            #load corpus from sparse matrix
            corpus = matutils.Sparse2Corpus(mat,False)
            dt_mat,topics = do_hdp(corpus,file_names,dictionary,
                                   indir,"sngrams",hdp_outfile)
            #store lda matrix
            mats.append(dt_mat)
            #add topic features
            names.append(["sngrams_hdp_topic_%i" %i 
                            for i in range(1,len(topics) + 1)])  

                                
    #clutering
    if ast.literal_eval(cp.get("cluster","cluster")):
        brown_file = ast.literal_eval(cp.get("cluster","brown_file"))
        max_vocab_size = ast.literal_eval(cp.get("cluster","max_vocab_size"))
        min_word_count = ast.literal_eval(cp.get("cluster","min_word_count"))
        batch_size = ast.literal_eval(cp.get("cluster","batch_size"))
        lower = ast.literal_eval(cp.get("cluster","lower"))
        save_clusters = ast.literal_eval(cp.get("cluster","save_clusters"))
        cluster_file = ast.literal_eval(cp.get("cluster","cluster_file")) 
        write_corpus = ast.literal_eval(cp.get("cluster","write_corpus"))
        corpus_outdir = ast.literal_eval(cp.get("cluster","corpus_outdir"))
        N = ast.literal_eval(cp.get("cluster","N"))
        bits = ast.literal_eval(cp.get("cluster","bits"))
        stem = ast.literal_eval(cp.get("cluster","stem"))
        punct = ast.literal_eval(cp.get("cluster","punct"))
        keep_stop_words = ast.literal_eval(cp.get("cluster","keep_stop_words"))
        freq = ast.literal_eval(cp.get("cluster","freq"))
        thresh = ast.literal_eval(cp.get("cluster","thresh"))
        outdir = ast.literal_eval(cp.get("cluster","outdir "))
        
        #run clusters
        gd,corpus = cluster_feats(brown_file,max_vocab_size,min_word_count,
                                  batch_size,lower,save_clusters,cluster_file,
                                  write_corpus,corpus_outdir,indir,N,bits,stem,
                                  punct,keep_stop_words,freq,thresh,outdir)
        
        #store sparse matrix
        mats.append(to_sparse(corpus))
        #store feature names
        names.append(list(gd.token2id.keys()))
    #phrases
    if ast.literal_eval(cp.get("phrases","phrases")):
        tfidf = ast.literal_eval(cp.get("ngrams","tfidf"))
        stem = ast.literal_eval(cp.get("phrases","stem"))
        keep_stop_words = ast.literal_eval(cp.get("phrases","keep_stop_words"))
        tag = ast.literal_eval(cp.get("phrases","tag"))
        allowed_tags = ast.literal_eval(cp.get("phrases","allowed_tags"))
        punct = ast.literal_eval(cp.get("phrases","punct"))
        thresh = ast.literal_eval(cp.get("phrases","thresh"))
        outdir = ast.literal_eval(cp.get("phrases","outdir")) 
        mat,dictionary = phrase_feats_eff(indir,tfidf,stem,keep_stop_words,tag,
                                           allowed_tags,punct,thresh,
                                           split_clauses,outdir)
        #store sparse matrix
        mats.append(mat)
        #store feature names
        names.append(list(dictionary.id2token.values()))
        #do lda if True
        if lda_phrases:
            #load corpus from sparse matrix
            corpus = matutils.Sparse2Corpus(mat,False)
            dt_mat,topics = do_lda(corpus,file_names,dictionary,
                                   lda_num_topics,indir,"phrases",lda_outfile)
            #store lda matrix
            mats.append(dt_mat)
            #add topic features
            names.append(["phrases_lda_topic_%i" %i 
                            for i in range(1,len(topics) + 1)])
        #do hdp if True
        if hdp_phrases:
            #load corpus from sparse matrix
            corpus = matutils.Sparse2Corpus(mat,False)
            dt_mat,topics = do_hdp(corpus,file_names,dictionary,
                                   indir,"phrases",hdp_outfile)
            #store lda matrix
            mats.append(dt_mat)
            #add topic features
            names.append(["phrases_hdp_topic_%i" %i 
                            for i in range(1,len(topics) + 1)])   
    #word2vec
    if ast.literal_eval(cp.get("word2vec","word2vec")):
        stem = ast.literal_eval(cp.get("word2vec","stem"))
        keep_stop_words = ast.literal_eval(cp.get("word2vec","keep_stop_words"))
        punct = ast.literal_eval(cp.get("word2vec","punct"))
        size = ast.literal_eval(cp.get("word2vec","size"))
        window = ast.literal_eval(cp.get("word2vec","window"))
        min_count = ast.literal_eval(cp.get("word2vec","min_count"))
        workers = ast.literal_eval(cp.get("word2vec","workers"))
        finalize = ast.literal_eval(cp.get("word2vec","finalize"))
        mem_efficient = ast.literal_eval(cp.get("word2vec","mem_efficient"))
        outfile = ast.literal_eval(cp.get("word2vec","outfile"))
        
        #run word2vec
        w2v_mat = word2vec_feats(indir,stem,keep_stop_words,punct,split_clauses,
                                 size,window,min_count,workers,finalize,
                                 mem_efficient,outfile)
        #store word2vec matrix
        mats.append(w2v_mat)
        #store names
        names.append(["word2vec_feat_%i" %i for i in range(w2v_mat.shape[1])])

    #doc2vec
    if ast.literal_eval(cp.get("doc2vec","doc2vec")):
        stem = ast.literal_eval(cp.get("doc2vec","stem"))
        keep_stop_words = ast.literal_eval(cp.get("word2vec","keep_stop_words"))
        punct = ast.literal_eval(cp.get("doc2vec","punct"))
        size = ast.literal_eval(cp.get("doc2vec","size"))
        window = ast.literal_eval(cp.get("doc2vec","window"))
        min_count = ast.literal_eval(cp.get("doc2vec","min_count"))
        workers = ast.literal_eval(cp.get("doc2vec","workers"))
        outfile = ast.literal_eval(cp.get("doc2vec","outfile"))
        
        #run word2vec
        w2v_mat = doc2vec_feats(indir,stem,keep_stop_words,punct,split_clauses,
                                 size,window,min_count,workers,outfile)
        #store word2vec matrix
        mats.append(w2v_mat)
        #store names
        names.append(["doc2vec_feat_%i" %i for i in range(w2v_mat.shape[1])])    
        
    
    logging.info("combining feature matrices")                               
    #combine the matrices and feature names
    comb_mat,comb_feat = combine_mats(mats,names)
    #create directory for results
    if out is None:
        #if indir is a zip archive, deal with it differently
        if not zipfile.is_zipfile(indir):
            out = os.path.join(indir,"final_results")
        else:
            #get the directory of the zip archive
            tmp = os.path.split(indir)[0]
            out = os.path.join(tmp,"final_results")
    #check if directory exists, if not create direcotry
    if not os.path.exists(out):
        os.mkdir(out)
    outdir = out    
    logging.info("saving output")
    #pickle the matrix and the feature names sperately
    with open(os.path.join(outdir,"feature_mat.pkl"),"wb") as f:
        pickle.dump(comb_mat,f,protocol = 2)
        
    with open(os.path.join(outdir,"feature_names.pkl"),"wb") as f:
        pickle.dump(comb_feat,f,protocol = 2)
    #pickle file names too
    with open(os.path.join(outdir,"file_names.pkl"),"wb") as f:
        pickle.dump(file_names,f,protocol = 2)
        
    #also return the feature matrix and feature names for use in other 
    #functions
    return(comb_mat,comb_feat)

def usage():
    """Prints usage information"""
    
    use = """Usage:
python run_all.py indir outdir configfile
                or
python runall.py indir configfile"""
    print(use)
    
if __name__ == "__main__":
    
    if len(sys.argv) < 3 or len(sys.argv) > 4:
        usage()
    elif len(sys.argv) == 3:
        main(sys.argv[1],sys.argv[2])
    else:
        main(sys.argv[1],sys.argv[2],sys.argv[3])
            