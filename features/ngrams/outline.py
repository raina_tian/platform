# -*- coding: utf-8 -*-
"""
Created on Wed May 20 13:10:57 2015

@author: elliott
"""

import nltk

# sentence tokenizing
nltk.sent_tokenize()

# word tokenizing
nltk.word_tokenize()

# snowball stemmer
from nltk.stem.snowball import SnowballStemmer
snow = SnowballStemmer('english')
return [snow.stem(x) for x in tokens]

# porter stemmer
from nltk.stem.porter import PorterStemmer
porter = PorterStemmer()
return [porter.stem(x) for x in tokens]

# lemmatizer
from nltk.stem.wordnet import WordNetLemmatizer
lemma = WordNetLemmatizer()
return [lemma.lemmatize(x) for x in tokens]


# POS tagging

# nltk default:
nltk.pos_tag(text)

# textblob ap tagger:
from textblob_aptagger import PerceptronTagger

class TagTokens(PerceptronTagger):
    "tag the words in a sentence"
    def tag(self, words):
        prev, prev2 = self.START
        tags = []
        context = self.START + [self._normalize(w) for w in words] + self.END
        for i, word in enumerate(words):
            tag = self.tagdict.get(word)
            if not tag:
                features = self._get_features(i, word, context, prev, prev2)
                tag = self.model.predict(features)
            tags.append(tag)
            prev2 = prev
            prev = tag
        return tags

# stanford tagger

 #http://www.nltk.org/_modules/nltk/tag/stanford.html


# generating n-grams

for n in range(2,5):
    gramns = nltk.ngrams(n)
    
    
# trim thresholds
from collections import Counter
C = Counter()    
print(C.most_common())
# term frequencies and document frequencies

# stopwords
from nltk.corpus import stopwords as stop
stopwords = set(stop.words('english'))
tokens = [t for t in tokens if t not in stopwords]

# write dictionary
gram2id = defaultdict(lambda: None)
id2gram = defaultdict(lambda: None)
for gramid, (gram, freq) in enumerate(gramcounter.most_common()):    
    gram2id[gram] = gramid
    id2gram[gramid] = gram
    
# write frequencies
    


