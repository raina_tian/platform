# -*- coding: utf-8 -*-
"""
Computes N-grams and frequencies. Writes output. Options for stemming and
thresholding

@author: Eli
"""



import nltk
from nltk.stem.snowball import SnowballStemmer
from nltk.stem.porter import PorterStemmer  
from nltk.stem import WordNetLemmatizer
from nltk.tag.stanford import StanfordPOSTagger as POSTagger
from textblob_aptagger import PerceptronTagger
from nltk.corpus import stopwords
from collections import Counter, defaultdict
import string
import csv
import os
import re
import argparse
import logging
import six
from six.moves import cPickle as pickle
import zipfile
import ngrams_corpus

import sys
sys.path.append("..")
from docLoader import *

#top level variable which sontains english stop words as a set
stop = set(stopwords.words('english'))

#logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s\t%(message)s')

class keydefaultdict(defaultdict):
    """Class to assign the key as the value in a defaultdict"""
    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError( key )
        else:
            ret = self[key] = self.default_factory(key)
            return ret



#top level variable holding default tag patterns and dict for conversion to
#universal tag set
default_tagpatterns = set(['AN','NN','AAN','ANN','NAN','NPN',
                                'VN','VAN','VNN','VPN','ANV','NVV','VDN'])

default_tagset = set(''.join(default_tagpatterns))


tagdict = keydefaultdict(lambda x: x,
                        {'NN':'N',
                        'NNS':'N',
                        'NNP':'N',
                        'NNPS':'N',
                        'JJ':'A',
                        'JJR':'A',
                        'JJS':'A',
                        'VBG':'A',
                        'RB':'A',
                        'DT':'D',
                        'IN':'P',
                        'TO':'P',
                        'VB':'V',
                        'VBD':'V',
                        'VBN':'V',
                        'VBP':'V',
                        'VBZ':'V'})




class APTagger(PerceptronTagger):
    "Helper class to use Averaged Perceptron Tagger with list of words"
    def tag(self, words):
        prev, prev2 = self.START
        tags = []
        context = self.START + [self._normalize(w) for w in words] + self.END
        for i, word in enumerate(words):
            tag = self.tagdict.get(word)
            if not tag:
                features = self._get_features(i, word, context, prev, prev2)
                tag = self.model.predict(features)
            tags.append(tag)
            prev2 = prev
            prev = tag
        return tags
    def tag_sents(self,sents):
        """Tag sentences that are already sperated and word tokenized"""
        tags = []
        for sent in sents:
            tags.append(self.tag(sent))
        return(tags)

def custom_ngrams(words,n):
    """Faster n gram generation than nltk.ngrams
    Input:
        words: word tokenized sentence
        n: order of ngram
    Output:
        ngrams: list of ngrams
    """
    ngrams = zip(*[words[i:] for i in range(n)])
    return(ngrams)

def word_tokenize(words):
    """Faster word tokenization than nltk.word_tokenize
    Input:
        words: a string to be tokenized
    Output:
        tokens: tokenized words
    """
    tokens = re.findall(r"[a-z]+-?[a-z]+", words.lower(),
                        flags = re.UNICODE | re.LOCALE)
    return(tokens)

#@profile
def ngrams_from_sents(sents,n, stem = None, stop_words = True, tag = None,
                      tag_pattern = None, punctuation = True):
    """Gets the ngrams from a list of sentences
    Input:
        sents: list of sentences as strings
        n: order of n gram
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        tag: {'ap','nltk','stanford',None}. What POS tagger to use. Defaults 
                to None
        tag_pattern: list of of tag patterns to allow in simplified form.
                     Defaults to None. if tag_pattern = "default", 
                     use default tag pattern.
        punctuation: Boolean. Whether to include punctuation. Defaults to True
    Output:
        ngrams: list of ngrams as tuples
    """
       
    

    #tag sentences first
    if tag is not None:
        #tokenize the sentences
        tmp = []
        for sent in sents:
            tmp.append([word.lower() for word in word_tokenize(sent)])
        sents = tmp
        if tag == 'ap':
            #tag words
            tagger = APTagger()
            tags = tagger.tag_sents(sents)
            #go from universal/penn treebank tagset to simple tagset
            tags = [[tagdict[t] for t in tags[i]] for i in range(len(tags))]
            #join tags and words
            #sents = [['::'.join(tagWord) for tagWord in zip(sents[i],tags[i])] 
                        #for i in range(len(sents))]
        
        elif tag == 'ntlk':
            #tag words
            tags = nltk.pos_tag_sents(sents)
            #extract the tags without the words
            tags = [[tagdict[tagWord[1]] for tagWord in tag[i]] for i \
                    in range(len(sents))]
            #join tags and words
            #sents = [['::'.join(t) for t in tags[i]] for i in range(len(sents))]
        
        elif tag == 'stanford':
           model = '../../stanford/postagger/models/english-bidirectional-distsim.tagger' 
           jar = '../../stanford/postagger/stanford-postagger-3.5.2.jar'
           tagger = POSTagger(model,jar,encoding = 'utf-8')
           #tag words
           tags = tagger.tag_sents(sents)
           #extract the tags without the words
           tags = [[tagdict[tagWord[1]] for tagWord in tag[i]] for \
                    i in range(len(sents))]
           #join tags and words
           #sents = [['::'.join(t) for t in tags[i]] for i in range(len(sents))]
           
        else:
            #raise a value error if an unsupproted tagger is included
            raise ValueError('Not an available tagger')
        
    
    
    
    #initialize stemmer if stem is not None
    if stem is not None:
        if stem == 'porter':
            stemmer = PorterStemmer()
        elif stem == 'snowball':
            stemmer = SnowballStemmer("english")
        elif stem == 'lemma':
            stemmer = WordNetLemmatizer()
            #add stem as another name for lemmatize
            stemmer.stem = stemmer.lemmatize
        else:
            #raise a value error if a wrong stemmer is chosen
            raise ValueError('Not an available stemmer')    
    #iterate through sentences and get ngrams
    ngrams = []
    for i,words in enumerate(sents):
        if tag is None:
            #if tag is None then word tokenization hasn't happend
            words = word_tokenize(words)
        #stem words if stem is not None
        if stem is not None:
            words = [stemmer.stem(word) for word in words]
        #join tags and words if tag is not None
        if tag is not None:
            words = ['::'.join(tagWord) for tagWord in zip(words,tags[i])]
        #remove stop words if stop = False
        if not stop_words:
            words = [word for word in words if not word.split("::")[0] in stop]
        #remove punctuation if punctuation is false
        if not punctuation:
            pun = string.punctuation
            words = [word for word in words if not word.split("::")[0] in pun]
            
        #get n grams and add to ngrams list
        sent_grams = [gram for gram in custom_ngrams(words,n)]
        #if tag_pattern isn't None, go through sent_grams and only keep those
        #ngrams with the proper tag pattern
        if tag_pattern is not None:
            #assign default tag pattern if tag_pattern == 'default'
            if tag_pattern == 'default':
                tag_pattern = default_tagpatterns
            tmp = []
            #maybe make this a list comprehension?
            for gram in sent_grams:
                #squash ngram
                squash = '::'.join(gram)
                
                #get tags by getting every odd element
                tags_squash = squash.split("::")[1::2]
                #convert tags into their reduced form
                #e.g. VB and VBN go to V
                tags_squash = [t for t in tags_squash]
                #check if the tag pattern is allowed
                if ''.join(tags_squash) in tag_pattern:
                    tmp.append(gram)
            sent_grams = tmp

            
        ngrams.extend(sent_grams)  
        
    return(ngrams)
    
    
def ngrams_from_file(fpath, n, stem = None, stop_words = True, tag = None,
                     tag_pattern = None, punctuation = True, zipped = None):
    """Gets the ngrams from a file
    Input:
        fpath: path of file
        n: order of n gram
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        tag: {'ap','nltk','stanford',None}. What POS tagger to use. Defaults 
                to None
        tag_pattern: list of of tag patterns to allow in simplified form.
                     Defaults to None. if tag_pattern = "default", 
                     use default tag pattern.
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        zipped: a ZipFile object to read fPath from if the directory is 
                zipped. Defaults to None
    Output:
        ngrams: list of ngrams as tuples    
    """     
    #get sentences from file
    sents = doc_sents(fpath,zipped = zipped)     
    
    #get the ngrams from the sentences
    ngrams = ngrams_from_sents(sents,n,stem,stop_words,tag,tag_pattern,
                               punctuation) 
    return(ngrams)
    
def count_grams(ngrams):
    """Counts the number of ngrams
    
    Input:
        ngrams: list of ngrams
    Output:
        Counter object which counts the ngrams
    """
    
    return(Counter(ngrams))
             
def gram_counts(grams, freq = False):
    """Counts the gram frequencies, and assings IDs to each gram
    
    Input:
        grams: A list of ngrams to be counted
        freq: Boolean. Whether to return frequencies. Defaults to False
    Output:
        gramCounter: Counter object of gram counts
        gram2id: dictionary with grams as keys and IDs as values
        id2gram: dictionary with IDs as keys and grams as values
    """
    #count the grams
    gramCounter = count_grams(grams)
    #initialize id dicts
    gram2id = {}
    id2gram = {}
    #get total number of tokens if freq == True
    if freq:
        total = sum(gramCounter.values())
    #iterate over the grams and assign IDs by most common grams
    for gramID, (gram,count) in enumerate(gramCounter.most_common()):
        #divide counts by total counts if freq == true
        if freq:
            gramCounter[gram] /= total
        gram2id[gram] = gramID
        id2gram[gramID] = gram
        
    return(gramCounter,gram2id,id2gram)


def save_output(outFile,gramNum,counts,gram2id,id2gram):
    """Saves the frequency Counter object and id dictionaries.
     
         Saves 1 pkl file of output as a tuple and 2 csv files
         
     
     Input:
         outFile: file name to save to w/o file extension
         gramNum: type of n-gram (e.g. 2 = bigram, 3 = trigram, etc.)
         counts: frequency Counter object
         gram2id, id2gram: id dictionaries
    """

    #pickle the files after combining them into a tuple
    output = (counts,gram2id,id2gram)
    fName = outFile + "_" + str(gramNum) + '.pkl'
    with open(fName,'wb+') as f:
        pickle.dump(output,f,protocol = 2)
        
    #write id csv files
    idName = outFile + "_"+ str(gramNum) + "_gram" + "_id.csv"
    #check type of python and act accordingly
    if six.PY2:
        with open(idName,'wb+') as csvfile:
            writer = csv.writer(csvfile)
            #iterate through key-value pairs in id2gram and write to csv
            for gramid in sorted(id2gram.keys()):
                #if there are tags, drop them when writing to file
                gram = [g.split("::")[0] for g in id2gram[gramid]]
                #have n-grams output as word-word rather than ("word","word")
                gram = "-".join(gram)
                """ #delete Unicode BOM
                if gram.startswith(u'\ufeff'):
                    gram = gram[1:]"""
                gram = gram.encode("utf-8")
                
                writer.writerow([gramid,gram])
        #write frequency csv file
        
        freqName = outFile + "_"+ str(gramNum) + "_gram" + "_freq.csv"
        
        with open(freqName, 'wb+') as freqfile:
            writer = csv.writer(freqfile)
            #iterate through gramids and write gramid,frequency pairs to csv
            for gramid in sorted(id2gram.keys()):
                writer.writerow([gramid,counts[id2gram[gramid]]])                
    elif six.PY3:
        with open(idName,'w+', newline = '',encoding = 'utf-8') as csvfile:
            writer = csv.writer(csvfile)
            #iterate through key-value pairs in id2gram and write to csv
            for gramid in sorted(id2gram.keys()):
                #if there are tags, drop them when writing to file
                g = [gram.split("::")[0] for gram in id2gram[gramid]]
                #have n-grams output as word-word rather than ("word","word")
                writer.writerow([gramid,"-".join(g)])        
        #write frequency csv file
        
        freqName = outFile + "_"+ str(gramNum) + "_gram" + "_freq.csv"
        
        with open(freqName, 'w+',newline = '') as freqfile:
            writer = csv.writer(freqfile)
            #iterate through gramids and write gramid,frequency pairs to csv
            for gramid in sorted(id2gram.keys()):
                writer.writerow([gramid,counts[id2gram[gramid]]])        
            
def gram_counts_file(fpath, n, stem = None, stop_words = True, tag = None,
                     tag_pattern = None, punctuation = True, freq = False,
                     zipped = None):
    """Gets the ngrams and their counts from a file
    Input:
        fpath: path of file
        n: order of n gram
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        tag: {'ap','nltk','stanford',None}. What POS tagger to use. Defaults 
                to None
        tag_pattern: list of of tag patterns to allow in simplified form.
                     Defaults to None. if tag_pattern = "default", 
                     use default tag pattern.
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        freq: Boolean. Whether to return frequencies. Defaults to False
        zipped: a ZipFile object to read fPath from if the directory is 
                zipped. Defaults to None        
    Output:
        gramCounter: Counter object of gram counts
        gram2id: dictionary with grams as keys and IDs as values
        id2gram: dictionary with IDs as keys and grams as values
    """
    #get ngrams
    ngrams = ngrams_from_file(fpath,n,stem,stop_words,tag,tag_pattern,
                              punctuation, zipped = zipped)  
    #get counter and gram2id and id2gram dicts
    counts,gram2id,id2gram = gram_counts(ngrams,freq)
    #return the counter and id dicts
    return(counts,gram2id,id2gram)
 


###Probably obsolete and won't be used   
def ngram_file_output(fpath, n, stem = None, stop_words = True, tag = None,
                     tag_pattern = None, punctuation = True, freq = False,
                     outdir = None):
    """Gets the ngram counts from a document and saves output by both pickling
        the dicts and writing to csv
    Input:
        fpath: path of file
        n: order of n gram
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        tag: {'ap','nltk','stanford',None}. What POS tagger to use. Defaults 
                to None
        tag_pattern: list of of tag patterns to allow in simplified form.
                     Defaults to None. if tag_pattern = "default", 
                     use default tag pattern.
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        freq: Boolean. Whether to return frequencies. Defaults to False
        outdir: directory to write to. Defaults to directory of fpath
        """
    #default outdir to the directory of fpath if outdir is None
    if outdir is None:
        outdir = os.path.split(fpath)[0]
    #get counts (not frequencies) and id dicts
    counts,gram2id,id2gram = gram_counts_file(fpath,n,stem,stop_words,tag,
                                              tag_pattern,punctuation,freq = False)
    
    #get file name from path w/o extension
    out = os.path.split(fpath)[1]
    out = os.path.join(outdir,out)
    out = os.path.splitext(out)[0]  
    
    
    #if freq == True, normalize the counts to get frequencies
    if freq:
        freqs = counts.copy()
        total = sum(freqs.values)
        for gram in freqs.keys():
            freqs[gram] /= total
        #save output with frequencies rather than counts
        save_output(out,n,freqs,gram2id,id2gram)
    else:
        #save output with counts rather than frequencies
        save_output(out,n,counts,gram2id,id2gram)
    
    #return counts
    return(counts)

def ngram_file_output2(fpath, n, stem = None, stop_words = True, tag = None,
                     tag_pattern = None, punctuation = True, freq = False,
                     outdir = None, zipped = None):
    """Gets the ngram counts from a document and returns output to be saved
    Input:
        fpath: path of file
        n: order of n gram
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        tag: {'ap','nltk','stanford',None}. What POS tagger to use. Defaults 
                to None
        tag_pattern: list of of tag patterns to allow in simplified form.
                     Defaults to None. if tag_pattern = "default", 
                     use default tag pattern.
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        freq: Boolean. Whether to return frequencies. Defaults to False
        outdir: directory to write to. Defaults to directory of fpath
        zipped: a ZipFile object to read fPath from if the directory is 
                zipped. Defaults to None
    Output:
        out: path of file to write to
        counts: gram counts:
        gram2id, id2gram: gram to id and id to gram dictionaries
        """
    #default outdir to the directory of fpath if outdir is None
    if outdir is None:
        outdir = os.path.split(fpath)[0]
    #get counts (not frequencies) and id dicts
    counts,gram2id,id2gram = gram_counts_file(fpath,n,stem,stop_words,tag,
                                              tag_pattern,punctuation,
                                              freq = False, zipped = zipped)
    
    #get file name from path w/o extension
    out = os.path.split(fpath)[1]
    out = os.path.join(outdir,out)
    out = os.path.splitext(out)[0]  
    
    
    #if freq == True, normalize the counts to get frequencies
    if freq:
        freqs = counts.copy()
        total = sum(freqs.values())
        for gram in freqs.keys():
            freqs[gram] /= total
        #return everything and freqs
        return(out,counts,freqs,gram2id,id2gram)
    else:
    
        #return everything but freqs, but pad the output with a None where
        #freqs would be
        return(out,counts,None,gram2id,id2gram)
    
##Probably obsolete and won't be used
def ngrams_from_dir(indir, n, stem = None, stop_words = True, tag = None,
                     tag_pattern = None, punctuation = True, freq = False,
                     outdir = None):
    """Gets the ngram counts from every document in a directory and saves 
        output by both pickling the dicts and writing to csv. Also saves 
        a file for the ngrams across the corpus
    Input:
        indir: path to directory of txt files
        n: order of n gram
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        tag: {'ap','nltk','stanford',None}. What POS tagger to use. Defaults 
                to None
        tag_pattern: list of of tag patterns to allow in simplified form.
                     Defaults to None. if tag_pattern = "default", 
                     use default tag pattern.
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        freq: Boolean. Whether to return frequencies. Defaults to False
        outdir: directory to write to. Defaults to indir/results
    """
    
    #list the files in the directory
    files = [os.path.join(indir,f) for f in os.listdir(indir) if \
                os.path.splitext(f)[1] == ".txt"]
    #create directory for results
    if outdir is None:
        outdir = os.path.join(indir,"results")
    #check if directory exists, if not create direcotry
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    #initialize Counter object which keep track of corupus wide ngram counts
    corpus = Counter()
    #index through all text files in directory and save sngram counts
    for fpath in files:
        counts = ngram_file_output(fpath,n,stem,stop_words,tag,tag_pattern,
                                   punctuation, outdir = outdir)
        #add counts to corpus
        corpus += counts
    #if freq == True normalize the corpus counts
    #also go through the corpus counts and assign IDs by most common
    gram2id_corpus = {}
    id2gram_corpus = {}
    if freq:
        corpus_total = sum(corpus.values())
    for gramID, (gram,count) in enumerate(corpus.most_common()):
        if freq:
            corpus[gram] /= corpus_total
        gram2id_corpus[gram] = gramID
        id2gram_corpus[gramID] = gram
    #save the counts/frequencies and ids for the whole corpus
    total_file = os.path.join(outdir,"corpus_total")
    save_output(total_file,n,corpus,gram2id_corpus,id2gram_corpus)
#@profile
def ngrams_from_dir2(indir, n, stem = None, stop_words = True, tag = None,
                     tag_pattern = None, punctuation = True, freq = False,
                     threshold = 5, outdir = None):
    """Gets the ngram counts from every document in a directory and saves 
        output by both pickling the dicts and writing to csv. Also saves 
        a file for the ngrams across the corpus
    Input:
        indir: path to directory of txt files
        n: order of n gram
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        tag: {'ap','nltk','stanford',None}. What POS tagger to use. Defaults 
                to None
        tag_pattern: list of of tag patterns to allow in simplified form.
                     Defaults to None. if tag_pattern = "default", 
                     use default tag pattern.
        punctuation: Boolean. Whether to include punctuation. Defaults to True
        freq: Boolean. Whether to return frequencies. Defaults to False
        threshold: minimum number of documents a gram has to be present in.
                    Defaults to 5.
        outdir: directory to write to. Defaults to indir/results
    Output:
        doc: document frequencies
        gram2id_df: gram2id dict for the doc freqs
        final_counts: term frequency dicts for each document
    """
    #check if directory is zip archive or directory and act accordingly
    if not zipfile.is_zipfile(indir):
        #list the files in the directory
        files = [os.path.join(indir,f) for f in os.listdir(indir) if \
                    os.path.splitext(f)[1] == ".txt"]
        #create directory for results
        if outdir is None:
            outdir = os.path.join(indir,"ngram_results")
        #check if directory exists, if not create direcotry
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        #set zip_corpus to None
        zip_corpus = None
    else:
        #files is the namelist of the zip archive
        zip_corpus = zipfile.ZipFile(indir)
        files = zip_corpus.namelist()
        #crete directory for results in the directory of the zip archive
        if outdir is None:
            #get the directory of the zip archive
            tmp = os.path.split(indir)[0]
            outdir = os.path.join(tmp,"ngram_results")
        #cheack if the directory exists, if not create the directory
        if not os.path.exists(outdir):
            os.mkdir(outdir)

    #initialize defaultdict which keep track of corupus wide ngram counts
    corpus = defaultdict(int)
    #initialize Counter object which keeps track of the number of documents
    #a word is in
    doc = defaultdict(int)
    #initialize dictionary which holds all of the information for each 
    #document to be saved. Keys are file paths
    info = {}
    #index through all text files in directory and save sngram counts
    for i,fpath in enumerate(files):
        if i % 50 == 0:
            logging.info("Computing N-grams for %ith file %s" %(i,fpath))
        output = ngram_file_output2(fpath,n,stem,stop_words,tag,tag_pattern,
                                   punctuation, freq, outdir = outdir,
                                   zipped = zip_corpus)
        #add counts to corpus
        for key in output[1].keys(): corpus[key] += output[1][key]
        #add ngrams to document frequency 
        for key in output[1].keys(): doc[key] += 1
        #add information to info to be saved later
        info[output[0]] = output[1:]

    #go through the output for every file and delete the grams if their
    #document frequency is not larger than threshold
    #also keep track of the final counts
    final_counts = []
    for fPath in info.keys():
        #index through grams and place those with high doc frequency in 
        #new counter
        counts = Counter()
        gram2id = {}
        id2gram = {}
        for gram in info[fPath][0]:
            if doc[gram] >= threshold:
                #if freq == True, add frequncies
                if freq:
                    counts[gram] = info[fPath][1][gram]
                else:
                    counts[gram] = info[fPath][0][gram]
                #add to gram2id
                gram_id = info[fPath][2][gram]
                gram2id[gram] = gram_id
                #add to id2gram
                id2gram[gram_id] = info[fPath][3][gram_id]
        #add counts to final_counts
        final_counts.append(counts)
        #save the output
        save_output(fPath,n,counts,gram2id,id2gram)
    #go through the grams in corpus and put those with high doc frequency in
    #a new counter
    corpus_tmp = defaultdict(int)
    gram2id_tmp = {}
    id2gram_tmp = {}
    #if freq == True normalize the corpus counts
    if freq:
        corpus_total = sum(corpus.values())
    #also go by most_common so that the ids will match up correctly
    #for gram_id, (gram,count) in enumerate(corpus.most_common()):
    for gram_id, (gram,count) in enumerate(sorted(corpus.items(), 
                                           key = lambda x: x[1], 
                                            reverse = True)):
        if doc[gram] >= threshold:
            #if freq divide by corpus_total
            if freq:
                count /= float(corpus_total)
            #delete from corpus
            corpus_tmp[gram] =  count
            #delete from gram2id_corpus and id2gram_corpus
            gram2id_tmp[gram] =  gram_id
            id2gram_tmp[gram_id] =  gram
    #save the counts/frequencies and ids for the whole corpus
    total_file = os.path.join(outdir,"corpus_total")
    save_output(total_file,n,corpus_tmp,gram2id_tmp,id2gram_tmp)
    
    #save document frequencies
    #iterate over grams and assign ids by largest doc frequencies 
    gram2id_df = {}
    id2gram_df = {}
    for gram_id, (gram,count) in enumerate(sorted(doc.items(), 
                                           key = lambda x: x[1], 
                                            reverse = True)):
        gram2id_df[gram] = gram_id
        id2gram_df[gram_id] = gram
    doc_file = os.path.join(outdir,"doc_freqs")
    save_output(doc_file,n,doc,gram2id_df,id2gram_df)
    
    #return the document frequencies and final_counts and gram2id_df
    return(doc,gram2id_df,final_counts)


def main(indir,n,tfidf,stem,stop_words,tag,tag_pattern,punctuation,
         threshold,split_clauses,outdir):
    """Main method using GramCorpus object. Inputs are from command line"""
    #instantiate GramCorpus object
    gc = ngrams_corpus.GramCorpus(indir,n,tfidf,stem,stop_words,tag,tag_pattern,
                                  punctuation,threshold,split_clauses,outdir)
    logging.info("Converting Corpus to sparse matrix")
    #get sparse matrix
    sparse = gc.to_sparse()
    #save sparse matrix and dictionary
    outdir = gc.outdir
    with open(os.path.join(outdir,"mat.pkl"),"wb") as f:
        pickle.dump(sparse,f, protocol = 2)
    with open(os.path.join(outdir,"dictionary.pkl"),"wb") as f:
        pickle.dump(gc.dictionary,f, protocol = 2)
        
    #return sparse matrix and id2token dictionary
    return(sparse,gc.dictionary)
    
                          
if __name__ == "__main__":
    #get arguments
    parser = argparse.ArgumentParser(
                                    description=" General Purpose tool for \
                                    extracting N-gram frequencies (frequency \
                                    distributions over phrases of length N) \
                                    from documents. \
                                    Input: a directory of pre-processed text \
                                    files. \
                                    Output: directory of gram-frequency CSV \
                                    files.")
    parser.add_argument("indir", metavar = "indir", type = str,
                        help = "directory of txt files to get N Grams from")
    parser.add_argument("N", metavar = "N",type = int,
                        help = "order of N Gram")  
    parser.add_argument("--tfidf",dest = "tfidf",action="store_true",
                        help = "do tfidf transformation",default = False)                     
    parser.add_argument("--stem",dest = "stem",type = str,
                        help = "Stemmer: 'snowball','porter','lemma', defaults \
                        to None", default = None)
    parser.add_argument("--no_stop",dest = "stop",action="store_false",
                        help = "discard stop words",default = True) 
    parser.add_argument("--tag",dest = "tag",type = str,
                        help = "POS tag: 'ap','nltk','stanford', defaults to \
                        no tagging", default = None)
    parser.add_argument("--tp",dest = "tag_pattern", type = str,
                        help = "tag pattern to allow. 'default' uses \
                        default tag patterns",nargs = "+",
                        default = None)
    parser.add_argument("--no_punct",dest = "punct", action = "store_false",
                        default = True, help = "discard punctuation")
                        
    parser.add_argument("--threshold", dest = "thresh", type = int,
                        default = 5, help = "minimum number of documents an \
                        ngram must be in, defaults to 5")
                        
    parser.add_argument("--split_clauses", dest = "split_clauses", 
                        action = "store_true",
                        default = False, help = "split documents on clauses")
    parser.add_argument("--outdir",dest = "outdir",default = None, type = str,
                        help = "outdir to write results to, defaults to \
                        indir/results")
                    
    args = parser.parse_args()
    indir = args.indir
    n = args.N
    tfidf = args.tfidf
    stem = args.stem
    stop_words = args.stop
    tag = args.tag
    tp = args.tag_pattern
    punct = args.punct
    thresh = args.thresh
    split_clauses = args.split_clauses
    outdir = args.outdir
    
    #check if tag pattern tp only contains 'default'
    if tp is not None and len(tp) == 1 and tp[0] == 'default':
        tp = 'default'
    
    #run ngrams_from_dir with input
    #ngrams_from_dir2(indir,n,stem,stop_words,tag,tp,punct,freq,thresh,outdir)
    #run main method
    main(indir,n,tfidf,stem,stop_words,tag,tp,punct,thresh,split_clauses,outdir)                           