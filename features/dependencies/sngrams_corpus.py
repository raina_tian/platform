# -*- coding: utf-8 -*-
"""
More memory efficient code for computing sngrams as a corpus iterator

@author: Eli
"""

import nltk
from nltk.stem.snowball import SnowballStemmer
from nltk.stem.porter import PorterStemmer  
from nltk.stem import WordNetLemmatizer
from nltk.tag.stanford import StanfordPOSTagger as POSTagger
from textblob_aptagger import PerceptronTagger
from nltk.corpus import stopwords
from collections import Counter, defaultdict, namedtuple
import string
import csv
import os
import re
import argparse
import logging
import six
from six.moves import cPickle as pickle
import zipfile
from gensim.corpora import dictionary
from gensim import matutils
from gensim import models
import numpy as np
from sklearn import feature_extraction
from dependencies import *
from stanford_corenlp_pywrapper import CoreNLP
from sklearn import feature_extraction


import sys
sys.path.append("..")
from docLoader import *
sys.path.append("../ngrams")

#logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s\t%(message)s')

class SngramCorpus(six.Iterator):
    """Class to get dependencies, do tokenization, stemming, etc.
        Only loads one document into memory at a time"""
        
    
    def __init__(self,indir,n,tfidf= False, relations = [None],stem = None,
                 stop_words = True,rel = False,threshold = 5, split_clauses,
                 outdir = None):
        """Constructor:
        Input:
            indir: file path for input directory (only looks at .txt files)
            n: Number of words in sn-gram
            tfidf: Boolean. do tfidf
            relations: relations to exclude, defaults to [None]
            stem: {'snowball','porter',None} what kind of stemmer to use. 
                    Defaults to None.
            stop_words: Boolean. Whether to include stopwords. Defaults to True
            rel: Boolean. Whether to include relations in the sngrams. Defaults to
                False.
            threshold: minimum number of documents a gram has to be present in.
                        Defaults to 5.
            outdir: file path for output directory, defaults to indir/results
                    if outdir = None    
        """
        self.indir = indir
        #check if directory is zip archive or directory and act accordingly
        if not zipfile.is_zipfile(indir):
            #list the files in the directory
            self.files = [os.path.join(indir,f) for f in os.listdir(indir) if \
                        os.path.splitext(f)[1] == ".txt"]
            #create directory for results
            if outdir is None:
                outdir = os.path.join(indir,"ngram_results")
            #check if directory exists, if not create direcotry
            if not os.path.exists(outdir):
                os.mkdir(outdir)
            #set zip_corpus to None
            self.zip_corpus = None
        else:
            #files is the namelist of the zip archive
            self.zip_corpus = zipfile.ZipFile(indir)
            self.files = self.zip_corpus.namelist()
            #crete directory for results in the directory of the zip archive
            if outdir is None:
                #get the directory of the zip archive
                tmp = os.path.split(indir)[0]
                outdir = os.path.join(tmp,"ngram_results")
            #cheack if the directory exists, if not create the directory
            if not os.path.exists(outdir):
                os.mkdir(outdir)   
                
        self.stop = set(stopwords.words('english'))
        
        self.n = n
        self.relations = relations
        self.stem = stem
        self.stop_words = stop_words
        self.rel = rel
        self.threshold = threshold
        self.outdir = outdir
        #if stem is not None, stem the words in the dependencies
        if stem is not None:
            if stem == 'porter':
                self.stemmer = PorterStemmer()
            elif stem == 'snowball':
                self.stemmer = SnowballStemmer("english")
            elif stem == 'lemma':
                self.stemmer = WordNetLemmatizer()
                #add stem as another name for lemmatize
                self.stemmer.stem = stemmer.lemmatize
            else:
                #raise a value error if a wrong stemmer is chosen
                raise ValueError('Not an available stemmer')
        self.index = 0
        self.parser = CoreNLP(configdict={
                'annotators': "tokenize,ssplit,pos,lemma,parse"}, 
                corenlp_jars=["../../stanford/corenlp/*",
                              "../../platform/stanford/srparser/stanford-srparser-2014-10-23-models.jar"])
        
        #initialize dictionary
        self.dictionary = dictionary.Dictionary()
        #set splitting on clauses
        self.split_clauses = split_clauses
        #current clauses
        self.curr_clauses = []
        
        
    def __len__(self):
        """len function, number of documents"""
        return(len(self.files))
        
    def __next__(self):
        """Next function for iterator"""
        if self.index >= len(self.files):
            raise StopIteration
        #get the dependencies from file
        #if not splitting clauses
        if not self.split_clauses:
            deps = deps_from_file(self.files[self.index],self.parser,zipped = self.zip_corpus)
            #get sngrams from deps
            sngrams = self.sngrams_from_sents(deps,n,self.relations,self.stem,
                                              self.stop_words,self.rel)
                                              
            self.index += 1
            return(self.dictionary.doc2bow(sngrams,allow_update = True))
        #if splitting on clauses use the clauses
        else:
            if len(self.curr_clauses) == 0:
                #get the sentences for the current clauses
                curr_clauses = deps_from_file(self.files[self.index],
                                              self.parser,
                                              zipped = self.zip_corpus,
                                              corpus = True)
                self.index += 1
            #pop one clauses from self.curr_clauses
            deps = curr_clauses.pop()
            sngrams = self.sngrams_from_sents(deps,n,self.relations,self.stem,
                                              self.stop_words,self.rel)
            return(self.dictionary.doc2bow(sngrams,allow_update = True))
            
        
    def __iter__(self):
        """Iterator, get dependencies and sngrams, does stemming etc."""
        if not self.split_clauses:
            for i, fName in enumerate(self.files):
                if i % 100 == 0:
                    logging.info("Computing SN-grams for %ith file %s" %(i,fName))            
                #get dependencies from file
                deps = deps_from_file(fName,self.parser,
                                      zipped = zipcorpus)
                #get sngrams from deps
                sngrams = self.sngrams_from_sents(deps,n,self.relations,self.stem,
                                                  self.stop_words,self.rel)
                yield(self.dictionary.doc2bow(sngrams,allow_update = True))
        else:
            for i, fName in enumerate(self.files):
                if i % 100 == 0:
                    logging.info("Computing SN-grams for %ith file %s" %(i,fName))            
                #get dependencies for each clause
                clause_deps = deps_from_file(fName,self.parser,
                                      zipped = zipcorpus)
                #iterate over the deps
                for deps in clause_deps:
                #get sngrams from deps
                    sngrams = self.sngrams_from_sents(deps,n,self.relations,self.stem,
                                                      self.stop_words,self.rel)
                    yield(self.dictionary.doc2bow(sngrams,allow_update = True))            
            
    def sngrams(depends, N, relations = [None], stem = None, stop_words = True, 
                                    rel = False):
        """Extracts all sngrams from a sentence by going through the 
            dependencies
        Input:
            depends: A list of the dependencies from the sentence
            N: Number of words in sn-gram
            relations: relations to exclude, defaults to [None]
            stem: {'snowball','porter','lemma',None} what kind of stemmer to 
                    use. Defaults to None.
            stop_words: Boolean. Whether to include stopwords. Defaults to 
                            True
            rel: Boolean. Whether to include relations in the sngrams. 
                Defaults to False.
        Output:
            sngrams: A list of the sngrams, each a tuple of words
        """
        if stem is not None:
            #stem the words in the dependencies and drop dependencies without
            #important relations
            depends = [stem_dep(dep,self.stemmer) for dep in depends \
                        if dep.relation not in relations]
        else:
            #get rid of all dependencies that do not have important relations
            depends = [dep for dep in depends if dep.relation not in relations]    
        #if stop = False, get rid of dependencies with stop words
        if not stop_words:
            depends = [dep for dep in depends if dep.head not in stop and \
                                                dep.dependent not in stop]
        #initially treat bigrams as dependencies, and n-grams as n-1 dependencies
        #initialize the sngrams to include all dependencies as their own list
        sngrams = [[depend] for depend in depends]
    
        #initialize stack
        stack = list(depends)
        #while stack is not empty
        while not stack == []:
            #get an n-1 gram
            gram = stack.pop()
    
            #turn gram into a list if it isn't
            if not type(gram) == list:
    
                gram = list([gram])
            else:
                gram = list(gram)
            #if the sn-1 gram is of length N, don't add anything more
            if len(gram) + 1 == N:
                continue
            
            
            #isolate the final dependency position (end of the n-gram)
            end = gram[-1].depPos
            #iterate over bigrams/dependencies and create a new n-gram by adding
            #on the bigrams which start with the end of gram
            for bigram in depends:
                if end == bigram.headPos:
                    #if the start of the bigram is the end of the n-1 gram
                    #create an n-gram by adding on that dependency
                    newGram = gram + [bigram]
                    #print(newGram,bigram)
                    #add the new n-gram to the stack
                    stack.append(newGram)
                    #add the new n-gram to the sngrams list
                    sngrams.append(newGram)
                    
        #iterate through the ngrams and get the string representations of 
        #the sngrams
        sngramsWords = []
        for i in range(len(sngrams)):
            
            #get the dependency links which form the sn-gram
            deps = sngrams[i]
            #only keep the sngrams of length n
            if len(deps) != n:
                continue
            #go through each dependency and add the words together in a list to 
            #form the sn-gram
            
            sngram = (deps[0].head,)
            for dep in deps:
                #if rel = True, then include the relation before each dependent
                if rel:
                    sngram += (dep.relation,dep.dependent)
                else:
                    sngram += (dep.dependent,)
            #add the sngram to snGramsWords
            #turn sngram into a string
            sngram = "_".join(sngram)
            sngramsWords.append(sngram)
                
        return(sngramsWords)
        
    def sngrams_from_sents(sentDeps, N, relations = [None], stem = None, \
                            stop_words = True, rel = False):
        """Gets the sngrams from multiple sentences 
        Input:
            sentDeps: A list of lists of dependencies from the sentences
            N: Number of words in sn-gram
            relations: relations to exclude, defaults to [None]
            stem: {'snowball','porter',None} what kind of stemmer to use. 
                    Defaults to None.
            stop_words: Boolean. Whether to include stopwords. Defaults to 
                        True
            rel: Boolean. Whether to include relations in the sngrams. 
                Defaults to False.
        Output:
            sngrams: A list of the sngrams, each a tuple of words
        """
        sentSngrams = []
        #iterate through sentDeps, get the sngrams and add to the list
        for deps in sentDeps:
            sentSngrams.extend(sngrams(deps,N,relations,stem,stop_words,rel))
        
        return(sentSngrams) 
        
    def to_sparse(self):
        """Turns the corpus into a sparse matrix"""
        #get sparse matrix
        sparse = matutils.corpus2csc(self,num_docs = len(self.files)).T
        #do thresholding
        sparse = self.threshold_matrix(sparse)
        #do tfidf if True
        if self.tfidf_bool:
            sparse = self.tfidf(sparse)
        #return sparse matrix
        return(sparse)
    
    def threshold_matrix(self,sparse):
        """Does thresholding on a sparse matrix
        Input:
            sparse: sparse matrix to do threhsolding on
        Output:
            sparse: sparse matrix after thresholding
        """
        print("thresholding")
        #convert the sparse matrix to a csc matrix b/c its faster
        sparse = sparse.tocsc()
        #create a boolean mask
        mask = np.zeros(sparse.shape[1],dtype = bool)
        #change the dictionary token2id to account for droped terms
        id2token = {}
        token2id = {}
        count = 0
        #get all columns with greater than threshold doc freq
        for i in range(sparse.shape[1]):
            if i % 100 == 0:
                print(i)
            if sparse[:,i].nnz >= self.threshold:
                mask[i] = True
                token = self.dictionary[i]
                #use count as the new id
                id2token[count] = token
                token2id[token] = count
                #increase count
                count += 1                
        #only keep the columns with greater than threshold doc freq
        mask_flat = np.flatnonzero(mask)        
        sparse = sparse[:,mask_flat]   
        #put these new dicts in the Dictionary
        self.dictionary.id2token = id2token
        self.dictionary.token2id = token2id
        #convert back to csr
        sparse = sparse.tocsr()
        return(sparse)
        
    def tfidf(self,sparse):
        """Returns the tfidf transformation of a sparse matrix
        Input:
            sparse: sparse amtrix to be transformed
        Output:
            sparse: sparse matrix after tfidf transformation
        """
        transform = feature_extraction.text.TfidfTransformer()
        sparse = transform.fit_transform(sparse)
        return(sparse)