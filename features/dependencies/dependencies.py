# -*- coding: utf-8 -*-
"""
Uses the Stanford Dependency Parser on documents

@author: Eli
"""

from nltk.parse.stanford import StanfordParser
import copy

#temporary import to use docLoader.py
#TODO: turn these files into a package heirachy so we can import across 
#directories easily
import sys
sys.path.append("..")
from docLoader import *
from clauseParser import clauseParser

class DepParser(StanfordParser):
    """Class to run the stanford dependency parser through nltk"""    
    def raw_parse_sents(self, sentences, verbose=False):
        """
        Use StanfordParser to parse multiple sentences. Takes multiple sentences as a
        list of strings.
        Each sentence will be automatically tokenized and tagged by the Stanford Parser.

        :param sentences: Input sentences to parse
        :type sentences: list(str)
        :rtype: list(Tree)
        """
        cmd = [
            'edu.stanford.nlp.parser.lexparser.LexicalizedParser',
            '-model', self.model_path,
            '-sentences', 'newline',
            '-outputFormat', 'typedDependencies',
        ]
        return self._execute(cmd, '\n'.join(sentences), verbose)

#Used for NLTK connection with stanford parser, now obsolete
class Dependency_NTLK():
    """Class to store a dependency relation"""
    
    def __init__(self,depString,lower = True):
        """Constructs a dependency relation from a string which is the output
            of the stanford dependency parser
        Input:
            depString:  string representation of dependency
            lower: whether to send all words to lowercase, defaults to True
        """
        
        #split the dependency string to get the elements
        split = depString.split("(")
        
        #dependency relation
        self.relation = split[0]

        #split after the "(" to get the words)
        words = split[1].split(",")
        
        #get the head word and position 
        #re add the hyphen in a word if it exits
        head = "-".join(words[0].split("-")[:-1])
        headPos = int("".join(d for d in words[0].split("-")[-1] if d.isdigit()))
        
        #get dependent word and position
        dependent = "-".join(words[1].split("-")[:-1])
        depPos =  int("".join(d for d in words[1].split("-")[-1] if d.isdigit()))
        
        #strip whitespace and send to lower case if lower = True
        if lower:
            #if the head is ROOT, don't send to lowercase
            if head.strip() == "ROOT":
                self.head = head.strip()
            else:
                self.head = head.strip().lower()
            self.dependent = dependent.strip().lower()
        else:
            self.head = head.strip()
            self.dependent = dependent.strip()
        #keep positions of head word and dependent word   
        self.headPos = headPos
        self.depPos = depPos
        
        
    def __repr__(self):
        """String representation of the dependecy"""
        rep = "%s(%s-%i, %s-%i)" % (self.relation,self.head,self.headPos,
                                    self.dependent, self.depPos)
        return(rep)
        
        
class Dependency():
    """Stores a dependency relation using stanford_corenlp_pywrapper conection"""
    def __init__(self,rel,head,dependent,lower = True):
        """Constructs a dependency relationship
        Input:
            rel: a list of the form ['relation',head_position,dependent_pos]
            head: the head word
            dependent: the dependent word
            lower: whether to send all words to lowercase, defaults to True
        """
        #lowercase if lower is True
        if lower:
            #don't lowercase head if head is 'ROOT'
            if not head == "ROOT":
                head = head.lower()
            dependent = dependent.lower()
        self.head = head
        self.dependent = dependent
        self.relation = rel[0]
        self.headPos = rel[1]
        self.depPos = rel[2]          
    def __repr__(self):
        """String representation of the dependecy"""
        rep = "%s(%s-%i, %s-%i)" % (self.relation,self.head,self.headPos,
                                    self.dependent, self.depPos)
        return(rep)

def deps_from_doc(parsed):
    """Gets the dependencies from the parsed output after parsing a document
    Input:
        parsed: a dict with the information from parsing
    Output:
        deps: A list of lists of dependencies from the parser, each list 
            corresponding to a sentence 
    """
    deps = []
    #iterate over sentences
    for parse in parsed['sentences']:
        rels = parse['deps_basic']
        lemmas = parse['lemmas']
        sent_deps = []
        #iterate over dependencies
        for rel in rels:
            #account for the case when the head is the root and the position
            # is -1
            if rel[1] == -1:
                head = 'ROOT'
            else:
                head = lemmas[rel[1]]
            #get dependent
            dependent = lemmas[rel[2]]
            #add dependency to sent_deps
            sent_deps.append(Dependency(rel,head,dependent))
        #add sent_deps to deps when done with sentence
        deps.append(sent_deps)
    return(deps)
            
def deps_from_file(fPath,parser,zipped = None,clauses = False):
    """Runs the stanford dependency parser on the file fName
    
    Input:
        fPath: path to document
        parser: a CoreNLP object which is already loaded
        zipped: a ZipFile object to read fPath from if the directory is 
                zipped. Defaults to None
    Output:
        deps: A list of lists of dependencies from the parser, each list 
            corresponding to a sentence            
    """
    #split into clauses if needed
    if not clauses:
        #deal with zipped file
        if zipped is None:
            #read file
            with open(fPath,'r') as f:
                text = f.read()
        else:
            #read file from zipped archive
            with zipped.open(fPath) as f:
                text = f.read()
        #parse text
        parsed = parser.parse_doc(text)
        #get dependencies
        deps = deps_from_doc(parsed)
        return(deps)
    else:
        #deal with zipped file
        if zipped is None:
            #read file
            with open(fPath,'r') as f:
                text = f.readlines()
        else:
            #read file from zipped archive
            with zipped.open(fPath) as f:
                text = f.readlines()
        #get clauses
        clauseparser = clauseParser()
        clauses = clauseparser.findClauses(text,fPath)
        #parse text
        parsed = [parser.parse_doc(clause) for clause in clauses.values()]
        #get dependencies
        deps = [deps_from_doc(parse) for parse in parsed]
        return(deps)        
    
        
#obsolete        
def deps_from_sent_NLTK(output):
    """Top level function to get a list of dependencies from stanford parser
        output as a string.
    Input:
        output: string representation of stanford dependency parser output
    Output:
        deps: A list of Dependencies from the parser
    """
    #split on new line characters, ignore last two elements
    depStrings = output.split("\n")[:-2]
    
    deps = []
    #iterate over string representations and get the dependencies
    for depString in depStrings:
        deps.append(Dependency(depString))
        
    #return the list of dependencies
    return(deps)
#obsolete    
def deps_from_doc_NLTK(output):
    """Top level function to get a list of lists of dependencies corresponding
        to the sentences in a document from the stanford parser output
    Input:
        output: string representation of stanford dependency parser output
    Output:
        deps: A list of lists of dependencies from the parser, each list 
            corresponding to a sentence
    """
    
    #split on "\n\r\n" to split isolate dependencies for each sentence
    #ignore last element because it is white space
    sentDepsStrings = output.split("\n\r\n")[:-1]
    
    sentDeps = []
    #iterate through and get the dependencies
    for sentDepsString in sentDepsStrings:
        sentDeps.append(deps_from_sent(sentDepsString))
        
    #return the list of lists of depndencies
    return(sentDeps)
    
#obsolete
def deps_from_file_NLTK(fPath,jarFile,modelJarFile):
    """Runs the stanford dependency parser on the file fName
    
    Input:
        fPath: file to dependency parse
        jarFile: location of stanford-corenlp-3.5.2.jar
        modelJarFile: location of stanford-corenlp-3.5.2-models.jar
    Output:
        deps: A list of lists of dependencies from the parser, each list 
            corresponding to a sentence
    """
    #get the sentences of the document with docLoader
    sents = doc_sents(fPath)
    #get the dependencies
    depParse = DepParser(jarFile,modelJarFile)
    parse = depParse.raw_parse_sents(sents)
    deps = deps_from_doc(parse)
    
    return(deps)

#obsolete
def stem_dep(dep,stemmer):
    """Stems the words in a dependency
    
    Input:
        dep: dependency to stem
        stemmer: stemmer object with method .stem
        
    Output:
        stemmedDep: dependency with stemmed words
    """
    stemmedDep = copy.copy(dep)
    #stem head word if it isn't ROOT
    if dep.head != "ROOT":
        stemmedDep.head = stemmer.stem(dep.head)
    #stem dependent
    stemmedDep.dependent = stemmer.stem(dep.dependent)
    #return
    return(stemmedDep)