# -*- coding: utf-8 -*-
"""
Created on Wed May 20 14:17:50 2015

@author: elliott
"""

from nltk.parse.stanford import StanfordParser

class DepParser(StanfordParser):
    
    def raw_parse_sents(self, sentences, verbose=False):
        """
        Use StanfordParser to parse multiple sentences. Takes multiple sentences as a
        list of strings.
        Each sentence will be automatically tokenized and tagged by the Stanford Parser.

        :param sentences: Input sentences to parse
        :type sentences: list(str)
        :rtype: list(Tree)
        """
        cmd = [
            'edu.stanford.nlp.parser.lexparser.LexicalizedParser',
            '-model', self.model_path,
            '-sentences', 'newline',
            '-outputFormat', 'typedDependencies',
        ]
        return self._execute(cmd, '\n'.join(sentences).encode("utf-8"), verbose)
        

D = DepParser('stanford-corenlp-3.5.2.jar', 'stanford-corenlp-3.5.2-models.jar')
deps = D.raw_parse_sents(('this is the english parser test', 'the parser is from stanford parser')).splitlines()
        