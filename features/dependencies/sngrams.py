# -*- coding: utf-8 -*-
"""
Creates syntactic n-grams (sn-grams) from Stanford Dependency Parser

@author: Eli
"""
from dependencies import *
from nltk.stem.snowball import SnowballStemmer
from nltk.stem.porter import PorterStemmer  
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from collections import Counter, defaultdict
import csv
import os
import argparse
import logging
from stanford_corenlp_pywrapper import CoreNLP
import six
from six.moves import cPickle as pickle
import zipfile
import sngrams_corpus

#temporary import to use docLoader.py
#TODO: turn these files into a package heirachy so we can import across 
#directories easily
import sys
sys.path.append("..")
from docLoader import *

#top level variable which sontains english stop words as a set
stop = set(stopwords.words('english'))


#logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s\t%(message)s')

def sngrams(depends, N, relations = [None], stem = None, stop_words = True, \
                                rel = False):
    """Extracts all sngrams from a sentence by going through the dependencies
    Input:
        depends: A list of the dependencies from the sentence
        N: Number of words in sn-gram
        relations: relations to exclude, defaults to [None]
        stem: {'snowball','porter','lemma',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        rel: Boolean. Whether to include relations in the sngrams. Defaults to
            False.
    Output:
        sngrams: A list of the sngrams, each a tuple of words
    """
    #if stem is not None, stem the words in the dependencies
    if stem is not None:
        if stem == 'porter':
            stemmer = PorterStemmer()
        elif stem == 'snowball':
            stemmer = SnowballStemmer("english")
        elif stem == 'lemma':
            stemmer = WordNetLemmatizer()
            #add stem as another name for lemmatize
            stemmer.stem = stemmer.lemmatize
        else:
            #raise a value error if a wrong stemmer is chosen
            raise ValueError('Not an available stemmer')
        #stem the words in the dependencies and drop dependencies without
        #important relations
        depends = [stem_dep(dep,stemmer) for dep in depends \
                    if dep.relation not in relations]
    else:
        #get rid of all dependencies that do not have important relations
        depends = [dep for dep in depends if dep.relation not in relations]    
    #if stop = False, get rid of dependencies with stop words
    if not stop_words:
        depends = [dep for dep in depends if dep.head not in stop and \
                                            dep.dependent not in stop]
    #initially treat bigrams as dependencies, and n-grams as n-1 dependencies
    #initialize the sngrams to include all dependencies as their own list
    sngrams = [[depend] for depend in depends]

    #initialize stack
    stack = list(depends)
    #while stack is not empty
    while not stack == []:
        #get an n-1 gram
        gram = stack.pop()

        #turn gram into a list if it isn't
        if not type(gram) == list:

            gram = list([gram])
        else:
            gram = list(gram)
        #if the sn-1 gram is of length N, don't add anything more
        if len(gram) + 1 == N:
            continue
        
        
        #isolate the final dependency position (end of the n-gram)
        end = gram[-1].depPos
        #iterate over bigrams/dependencies and create a new n-gram by adding
        #on the bigrams which start with the end of gram
        for bigram in depends:
            if end == bigram.headPos:
                #if the start of the bigram is the end of the n-1 gram
                #create an n-gram by adding on that dependency
                newGram = gram + [bigram]
                #print(newGram,bigram)
                #add the new n-gram to the stack
                stack.append(newGram)
                #add the new n-gram to the sngrams list
                sngrams.append(newGram)
                
    #iterate through the ngrams and get the string representations of the words
    #and make sure they are tuples for future hashing
    sngramsWords = []
    for i in range(len(sngrams)):
        #get the dependency links which form the sn-gram
        deps = sngrams[i]
        #go through each dependency and add the words together in a list to 
        #form the sn-gram
        
        sngram = (deps[0].head,)
        for dep in deps:
            #if rel = True, then include the relation before each dependent
            if rel:
                sngram += (dep.relation,dep.dependent)
            else:
                sngram += (dep.dependent,)
        #add the sngram to snGramsWords
        sngramsWords.append(sngram)
            
    return(sngramsWords)
        
                
        
def sngrams_from_sents(sentDeps, N, relations = [None], stem = None, \
                        stop_words = True, rel = False):
    """Gets the sngrams from multiple sentences 
    Input:
        sentDeps: A list of lists of dependencies from the sentences
        N: Number of words in sn-gram
        relations: relations to exclude, defaults to [None]
        stem: {'snowball','porter',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        rel: Boolean. Whether to include relations in the sngrams. Defaults to
            False.
    Output:
        sngrams: A list of the sngrams, each a tuple of words
    """
    sentSngrams = []
    #iterate through sentDeps, get the sngrams and add to the list
    for deps in sentDeps:
        sentSngrams.extend(sngrams(deps,N,relations,stem,stop_words,rel))
    
    return(sentSngrams)
    

def count_grams(sngrams):
    """Counts the sngrams 
    
    Input:
        sngrams: list of sngrams
    Output:
        gramcounter: Counter object which counts the sngrams 
    """
    
    return(Counter(sngrams))

def sngrams_from_file(fPath, N, parser, relations = [None], stem = None,\
                        stop_words = True, rel = False, zipped = None):
    """Gets the sngrams from a document
    Input:
        fPath: Filepath for document
        N: Number of words in sn-gram
        parser: a CoreNLP object that is already loaded into memory
        relations: relations to exclude, defaults to [None]
        stem: {'snowball','porter',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        rel: Boolean. Whether to include relations in the sngrams. Defaults to
            False.
        zipped: a ZipFile object to read fPath from if the directory is 
                zipped. Defaults to None
    Output:
        sngrams: A list of the sngrams, each a tuple of words
    """
    logging.info("Getting dependencies for %s" %fPath)
    deps = deps_from_file(fPath,parser,zipped = zipped)

    #get the sngrams
    sngrams = sngrams_from_sents(deps,N,relations,stem,stop_words,rel)
    
    return(sngrams)
    
        
def gram_counts(grams,freq = False):
    """Counts the gram frequencies, and assings IDs to each gram
    
    Input:
        grams: A list of sngrams to be counted
        freq: Boolean. Whether to return frequencies. Defaults to False
    Output:
        gramCounter: Counter object of gram counts
        gram2id: dictionary with grams as keys and IDs as values
        id2gram: dictionary with IDs as keys and grams as values
    """
    #count the grams
    gramCounter = count_grams(grams)
    #initialize id dicts
    gram2id = {}
    id2gram = {}
    #normalize counts to get term frequencies and also create id dicts
    if freq:
        total = sum(gramCounter.values())
    for gramID, (gram,count) in enumerate(gramCounter.most_common()):
        #divide counts by total counts
        if freq:
            gramCounter[gram] /= total
        gram2id[gram] = gramID
        id2gram[gramID] = gram
        
    return(gramCounter,gram2id,id2gram)
        

def save_output(outFile,gramNum,counts,gram2id,id2gram):
    """Saves the frequency Counter object and id dictionaries.
     
         Saves 1 pkl file of output as a tuple and 2 csv files
         
     
     Input:
         outFile: file name to save to w/o file extension
         gramNum: type of n-gram (e.g. 2 = bigram, 3 = trigram, etc.)
         counts: frequency Counter object
         gram2id, id2gram: id dictionaries
    """

    #pickle the files after combining them into a tuple
    output = (counts,gram2id,id2gram)
    fName = outFile + "_s_" + str(gramNum) + '.pkl'
    with open(fName,'wb+') as f:
        pickle.dump(output,f,protocol = 2)
        
    #write id csv files
    idName = outFile + "_s_"+ str(gramNum) + "_gram" + "_id.csv"
    #check type of python and act accordingly
    if six.PY2:
        with open(idName,'wb+') as csvfile:
            writer = csv.writer(csvfile)
            #iterate through key-value pairs in id2gram and write to csv
            for gramid in sorted(id2gram.keys()):
                #if there are tags, drop them when writing to file
                gram = [g.split("::")[0] for g in id2gram[gramid]]
                #have n-grams output as word-word rather than ("word","word")
                gram = "-".join(gram)
                """ #delete Unicode BOM
                if gram.startswith(u'\ufeff'):
                    gram = gram[1:]"""
                gram = gram.encode("utf-8")
                
                writer.writerow([gramid,gram])
        #write frequency csv file
        
        freqName = outFile + "_s_"+ str(gramNum) + "_gram" + "_freq.csv"
        
        with open(freqName, 'wb+') as freqfile:
            writer = csv.writer(freqfile)
            #iterate through gramids and write gramid,frequency pairs to csv
            for gramid in sorted(id2gram.keys()):
                writer.writerow([gramid,counts[id2gram[gramid]]])                
    elif six.PY3:
        with open(idName,'w+', newline = '',encoding = 'utf-8') as csvfile:
            writer = csv.writer(csvfile)
            #iterate through key-value pairs in id2gram and write to csv
            for gramid in sorted(id2gram.keys()):
                #if there are tags, drop them when writing to file
                g = [gram.split("::")[0] for gram in id2gram[gramid]]
                #have n-grams output as word-word rather than ("word","word")
                writer.writerow([gramid,"-".join(g)])        
        #write frequency csv file
        
        freqName = outFile + "_"+ str(gramNum) + "_gram" + "_freq.csv"
        
        with open(freqName, 'w+',newline = '') as freqfile:
            writer = csv.writer(freqfile)
            #iterate through gramids and write gramid,frequency pairs to csv
            for gramid in sorted(id2gram.keys()):
                writer.writerow([gramid,counts[id2gram[gramid]]])        
            

def gram_counts_file(fPath, N, parser, relations = [None], stem = None,\
                            stop_words = True, rel = False, freq = False,
                            zipped = None):
    """Gets the sngram counts from a document
    Input:
        fPath: Filepath for document
        N: Number of words in sn-gram
        parser: a CoreNLP object that is already loaded into memory
        relations: relations to exclude, defaults to [None]
        stem: {'snowball','porter',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        rel: Boolean. Whether to include relations in the sngrams. Defaults to
            False.
        zipped: a ZipFile object to read fPath from if the directory is 
                zipped. Defaults to None
    Output:
        gramCounter: Counter object of gram counts
        gram2id: dict of dictionaries with grams as keys and IDs as values,
                the outer dict's keys are the length of the ngrams  
        id2gram: dict of dictionaries with IDs as keys and grams as values,
                the outer dict's keys are the length of the ngrams
    """      

    #get sngrams
    sngrams = sngrams_from_file(fPath, N, parser, relations, stem,
                                stop_words, rel,zipped = zipped)
    #divide sngrams into bigrams,trigrams,... up to N-grams
    grams = defaultdict(list)
    for sngram in sngrams:
        #add sngram to the list that has its length
        # if rel is true then use int(len(sngram)/2.0 + .5)
        if rel:
            grams[int(len(sngram)/2.0 + .5)].append(sngram)
        else:
            grams[len(sngram)].append(sngram)
    #iterate through N-grams and get the counts and id dicts
    counts = {}
    gram2id = {}
    id2gram = {}
    for n in grams.keys():
        #unpack the output from the counter and append to the lists
        c,g,i = gram_counts(grams[n],freq)
        counts[n] = c
        gram2id[n] = g
        id2gram[n] = i
    
    
    return(counts,gram2id,id2gram)
    
def sngram_file_output(fPath, N, relations = [None], stem = None,\
                        stop_words = True, rel = False, freq = False, 
                        outdir = None):
    """Gets the sngram counts from a document and saves the files for each
        n <= N
        Saves 1 pkl file of output as a tuple and 2 csv files
    Input:
        fPath: Filepath for document

        N: Number of words in sn-gram
        relations: relations to exclude, defaults to [None]
        stem: {'snowball','porter',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        rel: Boolean. Whether to include relations in the sngrams. Defaults to
            False.
        freq: Boolean. Whether to return frequencies. Defaults to False
        outdir: directory to write to. Defaults to directory of fPath
    """   
    #default outdir to directory of fPath
    if outdir is None:
        outdir = os.path.split(fPath)[0]
    #get the counts (not frequencies) and id dictionaries
    counts,gram2id,id2gram = gram_counts_file(fPath, N, parser, relations, \
                                                stem,stop_words, rel,freq = False)
    #get the file name from the path without extension
    out = os.path.split(fPath)[1]
    out = os.path.join(outdir,out)
    out = os.path.splitext(out)[0]
    
    #if freq == True, normalize the counts to get the frequencies
    if freq:
        freqs = counts.copy()
        for n in freqs.keys():
            total = sum(freqs[n].values())
            for gram in freqs[n].keys():
                freqs[n][gram] /= total
        #iterate through ngrams and save the files with frequencies
        for n in freqs.keys():
            save_output(out,n,freqs[n],gram2id[n],id2gram[n])
    else:
        #iterate through bigrams,trigrams, etc and save the files with counts
        for n in counts.keys():
            save_output(out,n,counts[n],gram2id[n],id2gram[n])
    #return counts
    return(counts)


def sngram_file_output2(fPath, N, parser, relations = [None], stem = None,\
                        stop_words = True, rel = False, freq = False, 
                        outdir = None, zipped = None):
    """Gets the sngram counts from a document and saves the files for each
        n <= N
        Saves 1 pkl file of output as a tuple and 2 csv files
    Input:
        fPath: Filepath for document

        N: Number of words in sn-gram
        parser: a CoreNLP object that is already loaded into memory
        relations: relations to exclude, defaults to [None]
        stem: {'snowball','porter',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        rel: Boolean. Whether to include relations in the sngrams. Defaults to
            False.
        freq: Boolean. Whether to return frequencies. Defaults to False
        outdir: directory to write to. Defaults to directory of fPath
        zipped: a ZipFile object to read fPath from if the directory is 
                zipped. Defaults to None
    Output:
        out: path of file to write to
        counts: gram counts:
        gram2id, id2gram: gram to id and id to gram dictionaries
        
    """   
    #default outdir to the directory of fpath if outdir is None
    if outdir is None:
        outdir = os.path.split(fpath)[0]
    #get counts (not frequencies) and id dicts
    counts,gram2id,id2gram = gram_counts_file(fPath, N, parser,relations, 
                                    stem,stop_words, rel,freq = False,
                                    zipped = zipped)    
    #get file name from path w/o extension
    out = os.path.split(fPath)[1]
    out = os.path.join(outdir,out)
    out = os.path.splitext(out)[0]  
    
    
    #if freq == True, normalize the counts to get frequencies
    if freq:
        freqs = counts.copy()
        for n in freqs.keys():
            total = sum(freqs[n].values())
            for gram in freqs[n].keys():
                freqs[n][gram] /= total
        #return everything and freqs
        return(out,counts,freqs,gram2id,id2gram)
    else:
    
        #return everything but freqs, but pad the output with a None where
        #freqs would be
        return(out,counts,None,gram2id,id2gram)



def sngrams_from_dir2(indir, N, relations = [None], stem = None,\
                    stop_words = True, rel = False, freq = False, 
                    threshold = 5, outdir = None):
    """Gets the sngram counts from every document in a directory and saves the 
        files for each n <= N
        Saves 1 pkl file of output as a tuple and 2 csv files
    Input:
        indir: file path for input directory (only looks at .txt files)
        N: Number of words in sn-gram
        relations: relations to exclude, defaults to [None]
        stem: {'snowball','porter',None} what kind of stemmer to use. 
                Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        rel: Boolean. Whether to include relations in the sngrams. Defaults to
            False.
        freq: Boolean. Whether to return frequencies. Defaults to False
        outdir: file path for output directory, defaults to indir/results
                if outdir = None 
    Output:
        doc: dict with gram length as key and Counter of gram document freqs
                as values
        gram2id_df: dict with gram length as key and gram2id dictionary as 
                value
        total_counts: dict with gram length as key and Counter of term 
                    frequencies for each document as the values
    """   
    
    #check if directory is zip archive or directory and act accordingly
    if not zipfile.is_zipfile(indir):
        #list the files in the directory
        files = [os.path.join(indir,f) for f in os.listdir(indir) if \
                    os.path.splitext(f)[1] == ".txt"]
        #create directory for results
        if outdir is None:
            outdir = os.path.join(indir,"sngram_results")
        #check if directory exists, if not create direcotry
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        #set zip_corpus to None
        zip_corpus = None
    else:
        #files is the namelist of the zip archive
        zip_corpus = zipfile.ZipFile(indir)
        files = zip_corpus.namelist()
        #crete directory for results in the directory of the zip archive
        if outdir is None:
            #get the directory of the zip archive
            tmp = os.path.split(indir)[0]
            outdir = os.path.join(tmp,"sngram_results")
        #cheack if the directory exists, if not create the directory
        if not os.path.exists(outdir):
            os.mkdir(outdir)
    #initalize the CoreNLP object which does the parsing
    parser = CoreNLP(configdict={
                'annotators': "tokenize,ssplit,pos,lemma,parse"}, 
                corenlp_jars=["../../stanford/corenlp/*",
                              "../../platform/stanford/srparser/stanford-srparser-2014-10-23-models.jar"])
    #initialize Counter object which keep track of corupus wide ngram counts
    corpus = {n:Counter() for n in range(2,N+1)}
    #initialize Counter object which keeps track of the number of documents
    #a word is in
    doc = {n:Counter() for n in range(2,N+1)}
    #initialize dictionary which holds all of the information for each 
    #document to be saved. Keys are file paths
    info = {}
    #index through all text files in directory and save sngram counts
    for fpath in files:
        logging.info("Computing SN-grams for file %s" %fpath)
        output = sngram_file_output2(fpath, N, parser, relations, stem, 
                                     stop_words, rel, freq,outdir,
                                     zipped = zip_corpus)
        for n in range(2,N+1):
            #add counts to corpus
            corpus[n] += output[1][n]
            #add ngrams to document frequency counter
            doc[n] += Counter(output[1][n].keys())
            #add information to info to be saved later
            info[output[0]] = output[1:]
    #if freq == True normalize the corpus counts
    #also go through the corpus counts and assign IDs by most common
    gram2id_corpus = {n:{} for n in range(2,N+1)}
    id2gram_corpus = {n:{} for n in range(2,N+1)}
    #keep track of total counts
    total_counts = {n:[] for n in range(2,N+1)}
    for n in range(2,N+1):
        if freq:
            corpus_total = sum(corpus[n].values())
        for gramID, (gram,count) in enumerate(corpus[n].most_common()):
            if freq:
                corpus[n][gram] /= corpus_total
            gram2id_corpus[n][gram] = gramID
            id2gram_corpus[n][gramID] = gram
        #go through the output for every file and delete the grams if their
        #document frequency is not larger than threshold
        for fPath in info.keys():
            #index through grams and place those with high doc frequency in 
            #new counter
            counts = Counter()
            gram2id = {}
            id2gram = {}
            for gram in info[fPath][0][n]:
                if doc[n][gram] >= threshold:
                    #if freq == True, add frequncies
                    if freq:
                        counts[gram] = info[fPath][1][n][gram]
                    else:
                        counts[gram] = info[fPath][0][n][gram]
                    #add to gram2id
                    gram_id = info[fPath][2][n][gram]
                    gram2id[gram] = gram_id
                    #add to id2gram
                    id2gram[gram_id] = info[fPath][3][n][gram_id]
            #save the output
            save_output(fPath,n,counts,gram2id,id2gram)
            #add to total counts
            total_counts[n].append(counts)
    #go through the grams in corpus and put those with high doc frequency in
    #a new counter
    for n in range(2,N+1):
        corpus_tmp = Counter()
        gram2id_tmp = {}
        id2gram_tmp = {}
        #also go by most_common so that the ids will match up correctly
        for gram_id, (gram,count) in enumerate(corpus[n].most_common()):
            if doc[n][gram] >= threshold:
                #delete from corpus
                corpus_tmp[gram] =  count
                #delete from gram2id_corpus and id2gram_corpus
                gram2id_tmp[gram] =  gram_id
                id2gram_tmp[gram_id] =  gram
        #save the counts/frequencies and ids for the whole corpus
        total_file = os.path.join(outdir,"corpus_total")
        save_output(total_file,n,corpus_tmp,gram2id_tmp,id2gram_tmp)
    
    #save document frequencies
    #iterate over grams and assign ids by largest doc frequencies 
    gram2id_df = {}
    id2gram_df = {}
    for n in range(2,N+1):
        gram2id_df[n] = {}
        id2gram_df[n] = {}
        for gram_id, (gram,count) in enumerate(doc[n].most_common()):
            gram2id_df[gram] = gram_id
            id2gram_df[gram_id] = gram
        doc_file = os.path.join(outdir,"doc_freqs")
        save_output(doc_file,n,doc[n],gram2id_df,id2gram_df)
    #return the output
    return(doc,gram2id_df,total_counts)

def main(indir,n,tfidf,relations,stem,stop_words,rel,thresh,split_clauses,
         outdir):
    """Main method using SnGramsCorpus object. Arguments are from command
        line"""
    sc = sngrams_corpus.SngramCorpus(indir,n,tfidf,relations,stem,stop_words,
                                     rel,thresh,split_clauses,outdir)
    logging.info("Converting Corpus to sparse matrix")
    #get sparse matrix
    sparse = sc.to_sparse()
    #save sparse matrix and dictionary
    outdir = sc.outdir
    with open(os.path.join(outdir,"mat.pkl"),"wb") as f:
        pickle.dump(sparse,f, protocol = 2)
    with open(os.path.join(outdir,"dictionary.pkl"),"wb") as f:
        pickle.dump(sc.dictionary,f, protocol = 2)
    

    
#depParser = sngrams.DepParser('..\\..\\stanford_corenlp\\stanford-corenlp-3.5.2.jar','..\\..\\stanford_corenlp\\stanford-corenlp-3.5.2-models.jar')


if __name__ == "__main__":
    #get arguments
    parser = argparse.ArgumentParser(description = " General Purpose tool for \
                                    extracting Syntactic N-gram frequencies \
                                    (sequences of words of length N connected \
                                    by grammatical dependencies\
                                    from documents. \
                                    Input: a directory of pre-processed text \
                                    files. \
                                    Output: directory of gram-frequency CSV \
                                    files.")
    parser.add_argument("indir", metavar = "indir", type = str,
                        help = "directory of Text Files to get N Grams from")
    parser.add_argument("n", metavar = "n",type = int,
                        help = "order of N Gram")    
    parser.add_argument("--tfidf",dest = "tfidf",action="store_true",
                        help = "do tfidf transformation",default = False)                          
    parser.add_argument("--relations", nargs = "+", default = [None],
                        help = "relations to exclude, defaults to [None]",
                        dest = "relations")
                        
    parser.add_argument("--stem",dest = "stem",type = str,
                        help = "Stemmer: 'snowball','porter','lemma', defaults \
                        to None", default = None)
    parser.add_argument("--no_stop",dest = "stop",action="store_false",
                        help = "discard stop words",default = True)
    parser.add_argument("--include_relation",action = "store_true",
                        default = False,
                        help = "whether to include relations in sngrams, \
                        defaults to false", dest = "rel")
    parser.add_argument("--freq", dest = "freq", action = "store_true",
                        default = False, help = "save frequencies rather than \
                                                counts")
    parser.add_argument("--threshold", dest = "thresh", type = int,
                        default = 5, help = "minimum number of documents an \
                        ngram must be in, defaults to 5")   
    parser.add_argument("--split_clauses", dest = "split_clauses", 
                        action = "store_true",
                        default = False, help = "split documents on clauses")                                             
    parser.add_argument("--outdir",dest = "outdir",default = None, type = str,
                        help = "outdir to write results to, defaults to \
                        indir/results")                    
    args = parser.parse_args()
    indir = args.indir
    n = args.N
    tfidf = args.tfidf
    relations = args.relations
    stem = args.stem
    stop_words = args.stop
    rel = args.rel
    thresh = args.thresh
    split_clauses = args.split_clauses
    outdir = args.outdir
    
    #run sngrams_from_dir with input
    #sngrams_from_dir2(indir,n,relations,stem,stop_words,rel,freq,thresh,outdir)
    main(indir,n,tfidf,relations,stem,stop_words,rel,thresh,split_clauses,outdir)