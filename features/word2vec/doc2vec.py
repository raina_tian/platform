# -*- coding: utf-8 -*-
"""
Trains a doc2vec model on a corpus

@author: Eli
"""
import sys
from gensim import models
from embedding import Sentences
from gensim.models.doc2vec import LabeledSentence
import numpy as np
import string
from six.moves import cPickle as pickle

sys.path.append("..")
from docLoader import *

class LabeledDocs(Sentences):
    """Class for memory efficient loading of a corpus, document by document,
        with labels. Child class of embedding.Sentences. Only __iter__ is
        overridden"""
        
    def __iter__(self):
        """Iterator"""
        if not self.split_clauses:
            #iterate over files
            for doc_id,fName in enumerate(self.files):
                #get document sentences
                sents = doc_sents(fName,zipped = self.zip_corpus)
                #iterate over sentences, combine and yield them
                doc = []
                for sent in sents:
                    #stem/remove stopwords/remove punctuation
                    #stem words if stem is not None
                    words = [word.lower() for word in self.word_tokenize(sent)]
                    if self.stem is not None:
                        words = [self.stemmer.stem(word) for word in words]
                    #remove stop words if stop = False
                    if not self.stop_words:
                        words = [word for word in words if not word.split("::")[0] 
                                    in self.stop]
                    #remove punctuation if punctuation is false
                    if not self.punctuation:
                        pun = string.punctuation
                        words = [word for word in words if not word.split("::")[0] 
                                    in pun]
                    doc += words
                    self.len = doc_id
                yield(LabeledSentence(words = doc,labels=[str(doc_id)]))
        else:
            #iterate over files
            clause_id = 0
            for fName in self.files:
                #get sentences for clauses in document
                clauses = doc_sents(fName,zipped = self.zip_corpus,
                                    clauses = True)
                #iterate over clauses
                for sents in clauses:
                    #iterate over sentences, combine and yield them
                    clause = []
                    for sent in sents:
                        #stem/remove stopwords/remove punctuation
                        #stem words if stem is not None
                        words = [word.lower() for word in self.word_tokenize(sent)]
                        if self.stem is not None:
                            words = [self.stemmer.stem(word) for word in words]
                        #remove stop words if stop = False
                        if not self.stop_words:
                            words = [word for word in words if not word.split("::")[0] 
                                        in self.stop]
                        #remove punctuation if punctuation is false
                        if not self.punctuation:
                            pun = string.punctuation
                            words = [word for word in words if not word.split("::")[0] 
                                        in pun]                
                        clause += words
                    clause_id +=1
                    self.len = clause_id
                    yield(LabeledSentence(words = clause,labels=[clause_id - 1]))
    
    def __len__(self):
        """returns number of documents"""
        return(self.len)
    
def train_doc2vec(docs,size = 100 ,window = 5, min_count = 5,workers = 1) :
    """Trains a Doc2Vec model using docs. See gensim documentation for the 
        other parameters
    Input:
        docs: LabeledDocs object
        size: dimension of vector space embedded to
    Output:
        d2v: trained Doc2Vec object
    """
    d2v = models.doc2vec.Doc2Vec(docs,size=size,window=window,
                                 min_count=min_count, workers = workers)
    return(d2v)
    
def get_corpus_features(d2v,corpus):
    """Gets the document vectors for each document in the corpus
    Input:
        d2v: trained Doc2Vec object
        corpus: LabeledDocs object after iterating throuhg once
    Output:
        mat: matrix where rows are document vectors
    """
    mat = np.zeros((len(corpus),d2v["0"].size))
    for i in range(len(corpus)):
        mat[i,:] = d2v[str(i)]
    return(mat)
def load_train(indir,stem = None, stop_words = True, punctuation = True,
               split_clauses = False, size=100, window=5, min_count=5,
               workers=1):
    """Loads the corpus at indir and trains a doc2vec model
    Input:
        indir: path to directory of txt files
        stem: {'snowball','porter','lemma',None} what kind of stemmer to 
                use. Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to 
                        True
        split_clauses: Boolean. Split on clauses
        size,window,min_count,workers: see gensim Word2Vec documentation
    Output:
        w2v: a trained Word2Vec object
    """
    #load corpus
    docs = LabeledDocs(indir,stem,stop_words,punctuation,split_clauses)  
    #train doc2vec
    d2v = train_doc2vec(docs,size,window,min_count,workers)
    return(d2v,docs)

def save_output(d2v,doc_mat,outfile):
    """Saves the corpus doc vectors as a matrix and trained doc2vec
    Input:
        d2v: a trained Doc2Vec object                     
        doc_mat: a n_docs * n_features matrix corresponding to each document
        outfile: file name to write to
    """
    #save matrix 
    with open(outfile + "_mat.pkl","wb") as f:
        pickle.dump(doc_mat,f,protocol = 2)
    #save Word2Vec object
    with open(outfile + "_w2v.pkl","wb") as f:
        pickle.dump(d2v,f,protocol = 2)    
               
def load_train_save(indir,stem = None, stop_words = True, punctuation = True,
               split_clauses = False, size=100, window=5, min_count=5,
               workers=1, outfile = None):
    """Loads the corpus at indir, trains a doc2vec model and saves the output
    Input:
        indir: path to directory of txt files
        stem: {'snowball','porter','lemma',None} what kind of stemmer to 
                use. Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to 
                        True
        split_clauses: Boolean. Split on clauses
        size,window,min_count,workers: see gensim Doc2Vec documentation
        outfile: file name to write to (without extension). Defaults to
                indir/results/doc2vec
    Output:
        mat: doc2vec feature matrix
        fnames: file names
    """
    #check if indir/word2vec_results exists, if not create direcotry
    #also if indir is a zip archive, create output directory in directory
    #of indir
    if not zipfile.is_zipfile(indir):
        if not os.path.exists(os.path.join(indir,"doc2vec_results")):
            os.mkdir(os.path.join(indir,"doc2vec_results"))
        if outfile is None:
            outfile = os.path.join(indir,"doc2vec_results","doc2vec")
    else:
        #get the directory of the zip archive
        tmp = os.path.split(indir)[0]
        outdir = os.path.join(tmp,"doc2vec_results")
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        if outfile is None:
            outfile = os.path.join(outdir,"doc2vec")
    #train Doc2Vec
    d2v,corpus = load_train(indir,stem,stop_words,punctuation,split_clauses,
                     size,window,min_count, workers)
    #get matrix and file names
    mat= get_corpus_features(d2v,corpus)
    #save output
    save_output(d2v,mat,outfile)
    return(mat)
