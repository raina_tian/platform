# -*- coding: utf-8 -*-
"""
Trains a word2vec model on a corpus

@author: Eli
"""
import os
import nltk
from nltk.stem.snowball import SnowballStemmer
from nltk.stem.porter import PorterStemmer  
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
import string
import six
from six.moves import cPickle as pickle
import sys
import numpy as np
from gensim import models
import re
sys.path.append("..")
sys.path.append("../phrases")
from docLoader import *
from pos_phrases import PosPhrases
from phrases import load_phraser
import zipfile


class Sentences(object):
    """Class for memory efficient loading of corpus. Iterator over sentences"""
    
    def __init__(self,indir,stem = None, stop_words = True, punctuation = True,
                 split_clauses = False):
        """Input:
            indir: path to directory of txt files
            stem: {'snowball','porter','lemma',None} what kind of stemmer to 
                    use. Defaults to None.
            stop_words: Boolean. Whether to include stopwords. Defaults to True
            punctuation: Boolean. Whether to include punctuation. Defaults to 
                            True
            split_clauses: Boolean. Split on clauses
        """        
        #check if indir is a zip archive or directory and act accordingly
        if not zipfile.is_zipfile(indir):
            #get all text files in indir
            self.files = [os.path.join(indir,f) for f in os.listdir(indir) if \
                    os.path.splitext(f)[1] == ".txt"]
            self.zip_corpus = None
        else:
            self.zip_corpus = zipfile.ZipFile(indir)
            self.files = self.zip_corpus.namelist()
        self.stop_words = stop_words
        self.punctuation = punctuation
        self.stop = set(stopwords.words('english'))
        #initialize stemmer if stem is not None
        self.stem = stem
        if self.stem is not None:
            if stem == 'porter':
                self.stemmer = PorterStemmer()
            elif stem == 'snowball':
                self.stemmer = SnowballStemmer("english")
            elif stem == 'lemma':
                self.stemmer = WordNetLemmatizer()
                #add stem as another name for lemmatize
                self.stemmer.stem = self.stemmer.lemmatize
            else:
                #raise a value error if a wrong stemmer is chosen
                raise ValueError('Not an available stemmer') 
        #set splitting on clauses
        self.split_clauses = split_clauses
        #current clauses
        self.curr_clauses = []

    def word_tokenize(self,words):
        """Faster word tokenization than nltk.word_tokenize
        Input:
            words: a string to be tokenized
        Output:
            tokens: tokenized words
        """
        tokens = re.findall(r"[a-z]+-?[a-z]+", words.lower(),
                            flags = re.UNICODE | re.LOCALE)
        return(tokens)
                  
    def __iter__(self):
        """iterator"""
        if not self.split_clauses:
            #iterate over files
            for fName in self.files:
                #get document sentences
                sents = doc_sents(fName,zipped = self.zip_corpus)
                #iterate over sentences and yield them
                for sent in sents:
                    #stem/remove stopwords/remove punctuation
                    #stem words if stem is not None
                    words = [word.lower() for word in self.word_tokenize(sent)]
                    if self.stem is not None:
                        words = [self.stemmer.stem(word) for word in words]
                    #remove stop words if stop = False
                    if not self.stop_words:
                        words = [word for word in words if not word.split("::")[0] 
                                    in self.stop]
                    #remove punctuation if punctuation is false
                    if not self.punctuation:
                        pun = string.punctuation
                        words = [word for word in words if not word.split("::")[0] 
                                    in pun]                
                    yield words
        else:
            #iterate over files
            for fName in self.files:
                #get sentences for clauses in document
                clauses = doc_sents(fName,zipped = self.zip_corpus,
                                    clauses = True)
                #iterate over clauses
                for sents in clauses:
                    #iterate over sentences and yield them
                    for sent in sents:
                        #stem/remove stopwords/remove punctuation
                        #stem words if stem is not None
                        words = [word.lower() for word in self.word_tokenize(sent)]
                        if self.stem is not None:
                            words = [self.stemmer.stem(word) for word in words]
                        #remove stop words if stop = False
                        if not self.stop_words:
                            words = [word for word in words if not word.split("::")[0] 
                                        in self.stop]
                        #remove punctuation if punctuation is false
                        if not self.punctuation:
                            pun = string.punctuation
                            words = [word for word in words if not word.split("::")[0] 
                                        in pun]                
                        yield words            
                
def memory_inefficient_load(indir,stem = None, stop_words = True, 
                            punctuation = True):
    """Memory inefficient loading a corpus from a directory. Maybe faster to
        run word2vec?
    Input:
        indir: path to directory of txt files
        stem: {'snowball','porter','lemma',None} what kind of stemmer to 
                use. Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to 
                        True
    """    
    #check if indir is a zip archive or directory and act accordingly
    if not zipfile.is_zipfile(indir):
        #get all text files in indir                        
        files = [os.path.join(indir,f) for f in os.listdir(indir) if \
                    os.path.splitext(f)[1] == ".txt"]
        zip_corpus = None
    else:
        zip_corpus = zipfile.ZipFile(indir)
        files = zip_corpus.namelist()
    stop = set(stopwords.words('english'))
    #initialize stemmer if stem is not None
    if stem is not None:
        if stem == 'porter':
            stemmer = PorterStemmer()
        elif stem == 'snowball':
            stemmer = SnowballStemmer("english")
        elif stem == 'lemma':
            stemmer = WordNetLemmatizer()
            #add stem as another name for lemmatize
            stemmer.stem = stemmer.lemmatize
        else:
            #raise a value error if a wrong stemmer is chosen
            raise ValueError('Not an available stemmer')     
    all_sents = []       
    #iterate over all files in the directory and get the sentences
    for fName in files:
        #get the sentences
        sents = doc_sents(fName,zipped = zip_corpus)
        #iterate over sentences and word tokenize, stip punctuation, etc.
        for sent in sents:
            words = [word.lower() for word in nltk.word_tokenize(sent)]
            #stem words if stem is not None
            if stem is not None:
                words = [stemmer.stem(word) for word in words]
            #remove stop words if stop = False
            if not stop_words:
                words = [word for word in words if not word.split("::")[0] 
                            in stop]
            #remove punctuation if punctuation is false
            if not punctuation:
                pun = string.punctuation
                words = [word for word in words if not word.split("::")[0] 
                            in pun]        
            all_sents.append(words)
            
    return(all_sents)
    
    
def hash32(value):
    """Helper hash function which uses 32 bit hash rather than 64 bit, for 
        using Word2Vec with Python 3"""
    return hash(value) & 0xffffffff

  
def train_word2vec(sentences, size=100, window=5, min_count=5,workers=1,
                   finalize = True) :
    """Trains a Word2Vec model using sentences. See gensim documentation for
        description of the other parameters. Size = dimension of vector space
    Input:
        sentences: iterator over sentences
        finalize: whether to finalize the model and do no more training
    Output:
        w2v: trained Word2Vec object
    """
    #split by type of python
    if six.PY2:
        w2v = models.Word2Vec(sentences,size = size, window = window,
                              min_count = min_count, workers = workers)
    elif six.PY3:
        w2v = models.Word2Vec(sentences,size = size, window = window,
                              min_count = min_count, workers = workers,
                              hashfxn = hash32)
    w2v.init_sims(replace=finalize)
    return(w2v)


def avg_features(doc,w2v):
    """Computes a feature vector for a document by averaging the word vectors
        of the words in the document
    Input:
        doc: a document as a list of sentences, each a list of words
        w2v: trained Word2Vec object
    """
    #iterate over sentences and words and get average
    vecs = []
    for sent in doc:
        for word in sent:
            try:
                vecs.append(w2v[word])
            except KeyError:
                #if there's a KeyError then skip the word
                pass
            
    return(np.mean(vecs,axis = 0))
    
def yield_docs(fList,stem = None, stop_words = True, punctuation = True,
               zipped = None,split_clauses = False):
    """yields the files in fList to get the average
    Input:
        fList: list of .txt file paths to load
        n: order of n gram
        stem: {'snowball','porter','lemma',None} what kind of stemmer to 
                use. Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to 
                        True
        zipped: a ZipFile object to read fPath from if the directory is 
                zipped. Defaults to None
    Output:
        an generator over the documents which returns (filename,sents) pairs
    """    
    stop = set(stopwords.words('english'))
    #initialize stemmer if stem is not None
    if stem is not None:
        if stem == 'porter':
            stemmer = PorterStemmer()
        elif stem == 'snowball':
            stemmer = SnowballStemmer("english")
        elif stem == 'lemma':
            stemmer = WordNetLemmatizer()
            #add stem as another name for lemmatize
            stemmer.stem = stemmer.lemmatize
        else:
            #raise a value error if a wrong stemmer is chosen
            raise ValueError('Not an available stemmer')      
    if not split_clauses:
        #iterate over all files in the directory and get the sentences
        for fName in fList:
            #list holding the sentences in the document
            doc = []
            #get the sentences
            sents = doc_sents(fName,zipped = zipped)
            #iterate over sentences and word tokenize, stip punctuation, etc.
            for sent in sents:
                words = [word.lower() for word in nltk.word_tokenize(sent)]
                #stem words if stem is not None
                if stem is not None:
                    words = [stemmer.stem(word) for word in words]
                #remove stop words if stop = False
                if not stop_words:
                    words = [word for word in words if not word.split("::")[0] 
                                in stop]
                #remove punctuation if punctuation is false
                if not punctuation:
                    pun = string.punctuation
                    words = [word for word in words if not word.split("::")[0] 
                                in pun]        
                doc.append(words)
            yield(doc)
    else:
        #iterate over all files in the directory and get the sentences
        for fName in fList:
            #get the sentences in clauses in the document
            clauses = doc_sents(fName,zipped = zipped,clauses = True)
            #iterate over clauses
            for sents in clauses:
                #initialize document
                doc = []
                #iterate over sentences and word tokenize, stip punctuation, etc.
                for sent in sents:
                    words = [word.lower() for word in nltk.word_tokenize(sent)]
                    #stem words if stem is not None
                    if stem is not None:
                        words = [stemmer.stem(word) for word in words]
                    #remove stop words if stop = False
                    if not stop_words:
                        words = [word for word in words if not word.split("::")[0] 
                                    in stop]
                    #remove punctuation if punctuation is false
                    if not punctuation:
                        pun = string.punctuation
                        words = [word for word in words if not word.split("::")[0] 
                                    in pun]        
                    doc.append(words)
                yield(doc)        
            
    
def get_doc_features(fList,w2v,stem = None, stop_words = True, 
                     punctuation = True, zipped = None,split_clauses = False):
    """Returns feature vectors for each doc in fList by averaging the word 
        vectors for the words in the document
    Input:
        fList: list of .txt file paths to load
        w2v: a trained Word2Vec object
        n: order of n gram
        stem: {'snowball','porter','lemma',None} what kind of stemmer to 
                use. Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to 
                        True
        zipped: a ZipFile object to read fPath from if the directory is 
                zipped. Defaults to None
        split_clauses: Boolean. Split on clauses
    Output:
        doc_mat: a n_docs * n_features matrix corresponding to each document
        fnames: a list of the file names the correspond to the rows
    """
    #get document generator
    doc_iter = yield_docs(fList,stem,stop_words,punctuation,zipped = zipped,
                          split_clauses = split_clauses)
    #iterate through documents to get average vectors and add to matrix
    doc_mat = []
    for doc in doc_iter:
        doc_mat.append(avg_features(doc,w2v))
    #convert to dense numpy array
    doc_mat = np.array(doc_mat)
    
    #get names
    fnames = []
    for f in fList:
        #strip extension and path to just get the file code
        fnames.append(os.path.splitext(os.path.basename(f))[0])
        
    return(doc_mat,fnames)


def get_corpus_features(indir,w2v,stem = None, stop_words = True, 
                     punctuation = True,split_clauses = False):
    """Returns feature vectors for each .txt file in indir by averaging the 
        word vectors for the words in the document
    Input:
        indir: directory of documents as txt files to read from
        w2v: a trained Word2Vec object
        n: order of n gram
        stem: {'snowball','porter','lemma',None} what kind of stemmer to 
                use. Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to 
                        True
        split_clauses: Boolean. Split on clauses
    Output:
        doc_mat: a n_docs * n_features matrix corresponding to each document
        fnames: a list of the file names the correspond to the rows
    """    
    #check if directory is zip archive or directory and act accordingly
    if not zipfile.is_zipfile(indir):
        #list the files in the directory
        files = [os.path.join(indir,f) for f in os.listdir(indir) if \
                    os.path.splitext(f)[1] == ".txt"]
        zip_corpus = None
    else:
        zip_corpus = zipfile.ZipFile(indir)
        files = zip_corpus.namelist()
        
    return(get_doc_features(files,w2v,stem,stop_words,punctuation,
                            zipped = zip_corpus,split_clauses = split_clauses))


def save_output(w2v,doc_mat,fnames,outfile):
    """Saves the corpus as a matrix along with the corresponding file names
        and the trained Word2Vec object
    Input:
         w2v: a trained Word2Vec object                     
        doc_mat: a n_docs * n_features matrix corresponding to each document
        fnames: a list of the file names the correspond to the rows
        outfile: file name to write to
    """
    #save matrix 
    with open(outfile + "_mat.pkl","wb") as f:
        pickle.dump(doc_mat,f,protocol = 2)
    #save names    
    with open(outfile + "_fnames.pkl","wb") as f:
        pickle.dump(fnames,f,protocol = 2) 
    #save Word2Vec object
    with open(outfile + "_w2v.pkl","wb") as f:
        pickle.dump(w2v,f,protocol = 2)
        
def load_train(indir,stem = None, stop_words = True, punctuation = True,
               split_clauses = False, size=100, window=5, min_count=5,workers=1, finalize = True,
               mem_efficient = False):
    """Loads the corpus at indir and trains a word2vec model
    Input:
        indir: path to directory of txt files
        stem: {'snowball','porter','lemma',None} what kind of stemmer to 
                use. Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to 
                        True
        split_clauses: Boolean. Split on clauses
        size,window,min_count,workers: see gensim Word2Vec documentation
        finalize: whether to finalize the model and do no more training.
                    Defaults to True
        mem_efficient: use the memory efficient corpus loading. Defaults 
                    to False
    Output:
        w2v: a trained Word2Vec object
    """
    #load corpus
    if mem_efficient:
        sents = Sentences(indir,stem,stop_words,punctuation,split_clauses)
    else:
        sents = memory_inefficient_load(indir,stem,stop_words,punctuation)
    #train Word2Vec
    w2v = train_word2vec(sents,size,window,min_count,workers,finalize)
    
    return(w2v)

def load_train_save(indir,stem = None, stop_words = True, punctuation = True,
               split_clauses = False, size=100, window=5, min_count=5,
               workers=1, finalize = True, mem_efficient = True,
               outfile = None):
    """Loads the corpus at indir, trains a word2vec model and saves the output
    Input:
        indir: path to directory of txt files
        stem: {'snowball','porter','lemma',None} what kind of stemmer to 
                use. Defaults to None.
        stop_words: Boolean. Whether to include stopwords. Defaults to True
        punctuation: Boolean. Whether to include punctuation. Defaults to 
                        True
        split_clauses: Boolean. Split on clauses
        size,window,min_count,workers: see gensim Word2Vec documentation
        finalize: whether to finalize the model and do no more training.
                    Defaults to True
        mem_efficient: use the memory efficient corpus loading. Defaults 
                    to False
        outfile: file name to write to (without extension). Defaults to
                indir/results/word2vec
    Output:
        mat: word2vec feature matrix
        fnames: file names
    """
    #check if indir/word2vec_results exists, if not create direcotry
    #also if indir is a zip archive, create output directory in directory
    #of indir
    if not zipfile.is_zipfile(indir):
        if not os.path.exists(os.path.join(indir,"word2vec_results")):
            os.mkdir(os.path.join(indir,"word2vec_results"))
        if outfile is None:
            outfile = os.path.join(indir,"word2vec_results","word2vec")
    else:
        #get the directory of the zip archive
        tmp = os.path.split(indir)[0]
        outdir = os.path.join(tmp,"word2vec_results")
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        if outfile is None:
            outfile = os.path.join(outdir,"word2vec")
    #train Word2Vec
    w2v = load_train(indir,stem,stop_words,punctuation,split_clauses,
                     size,window,min_count, workers, finalize, mem_efficient)
    #get matrix and file names
    mat,fnames = get_corpus_features(indir,w2v,stem,stop_words,punctuation)
    #save output
    save_output(w2v,mat,fnames,outfile)
    return(mat,fnames)