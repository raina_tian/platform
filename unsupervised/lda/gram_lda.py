# -*- coding: utf-8 -*-
"""
Performs LDA on given grams

@author: Eli
"""
import os
from gensim import corpora
import six
from six.moves import cPickle as pickle
from gensim.models.ldamodel import LdaModel
from gensim.matutils import corpus2dense
import re
from scipy.stats import entropy
import numpy as np
import zipfile

class gramDict(corpora.Dictionary):
    """Child class of corpora.Dictionary. Changes constructor to accept
        document id and counts dictionaries"""
    
    def __init__(self,gram2id,doc_freqs):
        """Constructor, uses precomputed ids
        Input:
            gram2id: dict with grams as keys and ids as values
            doc_freqs: a dict with word,doc freqs key,value pairs
         """
        #call the null super constructor
        corpora.Dictionary.__init__(self)
        #assign token dicts
        #connect grams with "-"
        for gram,gram_id in six.iteritems(gram2id):
            self.token2id["-".join(gram)] = gram_id
            self.id2token[gram_id] = "-".join(gram)
        #iterate over doc_freqs to change the keys to the ids
        for gram in doc_freqs.keys():
            self.dfs[gram2id[gram]] = doc_freqs[gram]
    
    def drop_below(self,thresh):
        """Drops every token with less than thresh doc freq"""
        
        t2id_tmp = {}
        id2t_tmp = {}
        dfs_tmp = {}
        #iterate over tokens and only keep those with dfs[gram] >= thresh
        for gram,gram_id in six.iteritems(self.token2id):
            if self.dfs[gram_id] >= thresh:
                t2id_tmp[gram] = gram_id
                id2t_tmp[gram_id] = gram
                dfs_tmp[gram_id] = self.dfs[gram_id]
        #replace old dicts with new
        self.token2id = t2id_tmp
        self.id2token = id2t_tmp
        self.dfs = dfs_tmp
        #compactify the dictionary
        self.compactify()


def get_gramDict(fPath):
    """Creates a gramDict from a pickled file at fPath
    Input: 
        fPath: a pickled tuple t with the follwoing structure:
                t[0] = document frequencies over corpus
                t[1] = dict with grams as keys and ids as values
                t[0] dict with ids as keys and grams as values
    Output:
        gd: a gramDict made from the input
    """
    #load object
    with open(fPath,'rb') as f:
        tup = pickle.load(f)
    #create gramDict
    gd = gramDict(tup[1],tup[0])
    
    return(gd)
    
def load_doc(fPath,gd):
    """Loads a pickled bag of words representation of a document at fPath
    Input: 
        fPath: a pickled tuple t with the follwoing structure:
                t[0] = term frequencies over corpus
                t[1] = dict with grams as keys and ids as values
                t[0] dict with ids as keys and grams as values
        gd: a gramDict with ids already taken from a file
    Output:
        bow: bag of words representation for use in corpus
    """
    
    #load object
    with open(fPath,'rb') as f:
        tup = pickle.load(f)
    #go through the term frequencies dict and convert to bag of words
    bow = []
    for gram,count in six.iteritems(tup[0]):
        #create a (gram_id,gram_count) tuple
        gram_count = (gd.token2id["-".join(gram)],count)
        #add to bow
        bow.append(gram_count)
    
    return(bow)
    
def load_docs(fPaths,gd):
    """Creats a corpus out of multiple files
    Input: 
        fPath: a list of files each a pickled tuple t with the following 
                structure:
                t[0] = term frequencies over corpus
                t[1] = dict with grams as keys and ids as values
                t[0] dict with ids as keys and grams as values
        gd: a gramDict with ids already taken from a file
    Output:
        corpus: a bag of words representation of the files
        names: the file names
    """
    #regex for getting rid of extension 
    drop_ext = re.compile("_[0-9].pkl")
    corpus = []
    names = []
    #iterate over documents and add to corupus
    for fPath in fPaths:
        corpus.append(load_doc(fPath,gd))
        names.append(drop_ext.split(os.path.basename(fPath))[0])
    
    return(corpus,names)
    
def load_corpus(indir,gd):
    """Loads all the pkl files in a directory into a bag of words corpus
    Input: 
        indir: a path to a directory with pickle files containing tuples
                    with the following structure:
                t[0] = term frequencies over corpus
                t[1] = dict with grams as keys and ids as values
                t[0] dict with ids as keys and grams as values
        gd: a gramDict with ids already taken from a file
    Output:
        corpus: a bag of words representation of the files
        names: file names
    """
    #list files, filtering out non pickle files and the corpus total
    #and doc frequency files
    files = [os.path.join(indir,f) for f in os.listdir(indir) if \
                os.path.splitext(f)[1] == ".pkl" and "corpus_total" \
                not in f and "doc_freqs" not in f and "lda_" not in f]
    #get the corpus from the files
    return(load_docs(files,gd))
    
def train_lda(corpus,gd,num_topics):
    """Trains an LDA model using corpus
    Input:
        corpus: a list of bag of words representations of the documents 
                in a corpus
        gd: a gramDict with ids already taken from a file
        num_topics: number of topics
    Output:
        lda: trained LdaModel
    """
    #train the lda model
    lda = LdaModel(corpus, id2word = gd, num_topics = num_topics,passes = 2, 
                   iterations = 100)
    
    return(lda)
    
def get_doc_topics(corpus,lda,names):
    """Gets the document topic vectors for a corpus
    Input:
        corpus: a list of bag of words representations of the documents 
                in a corpus
        lda: a trained LdaModel
        names: the names of the documents in corpus
    Output:
        doc_tops: a dict lists of tuples corresponding to the topic vectors
                for each document. doc names are keys, topic vectors are values
        dt_mat: a n_documents x n_topics matrix 
    """
    #get topic vectors in dict
    doc_tops = {names[i]:lda[doc] for i,doc in enumerate(corpus)}
    
    #get topic vectors in list
    dt_mat = [lda[doc] for doc in corpus]
    #convert list to matrix
    dt_mat = corpus2dense(dt_mat,lda.num_topics).T

    
    return(doc_tops,dt_mat)
    
def save_doc_topics(corpus,lda,names,outfile):
    """Saves the document topic vectors for a corpus as a dict and as a 
    sparse/dense matrix
    Input:
        corpus: a list of bag of words representations of the documents 
                in a corpus
        lda: a trained LdaModel
        names: the names of the documents in corpus
        outfile: file name to write to (without extension)
    Output:
        doc_tops: a dict lists of tuples corresponding to the topic vectors
                for each document. doc names are keys, topic vectors are values
        dt_mat: a n_documents x n_topics matrix 
    """
    
    doc_tops,dt_mat = get_doc_topics(corpus,lda,names)
    #pickle dict
    with open(outfile + "_topic_dict.pkl","wb") as f:
        pickle.dump(doc_tops,f,protocol = 2)
        
    #pickle matrix
    with open(outfile + "_topic_mat.pkl","wb") as f:
        pickle.dump(dt_mat,f,protocol = 2)
    
    #pickle file names
    with open(outfile+"_file_names.pkl","wb") as f:
        pickle.dump(names,f,protocol = 2)
        
    return(doc_tops,dt_mat)

def save_topics(lda,outfile):
    """Saves the topic distributions
    Input:
        lda: a trained LdaModel
        outfile: file name to write to (without extension)
    Output:
        topics: list of lists of (word,frequency) pairs for each topic
    """
    #get topic distributions
    topics = lda.show_topics(num_topics = lda.num_topics, 
                             num_words = len(lda.id2word),formatted = False)
    #flip the tuples so that its (word,freq)
    tmp = [[(tup[1],tup[0]) for tup in topic] for topic in topics]
    topics = tmp
    with open(outfile + "_topics.pkl","wb") as f:   
        pickle.dump(topics,f,protocol = 2)
    
    return(topics)
        
def save_all(corpus,lda,names,outfile):
    """Wrapper for the save functions. Also pickles the lda object
    Input:
        corpus: a list of bag of words representations of the documents 
                in a corpus
        lda: a trained LdaModel
        names: the names of the documents in corpus
        outfile: file name to write to (without extension)
    Output:
        doc_tops: a dict lists of tuples corresponding to the topic vectors
                for each document. doc names are keys, topic vectors are values
        dt_mat: a n_documents x n_topics matrix 
        topics: list of lists of (word,frequency) pairs for each topic        
    """
   
    doc_tops,dt_mat = save_doc_topics(corpus,lda,names,outfile)
    topics = save_topics(lda,outfile)
    with open(outfile + "_lda.pkl","wb") as f:
        pickle.dump(lda,f,protocol = 2)
    return(doc_tops,dt_mat,topics)
        
def load_train(dict_path,indir,num_topics):
    """Loads a GramDict and a corpus, then trains an lda model
    Input:
        dict_path: a pickled tuple t with the follwoing structure:
                t[0] = document frequencies over corpus
                t[1] = dict with grams as keys and ids as values
                t[0] dict with ids as keys and grams as values  
        indir: a path to a directory with pickle files containing tuples
                    with the following structure:
                t[0] = term frequencies over corpus
                t[1] = dict with grams as keys and ids as values
                t[0] dict with ids as keys and grams as values
        num_topics: number of topics
    Output:
        lda: trained LdaModel
    """
    
    #get the GramDict
    gd = get_gramDict(dict_path)
    #load corpus in bag of words format
    corpus,names = load_corpus(indir,gd)
    #train lda
    lda = train_lda(corpus,gd,num_topics)
    
    return(lda)
    
def train_save(corpus,names,gd,num_topics,outfile):
    """Trains an LdaModel and saves the output
    Input:
        corpus: a list of bag of words representations of the documents 
                in a corpus
        names: the names of the documents in corpus
        gd: a gramDict with ids already taken from a file
        num_topics: number of topics 
        outfile: file name to write to (without extension)
    Output: output of save_all
    """
    
    #train lda
    lda = train_lda(corpus,gd,num_topics)
    #save output
    output = save_all(corpus,lda,names,outfile)
    return(output)
    
def load_train_save(dict_path,indir,num_topics,outfile = None):
    """Loads a GramDict and a corpus, trains an lda model, and saves output
    Input:
        dict_path: a pickled tuple t with the following structure:
                t[0] = document frequencies over corpus
                t[1] = dict with grams as keys and ids as values
                t[0] dict with ids as keys and grams as values  
        indir: a path to a directory with pickle files containing tuples
                    with the following structure:
                t[0] = term frequencies over corpus
                t[1] = dict with grams as keys and ids as values
                t[0] dict with ids as keys and grams as values
        num_topics: number of topics
        outfile: file name to write to (without extension). Defaults to
                indir
    """

    if outfile is None:
        #if indir is a zip archive save the files differently
        if not zipfile.is_zipfile(indir):
            outfile = os.path.join(indir,"lda")
        else:
            #get the directory of the zip archive
            tmp = os.path.split(indir)[0]
            outfile = os.path.join(tmp,"lda")
    #get the GramDict
    gd = get_gramDict(dict_path)
    #load corpus in bag of words format
    corpus,names = load_corpus(indir,gd)
    #train lda
    lda = train_lda(corpus,gd,num_topics)    
    #save output
    return(save_all(corpus,lda,names,outfile))
