# -*- coding: utf-8 -*-
"""
Fits an HDP model on data

@author: Eli
"""

import os
from gensim.models.hdpmodel import HdpModel
from gensim.models.ldamodel import LdaModel
from six.moves import cPickle as pickle
from gensim.matutils import corpus2dense
import numpy as np

def train_hdp(corpus,gd):
    """Trains an HdpModel using the corpus\
    Input:
        corpus: a streaming corpus iterator
        gd: a gramDict with ids already taken from a file
        num_topics: number of topics
    Output:
        hdp: trained HdpModel
    """
    
    hdp = HdpModel(corpus,gd)
    return(hdp)

def hdp_to_lda(hdp):
    """Converts an HdpModel to an LdaModel for document inference
    Input:
        hdp: trained HdpModel
    Output:
        lda: LdaModel approximation of hdp
    """
    alpha,beta = hdp.hdp_to_lda()
    lda = LdaModel(id2word=hdp.id2word, 
    num_topics=len(alpha), alpha=alpha, eta=hdp.m_eta) 
    lda.expElogbeta = np.array(beta, dtype=np.float32)    
    return(lda)
    
def get_doc_topics(corpus,hdp,names):
    """Gets the document topic vectors for a corpus
    Input:
        corpus: a list of bag of words representations of the documents 
                in a corpus
        lda: a trained LdaModel
        names: the names of the documents in corpus
    Output:
        doc_tops: a dict lists of tuples corresponding to the topic vectors
                for each document. doc names are keys, topic vectors are values
        dt_mat: a n_documents x n_topics matrix 
    """
    #convert to lda
    lda = hdp_to_lda(hdp)
    #get topic vectors in dict
    doc_tops = {names[i]:lda[doc] for i,doc in enumerate(corpus)}
    
    #get topic vectors in list
    dt_mat = [lda[doc] for doc in corpus]
    #convert list to matrix
    dt_mat = corpus2dense(dt_mat,lda.num_topics).T
    
    return(doc_tops,dt_mat)
    
    
def save_doc_topics(corpus,hdp,names,outfile):
    """Saves the document topic vectors for a corpus as a dict and as a 
    sparse/dense matrix
    Input:
        corpus: streaming corpus iterator
        hdp: trained hdpModel
        names: the names of the documents in corpus
        outfile: file name to write to (without extension)
    Output:
        doc_tops: a dict lists of tuples corresponding to the topic vectors
                for each document. doc names are keys, topic vectors are values
        dt_mat: a n_documents x n_topics matrix 
    """  
    doc_tops,dt_mat = get_doc_topics(corpus,hdp,names)
    #pickle dict
    with open(outfile + "_topic_dict.pkl","wb") as f:
        pickle.dump(doc_tops,f,protocol = 2)
        
    #pickle matrix
    with open(outfile + "_topic_mat.pkl","wb") as f:
        pickle.dump(dt_mat,f,protocol = 2)
    
    #pickle file names
    with open(outfile+"_file_names.pkl","wb") as f:
        pickle.dump(names,f,protocol = 2)
        
    return(doc_tops,dt_mat)
    
def save_topics(hdp,outfile):
    """Saves the topic distributions
    Input:
        hdp: trained HdpModel
        outfile: file name to write to (without extension)
    Output:
        topics: list of lists of (word,frequency) pairs for each topic
    """
    #get topic distributions
    topics = hdp.show_topics(topics = -1, 
                             topn = 500,formatted = False)
    #flip the tuples so that its (word,freq)
    tmp = [[(tup[0],tup[1]) for tup in topic[1]] for topic in topics]
    topics = tmp
    with open(outfile + "_topics.pkl","wb") as f:   
        pickle.dump(topics,f,protocol = 2)
    
    return(topics)


def save_all(corpus,hdp,names,outfile):
    """Wrapper for save functions, also pickle hdp object
    Input:
        corpus: a streaming corpus iterator
        hdp: trained HdpModel
        names: names of documents in corpus
        outfile: file name to write to (without extension)
    Output:
        doc_tops: a dict lists of tuples corresponding to the topic vectors
                for each document. doc names are keys, topic vectors are values
        dt_mat: a n_documents x n_topics matrix 
        topics: list of lists of (word,frequency) pairs for each topic
    """
    
    doc_tops,dt_mat = save_doc_topics(corpus,hdp,names,outfile)
    topics = save_topics(hdp,outfile)
    with open(outfile + "_hdp.pkl","wb") as f:
        pickle.dump(hdp,f,protocol = 2)
    return(doc_tops,dt_mat,topics)

def train_save(corpus,names,gd,outfile):
    """Trains an HdpModel and saves the output
    Input:
        corpus: a list of bag of words representations of the documents 
                in a corpus
        names: the names of the documents in corpus
        gd: a gramDict with ids already taken from a file
        outfile: file name to write to (without extension)
    Output: output of save_all   
    """
    #train model
    hdp = train_hdp(corpus,gd)
    #save output
    output = save_all(corpus,hdp,names,outfile)
    return(output)